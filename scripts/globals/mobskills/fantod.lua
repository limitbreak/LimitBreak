---------------------------------------------
-- Fantod
-- Description: Attack Boost
-- Type: Enhancing
-- Utsusemi/Blink absorb: N/A
-- Range: Self
-- Notes: Enhances damage of next attack
---------------------------------------------
require("scripts/globals/monstertpmoves")
require("scripts/globals/settings")
require("scripts/globals/status")
---------------------------------------------

function onMobSkillCheck(target, mob, skill)
    return 0
end

function onMobWeaponSkill(target, mob, skill)

    local rand = math.random(1,2)
    if rand == 1 and mob:getFamily() == 141 then -- Hoof Volley ability only used by NMs
        if (skill:getID() ~= 1330) then
        mob:useMobAbility(1330)
       end
    end
	
	local power = 50
    local duration = 60

    local typeEffect = tpz.effect.BOOST

    skill:setMsg(MobBuffMove(mob, typeEffect, power, 0, duration))
    return typeEffect
end
