---------------------------------------------
-- Nightmare
-- AOE Sleep with Bio dot
---------------------------------------------
require("scripts/globals/monstertpmoves")
require("scripts/globals/settings")
require("scripts/globals/status")
---------------------------------------------

function onMobSkillCheck(target, mob, skill)
    return 0
end

function onMobWeaponSkill(target, mob, skill)
	typeEffect = tpz.effect.SLEEP_I
	local duration = math.random(30,90)
	
    if (target:canGainStatusEffect(typeEffect)) then
        local statmod = tpz.mod.INT
        local element = mob:getStatusEffectElement(typeEffect)

        local resist = applyPlayerResistance(mob, typeEffect, target, mob:getStat(statmod)-target:getStat(statmod), 0, element)
        local totalDuration = utils.clamp(duration * resist, 1)
        target:addStatusEffect(typeEffect, 1, 0, totalDuration, 25)
		target:addStatusEffect(tpz.effect.BIO, 21, 3, totalDuration, 0)
        skill:setMsg(tpz.msg.basic.SKILL_ENFEEB_IS) 
    else
    skill:setMsg(tpz.msg.basic.SKILL_NO_EFFECT)
	end
	return typeEffect
end
