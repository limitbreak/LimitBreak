------------------------------------
-- Phanauet Channel
-- https://ffxiclopedia.fandom.com/wiki/Phanauet_Channel
--
-- code modified from Manaclipper
--
------------------------------------
-- cut scenes
------------------------------------
-- 10 - arrival parked boat 
-- 11 - arrival fly in to dock, no boat
-- 12 - player on dock ?
-- 13 - player on dock ?
-- 14 - south landing departure on boat
-- 15 - south landing on dock
-- 16 - north landing departure on boat
-- 17 - north landing on dock
-- 18 - Chuaie - south landing
-- 19 - Ratoulle - central landing
-- 20 - Felourie - north landing

-- 33 - knocked out for no barge ticket - South
-- 34 - knocked out for no barge ticket - North
-- 42 - knocked out for no barge ticket - Central

-- 38 - central landing dock fly in
-- 39 - central landing player on dock
-- 40 - central landing departure on boat
-- 41 - central landing on dock

------------------------------------
local ID = require("scripts/zones/Carpenters_Landing/IDs")
require("scripts/globals/keyitems")
require("scripts/globals/zone")
------------------------------------

tpz = tpz or {}
tpz.barge = tpz.barge or {}

-- set true for debug output
local verbose = false

local act =
{
    ARRIVE = 0,
    DEPART = 1,
}

-- barge destinations
local dest =
{
    NORTH_LANDING       = 0,
    SOUTH_LANDING       = 1,
    CENTRAL_LANDING     = 2,
}

-- locations for timekeeper NPCs
tpz.barge.location =
{
    NORTH_LANDING    = 0,
    SOUTH_LANDING    = 1,
    CENTRAL_LANDING  = 2,
    BARGE            = 3,
}

local bargeSchedule =
{
    [tpz.barge.location.NORTH_LANDING] =
    {
        {time =  960, act = act.ARRIVE, dest = dest.CENTRAL_LANDING},   -- 16:00 from South Landing
        {time = 1045, act = act.DEPART, dest = dest.CENTRAL_LANDING},   -- 17:25 to Central Landing 
        {time = 2400, act = act.ARRIVE, dest = dest.CENTRAL_LANDING},   -- 16:00 from South Landing tomorrow
    },
    [tpz.barge.location.SOUTH_LANDING] =
    {
        {time =   15, act = act.ARRIVE, dest = dest.CENTRAL_LANDING}, -- 00:15 from Central Landing - Newtpool AM
        {time =   50, act = act.DEPART, dest = dest.CENTRAL_LANDING}, -- 00:50 to Central Landing - Emfea
        {time =  535, act = act.ARRIVE, dest = dest.NORTH_LANDING}, -- 08:55 from Central Landing - Newtpool PM
        {time =  610, act = act.DEPART, dest = dest.NORTH_LANDING},   -- 10:10 to North Landing 
        {time = 1455, act = act.ARRIVE, dest = dest.CENTRAL_LANDING}, -- 23:35 from Central Landing - Newtpool Tomorrow AM
    },
    [tpz.barge.location.CENTRAL_LANDING] =
    {
        {time =  275, act = act.ARRIVE, dest = dest.SOUTH_LANDING}, -- 04:35 from South Landing - Emfea
        {time =  310, act = act.DEPART, dest = dest.SOUTH_LANDING}, -- 05:10 to South Landing - Newtpool AM
        {time = 1155, act = act.ARRIVE, dest = dest.NORTH_LANDING}, -- 19:15 from North Landing 
        {time = 1190, act = act.DEPART, dest = dest.SOUTH_LANDING}, -- 19:50 to South Landing - Newtpool PM
        {time = 1455, act = act.ARRIVE, dest = dest.SOUTH_LANDING}, -- 23:35 to South Landing - Newtpool Tomorrow AM
    },
    [tpz.barge.location.BARGE] =
    {
        {time =  275, act = act.ARRIVE, route = dest.CENTRAL_LANDING},   -- 04:35 Central Landing
        {time =  535, act = act.ARRIVE, route = dest.SOUTH_LANDING},     -- 08:55 South Landing 
        {time =  960, act = act.ARRIVE, route = dest.NORTH_LANDING},     -- 16:00 North Landing 
        {time = 1155, act = act.ARRIVE, route = dest.CENTRAL_LANDING},   -- 19:15 Central Landing
        {time = 1455, act = act.ARRIVE, route = dest.SOUTH_LANDING},     -- 23:35 South Landing 24:15 wrap the day
    },
}

tpz.barge.timekeeperOnTrigger = function(player, location, eventId)
    
    if verbose then 
        printf("INFO: [%i] [%i] in tpz.barge.timekeeperOnTrigger", location, eventId)
    end
        
    local schedule = bargeSchedule[location]
    
    if schedule then
        local currentTime = VanadielHour() * 60 + VanadielMinute()
        local nextEvent = nil

        for i = 1, #schedule do
            if schedule[i].time > currentTime then
                nextEvent = schedule[i]
                break
            end
        end

        local gameMins = nextEvent.time - currentTime
        local earthSecs = gameMins * 60 / 25 -- one earth second is 25 game seconds
        local earthMins = math.ceil(earthSecs / 60)
        local gameHours = math.floor(gameMins / 60)

        if location == tpz.barge.location.BARGE then
            if verbose then 
                printf("INFO: sending startevent [%i] [%i] [%i] [%i] [%i] [%i] in tpz.barge.timekeeperOnTrigger", eventId, nextEvent.time, earthMins, gameHours, gameMins, nextEvent.route)
            end
            player:startEvent(eventId, earthMins, gameHours, nextEvent.route)
        else
            if verbose then 
                printf("INFO: sending startevent [%i] [%i] [%i] [%i] [%i] [%i] in tpz.barge.timekeeperOnTrigger", eventId, nextEvent.time, earthMins, gameHours, gameMins, nextEvent.dest)
            end
            player:startEvent(eventId, earthSecs, nextEvent.act, 0, nextEvent.dest)
        end
    else
        printf("[warning] bad location %i in tpz.barge.timekeeperOnTrigger", location)
    end
end

tpz.barge.aboard = function(player, regionId, isAboard)
    if verbose then 
        printf("INFO: player aboard set [%s] [%i] [%s] in tpz.barge.aboard", player:getName(), regionId, tostring(isAboard))
    end

    player:setCharVar("[barge]aboard", isAboard and regionId or 0)
end

tpz.barge.onZoneIn = function(player)
    local zoneId = player:getZoneID()

    -- zoning onto barge. set [barge]arrivalEventId based on schedule.
    if zoneId == tpz.zone.PHANAUET_CHANNEL then
        local schedule = bargeSchedule[tpz.barge.location.BARGE]
        local currentTime = VanadielHour() * 60 + VanadielMinute()
        local nextEvent = nil

        for i = 1, #schedule do
            if schedule[i].time > currentTime then
                nextEvent = schedule[i]
                break
            end
        end

        if nextEvent.route == dest.NORTH_LANDING then
            -- arrival CS - 10 parked boat 
            -- arrival CS - 11 fly in to dock, no boat        
            -- north landing departure CS - 16 on boat
            -- north landing CS 17 - player on dock - 13?
            player:setCharVar("[barge]arrivalEventId", 11) -- 
        elseif nextEvent.route == dest.CENTRAL_LANDING then
            -- CS38 dock fly in
            -- Central landing CS 39 - player on dock
            -- CS40 Central landing departure on boat
            -- CS41 Central landing on dock
            player:setCharVar("[barge]arrivalEventId", 38) -- 
        elseif nextEvent.route == dest.SOUTH_LANDING then
            -- arrival CS - 10 parked boat 
            -- arrival CS - 11 fly in to dock, no boat
            -- south landing departure CS - 14 on boat
            -- south landing CS 15 - player on dock - 12?
            player:setCharVar("[barge]arrivalEventId", 10) -- 
        end 

    -- zoning into carpenters landing. play the eventId stored in [barge]arrivalEventId.
    elseif zoneId == tpz.zone.CARPENTERS_LANDING then
        local eventId = player:getCharVar("[barge]arrivalEventId")
        player:setCharVar("[barge]arrivalEventId", 0)

        if eventId > 0 then
            return eventId
        else
             player:setPos(669.917, -23.138, 911.655, 111)
            return -1
        end
    end

    if verbose then 
        printf("INFO: [%s] [%i] [%s] in tpz.barge.onZoneIn", player:getName(), zoneId, player:getCharVar("[barge]arrivalEventId"))
    end

end

tpz.barge.onTransportEvent = function(player, transport)
    local ID = zones[player:getZoneID()]
    local aboard = player:getCharVar("[barge]aboard")
    local canride = false

    if aboard > 0 then
        if player:hasKeyItem(tpz.ki.ALLYOUCANRIDEPASS) then
            -- GM only KI
            canride = true
        elseif player:hasKeyItem(tpz.ki.BARGE_TICKET) then
            player:delKeyItem(tpz.ki.BARGE_TICKET)
            canride = true
        elseif player:hasKeyItem(tpz.ki.BARGE_MULTITICKET) then
            local uses = player:getCharVar("Barge_Ticket")

            if uses == 1 then
                player:messageSpecial(ID.text.END_BILLET, 0, tpz.ki.BARGE_MULTITICKET)
                player:delKeyItem(tpz.ki.BARGE_MULTITICKET)
            else
                player:messageSpecial(ID.text.LEFT_BILLET, 0, tpz.ki.BARGE_MULTITICKET, uses - 1)
            end
            player:setCharVar("Barge_Ticket", uses - 1)
            canride = true
        else
            canride = false
        end
    end

    if verbose then 
        printf("INFO: [%s] [%s] [%s] [%s] in tpz.barge.onTransportEvent", player:getName(), aboard, tostring(canride), player:getCharVar("Barge_Ticket"))
    end

    -- leaving North Landing. must be standing in region 1. must have a ticket.
    if aboard == 1 then
        if canride then
            player:startEvent(16)    
        else
            player:startEvent(34)
        end
    -- leaving South Landing. must be standing in region 2. must have a ticket.
    elseif aboard == 2 then
        if canride then
            player:startEvent(14)    
        else
            player:startEvent(33)
        end
    -- leaving Central Landing. must be standing in region 3. must have a ticket.    
    elseif aboard == 3 then
        if canride then
            player:startEvent(40)    
        else
            player:startEvent(42)
        end
    end

end

tpz.barge.ticketshopOnTrigger = function(player, eventid)
    local currentticket=0
    
    if  (player:hasKeyItem(tpz.ki.BARGE_TICKET)) then
        currentticket=tpz.ki.BARGE_TICKET
    elseif (player:hasKeyItem(tpz.ki.BARGE_MULTITICKET)) then
        currentticket=tpz.ki.BARGE_MULTITICKET
    end

    if ( currentticket ~= 0 ) then
        player:messageSpecial(ID.text.HAVE_BILLET, currentticket)
    else
        local gils=player:getGil()
        player:startEvent(eventid, tpz.ki.BARGE_TICKET, tpz.ki.BARGE_MULTITICKET , 50, 300)
    end   
end

tpz.barge.ticketshoponEventFinish = function(player, csid, option)
    if (csid == 31 or csid == 32 or csid == 43) then
        if (option==1) then
            player:delGil(50)
            player:addKeyItem(tpz.ki.BARGE_TICKET)
            player:messageSpecial(ID.text.KEYITEM_OBTAINED, tpz.ki.BARGE_TICKET)
        elseif (option==2) then
            player:delGil(300)
            player:addKeyItem(tpz.ki.BARGE_MULTITICKET)
            player:messageSpecial(ID.text.KEYITEM_OBTAINED, tpz.ki.BARGE_MULTITICKET)
            player:setCharVar("Barge_Ticket", 10)
        end
    end
end

tpz.barge.onRouteStart = function(transportZone)
    -- set stage with relevant npc's for the route and questlines
    if verbose then 
        printf("INFO: [%s] [%i] in tpz.barge.onRouteStart", transportZone,VanadielHour() * 60 + VanadielMinute())
    end



end

tpz.barge.onRouteEnd = function(transportZone)
    -- clean stage of relevant npc's for the route and questlines
    -- clean up any mobs
    if verbose then 
        printf("INFO: [%s] [%i] in tpz.barge.onRouteEnd", transportZone,VanadielHour() * 60 + VanadielMinute())
    end


end

tpz.barge.onRouteUpdate = function(transportZone, tripTime)
    -- npc's should have their own hook for this to say their lines per route
    -- setup for pop window (ie. Stubborn Dredvodd)
    -- TODO: find the cut scene id for the Dredvodd pop
    if verbose then 
        printf("INFO: [%s] [%i] [%i] in tpz.barge.onRouteUpdate", transportZone,VanadielHour() * 60 + VanadielMinute(), tripTime)
    end




end
