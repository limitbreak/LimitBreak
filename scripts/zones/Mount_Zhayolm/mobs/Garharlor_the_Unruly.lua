-----------------------------------
-- Area: Mount Zhayolm
--  Mob: Garhorlor the Unruly RNG
-----------------------------------
mixins = {require("scripts/mixins/job_special")}
local ID = require("scripts/zones/Mount_Zhayolm/IDs")
-----------------------------------
function onMobSpawn(mob)
    tpz.mix.jobSpecial.config(mob, {
        specials =
        {
            {id = 735, hpp = math.random(45,70)}, -- uses eagle eye shot between 45 and 70
        },
    })
end

function onMobFight(mob, target)
    local brutal = mob:getID()
    -- Link all 3 NMs
    for i = brutal+1, brutal+2 do
        local m = GetMobByID(i)
        if m:getCurrentAction() == tpz.act.ROAMING then
            m:updateEnmity(target)
        end
    end
end

function onMobDespawn(mob)
    local unruly = GetMobByID(ID.mob.GARHARLOR_THE_UNRULY)
    local rabid = GetMobByID(ID.mob.GARFURLAR_THE_RABID)
    local brutal = GetMobByID(ID.mob.GARHORLUR_THE_BRUTAL)

    -- 16 to 36 hours or longest current set respawn, whichever is largest.
    local poptime = os.time() + math.random(57600,129600)
    local respawn = math.max(poptime, GetServerVariable("BrutalDeath"), GetServerVariable("RabidDeath"))

    SetServerVariable("BrutalDeath", respawn)
    SetServerVariable("UnrulyDeath", respawn)
    SetServerVariable("RabidDeath", respawn)

    brutal:setRespawnTime(respawn)
    unruly:setRespawnTime(respawn)
    rabid:setRespawnTime(respawn)
end