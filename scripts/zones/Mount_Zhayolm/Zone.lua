-----------------------------------
--
-- Zone: Mount_Zhayolm (61)
--
-----------------------------------
local ID = require("scripts/zones/Mount_Zhayolm/IDs")
require("scripts/globals/helm")
require("scripts/globals/zone")
-----------------------------------

function onInitialize(zone)
    UpdateNMSpawnPoint(ID.mob.CERBERUS)
	local cerre = GetServerVariable("CerbRespawn")
	if os.time() < cerre then
		GetMobByID(ID.mob.CERBERUS):setRespawnTime(cerre - os.time())
	else
		SpawnMob(ID.mob.CERBERUS)
	end

	UpdateNMSpawnPoint(ID.mob.GARHARLOR_THE_UNRULY)
	UpdateNMSpawnPoint(ID.mob.GARFURLAR_THE_RABID)
	UpdateNMSpawnPoint(ID.mob.GARHORLUR_THE_BRUTAL)
	local triorespawn = math.max(GetServerVariable("BrutalDeath"),GetServerVariable("RabidDeath"),GetServerVariable("UnrulyDeath"))
	if os.time() < triorespawn then
		GetMobByID(ID.mob.GARHARLOR_THE_UNRULY):setRespawnTime(triorespawn - os.time())
		GetMobByID(ID.mob.GARFURLAR_THE_RABID):setRespawnTime(triorespawn - os.time())
		GetMobByID(ID.mob.GARHORLUR_THE_BRUTAL):setRespawnTime(triorespawn - os.time())
	else
		SpawnMob(ID.mob.GARHARLOR_THE_UNRULY)
		SpawnMob(ID.mob.GARFURLAR_THE_RABID)
		SpawnMob(ID.mob.GARHORLUR_THE_BRUTAL)
	end

    tpz.helm.initZone(zone, tpz.helm.type.MINING)
end

function onZoneIn(player, prevZone)
    local cs = -1
    if prevZone == tpz.zone.LEBROS_CAVERN then
        player:setPos(681.950, -24.00, 369.936, 40)
    elseif player:getXPos() == 0 and player:getYPos() == 0 and player:getZPos() == 0 then
        player:setPos(-521.016, -6.191, 60.013, 126)
    end
    return cs
end

function afterZoneIn(player)
    player:entityVisualPacket("1pb1")
    player:entityVisualPacket("2pb1")
end

function onRegionEnter(player, region)
end

function onEventUpdate(player, csid, option)
end

function onEventFinish(player, csid, option)
    if csid == 208 then
        player:setPos(0, 0, 0, 0, 63)
    end
end

function onGameDay()
	local NextSpawn = math.random(1,9)
	local MetalKI = GetNPCByID(ID.npc.CAST_METAL_PLATE)
	
	if NextSpawn == 1 then
		MetalKI:setPos(152.354, -14.225, -146.770)
	elseif NextSpawn == 2 then
		MetalKI:setPos(258.449,	-16.209, -220.908)
	elseif NextSpawn == 3 then
		MetalKI:setPos(389.521, -14.311, -228.919)
	elseif NextSpawn == 4 then
		MetalKI:setPos(462.338, -15.541, -59.217)
	elseif NextSpawn == 5 then
		MetalKI:setPos(600.102, -17.890, 14.216)
	elseif NextSpawn == 6 then
		MetalKI:setPos(732.790,	-22.990, 19.116)
	elseif NextSpawn == 7 then
		MetalKI:setPos(617.666, -24.054, 102.911)
	elseif NextSpawn == 8 then
		MetalKI:setPos(570.851,	-25.235, 212.295)
	elseif NextSpawn == 9 then
		MetalKI:setPos(789.204,	-21.883, 329.779)
	end
end