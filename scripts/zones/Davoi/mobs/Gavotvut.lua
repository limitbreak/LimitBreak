-----------------------------------
-- Area: Davoi
--  Mob: Gavotvut
-- Involved in Quest: The Doorman
-----------------------------------
require("scripts/globals/settings")
require("scripts/globals/quests")
mixins = {require("scripts/mixins/job_special")}
-----------------------------------
function onMobSpawn(mob)
	mob:setMod(tpz.mod.SLEEPRES, 75)
	mob:setMod(tpz.mod.LULLABYRES, 75)
end

function onMobDeath(mob, player, isKiller)

    if (player:getCharVar("theDoormanMyMob") == 1) then
        player:addCharVar("theDoormanKilledNM", 1)
    end

end
