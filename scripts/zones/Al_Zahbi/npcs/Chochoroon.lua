-----------------------------------
-- Area: Al Zahbi
--  NPC: Chochoroon
-- Type: Appraiser
-- !pos -42.739 -1 -45.987 48
-----------------------------------
local ID = require("scripts/zones/Al_Zahbi/IDs")
require("scripts/globals/appraisal")
require("scripts/globals/npc_util")
-----------------------------------

local gilToPay = 50

function onTrade(player, npc, trade)
    local appraisalCsid = 261
    tpz.appraisalUtil.appraiseItem(player, npc, trade, gilToPay, appraisalCsid, { ID.text.NOT_HAVE_ENOUGH_GIL, 4618 })
end

function onTrigger(player, npc)
    player:startEvent(260, gilToPay)
end

function onEventUpdate(player, csid, option)
end

function onEventFinish(player, csid, option)
    local appraisalCsid = 261
    tpz.appraisalUtil.appraisalOnEventFinish(player, csid, option, gilToPay, appraisalCsid)
end