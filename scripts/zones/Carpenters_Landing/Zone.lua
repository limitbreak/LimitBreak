-----------------------------------
--
-- Zone: Carpenters_Landing (2)
--
-----------------------------------
local func = require("scripts/zones/Carpenters_Landing/globals")
local ID = require("scripts/zones/Carpenters_Landing/IDs")
require("scripts/globals/chocobo_digging")
require("scripts/globals/conquest")
require("scripts/globals/helm")
require("scripts/globals/barge")
-----------------------------------

function onChocoboDig(player, precheck)
    return tpz.chocoboDig.start(player, precheck)
end

function onInitialize(zone)
--    UpdateNMSpawnPoint(ID.mob.TEMPEST_TIGON)
--    GetMobByID(ID.mob.TEMPEST_TIGON):setRespawnTime(math.random(900, 10800))

    tpz.helm.initZone(zone, tpz.helm.type.LOGGING)
    func.herculesTreeOnGameHour()

    -- barge regions
    zone:registerRegion(1, -300, -10, 499, -275, 10, 534) -- Barge at north landing
    zone:registerRegion(2, 233, -10, -545, 260, 10, -513) -- Barge at south landing
    zone:registerRegion(3, -121, -10, 56.2, -143, 10, 90) -- Barge as central landing

    -- barge events
    zone:addListener("TRANSPORTZONE_END", "CARPENTERS_LANDING_TRANSPORTZONE_END", function(transportZone)
        tpz.barge.onRouteEnd(transportZone)      
    end)

    zone:addListener("TRANSPORTZONE_START", "CARPENTERS_LANDING_TRANSPORTZONE_START", function(transportZone)
        tpz.barge.onRouteStart(transportZone)

        -- barge spawns by route - TODO: see sea_creatures for reference
        --tpz.sea_creatures.checkSpawns(ID, 5, 1) -- 5 percent on init
    end)

    zone:addListener("TRANSPORTZONE_UPDATE", "CARPENTERS_LANDING_TRANSPORTZONE_UPDATE", function(transportZone, tripTime)
        tpz.barge.onRouteUpdate(transportZone, tripTime)

        -- barge spawns by route - TODO: see sea_creatures for reference
        -- tpz.sea_creatures.checkSpawns(ID, 1, 2) -- 1 percent per vana minute, 2 total mobs
    end)
end

function onZoneIn(player, prevZone)
    local cs = -1
    if (player:getXPos() == 0 and player:getYPos() == 0 and player:getZPos() == 0) then
        if prevZone == tpz.zone.PHANAUET_CHANNEL then
            cs = tpz.barge.onZoneIn(player)
        else
            player:setPos(6.509, -9.163, -819.333, 239)
        end
    end
    return cs
end

function onConquestUpdate(zone, updatetype)
    tpz.conq.onConquestUpdate(zone, updatetype)
end

function onGameHour(zone)
    local hour = VanadielHour()

    if hour == 7 or hour == 22 then
        func.herculesTreeOnGameHour()
    end
end

function onGameDay()
	SetServerVariable("[DIG]ZONE2_ITEMS", 0)
end

function onRegionEnter(player, region)
    --printf("region enter %i ", region:GetRegionID())
    tpz.barge.aboard(player, region:GetRegionID(), true)
end

function onRegionLeave(player, region)
    --printf("region leave %i ", region:GetRegionID())
    tpz.barge.aboard(player, region:GetRegionID(), false)
end

function onTransportEvent(player, transport)
    tpz.barge.onTransportEvent(player, transport)
end

function onEventUpdate(player, csid, option)
end

function onEventFinish(player, csid, option)

    ------------------------------------
    -- cut scenes
    ------------------------------------
    -- 10 - arrival parked boat 
    -- 11 - arrival fly in to dock, no boat
    -- 12 - player on dock ?
    -- 13 - player on dock ?
    -- 14 - south landing departure on boat
    -- 15 - south landing on dock
    -- 16 - north landing departure on boat
    -- 17 - north landing on dock
    -- 18 - Chuaie - south landing
    -- 19 - Ratoulle - central landing
    -- 20 - Felourie - north landing

    -- 33 - knocked out for no barge ticket - South
    -- 34 - knocked out for no barge ticket - North
    -- 42 - knocked out for no barge ticket - Central

    -- 38 - central landing dock fly in
    -- 39 - central landing player on dock
    -- 40 - central landing departure on boat
    -- 41 - central landing on dock    

    if csid == 10 then
        player:startEvent(15) -- arrive at South Landing 
    elseif csid == 11 then
        player:startEvent(17) -- arrive at North Landing
    elseif csid == 38 then
        player:startEvent(41) -- arrive at Central Landing
    elseif csid == 14 or csid == 16 or csid == 40 then
        player:setPos(0, 0, 0, 0, tpz.zone.PHANAUET_CHANNEL)
    end    
end
