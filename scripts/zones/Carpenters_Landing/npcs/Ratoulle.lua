-----------------------------------
-- Area: Carpenters' Landing
--  NPC: Ratoulle
-- Type: Adventurer's Assistant
-- !pos -133.959 -3 60.839 2
-- -134.909 0.000 63.070
-----------------------------------
require("scripts/globals/barge")
local timekeeperLocation = tpz.barge.location.CENTRAL_LANDING
local timekeeperEventId = 19

function onTrade(player, npc, trade)
end

function onTrigger(player, npc)
    tpz.barge.timekeeperOnTrigger(player, timekeeperLocation, timekeeperEventId)
end

function onEventUpdate(player, csid, option)
end

function onEventFinish(player, csid, option)
end
