-----------------------------------
-- Area: Carpenters' Landing
--  NPC: Felourie
-- Type: Standard NPC
-- !pos -300.134 -2.999 505.016 2 
-- -301.181 -2.999 507.121
-----------------------------------
require("scripts/globals/barge")
local timekeeperLocation = tpz.barge.location.NORTH_LANDING
local timekeeperEventId = 20

function onTrade(player, npc, trade)
end

function onTrigger(player, npc)
    tpz.barge.timekeeperOnTrigger(player, timekeeperLocation, timekeeperEventId)
end

function onEventUpdate(player, csid, option)
end

function onEventFinish(player, csid, option)
end
