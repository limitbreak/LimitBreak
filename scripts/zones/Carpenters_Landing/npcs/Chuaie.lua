-----------------------------------
-- Area: Carpenters' Landing
--  NPC: Chuaie
-- Type: Adventurer's Assistant
-- !pos 231.384 -3 -531.830 2
-- 232.547 -4.176 -529.924
-----------------------------------
require("scripts/globals/barge")
local timekeeperLocation = tpz.barge.location.SOUTH_LANDING
local timekeeperEventId = 18

function onTrade(player, npc, trade)
end

function onTrigger(player, npc)
    tpz.barge.timekeeperOnTrigger(player, timekeeperLocation, timekeeperEventId)
end

function onEventUpdate(player, csid, option)
end

function onEventFinish(player, csid, option)
end
