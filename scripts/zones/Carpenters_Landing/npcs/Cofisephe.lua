-----------------------------------
-- Area: Carpenters' Landing
--  NPC: Cofisephe
-- Type: Adventurer's Assistant
-- !pos 210.327 -3.885 -532.511 2
-----------------------------------
require("scripts/globals/barge")
local ticketshopEventId = 31

function onTrade(player, npc, trade)
end

function onTrigger(player, npc)
    tpz.barge.ticketshopOnTrigger(player,ticketshopEventId) 
end

function onEventUpdate(player, csid, option)
end

function onEventFinish(player, csid, option)
    tpz.barge.ticketshoponEventFinish(player, csid, option)
end
