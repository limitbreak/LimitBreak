-----------------------------------
-- Area: Carpenters' Landing
--  NPC: Coupulie
-- Type: Standard NPC
-- !pos -313.585 -3.628 490.944 2
-----------------------------------
require("scripts/globals/barge")
local ticketshopEventId = 32

function onTrade(player, npc, trade)
end

function onTrigger(player, npc)
    tpz.barge.ticketshopOnTrigger(player, ticketshopEventId)
end

function onEventUpdate(player, csid, option)
end

function onEventFinish(player, csid, option)
    tpz.barge.ticketshoponEventFinish(player, csid, option)
end
