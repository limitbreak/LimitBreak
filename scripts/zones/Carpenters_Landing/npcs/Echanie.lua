-----------------------------------
-- Area: Carpenters' Landing
--  NPC: Echanie
-- Type: Standard NPC
-- !pos -148.3136 -2.9521 46.4020 128
-----------------------------------
require("scripts/globals/barge")
local ticketshopEventId = 43

function onTrade(player, npc, trade)
end

function onTrigger(player, npc)
    tpz.barge.ticketshopOnTrigger(player, ticketshopEventId)
end

function onEventUpdate(player, csid, option)
end

function onEventFinish(player, csid, option)
    tpz.barge.ticketshoponEventFinish(player, csid, option)
end
