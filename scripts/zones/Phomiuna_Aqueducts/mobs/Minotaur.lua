-----------------------------------
-- Area: Phomiuna Aqueducts
--  Mob: Minotaur
-----------------------------------
mixins = {require("scripts/mixins/fomor_hate")}
require("scripts/globals/missions")
require("scripts/globals/status")
-----------------------------------

function onMobSpawn(mob)
    mob:setLocalVar("fomorHateDecrease", 1)
    mob:setLocalVar("fomorHateAdj", 2)
    mob:setMobMod(tpz.mobMod.DRAW_IN, 1)
    mob:setMobMod(tpz.mobMod.DRAW_IN_INCLUDE_PARTY, 1)
    mob:setMobMod(tpz.mobMod.DRAW_IN_CUSTOM_RANGE, 10)
    mob:addMod(tpz.mod.DOUBLE_ATTACK, 100)
end

function onMobDeath(mob, player, isKiller)
    if (player:getCurrentMission(COP) == tpz.mission.id.cop.DISTANT_BELIEFS and player:getCharVar("PromathiaStatus") == 0) then
        player:setCharVar("PromathiaStatus", 1)
    end
end

function onMobDrawIn(mob, target)
    local battleTarget = mob:getTarget()

    if target:getID() == battleTarget:getID() then
        mob:useMobAbility(({498,499,500,501,502})[math.random(1,6)]) -- triclip, back_swish, mow, frightful_roar, mortal_ray
    end
end

function onMobWeaponSkillPrepare(mob, target)
    return ({498,499,500,501,502})[math.random(1,5)]
end