-----------------------------------
-- Area: The Boyahda Tree
--   NM: Voluptuous Vivian
-----------------------------------
mixins = {require("scripts/mixins/job_special")}
require("scripts/globals/status")
require("scripts/globals/titles")
-----------------------------------

function onMobSpawn(mob)
    mob:setMobMod(tpz.mobMod.CLAIM_SHIELD, 1)
    mob:setMobMod(tpz.mobMod.SIGHT_RANGE, 22)
    mob:setMobMod(tpz.mobMod.SOUND_RANGE, 22)
    mob:addMod(tpz.mod.SLEEPRESTRAIT, 50)
    mob:setMod(tpz.mod.DOUBLE_ATTACK, 10)
end

function onMobInitialize(mob)
    mob:setMobMod(tpz.mobMod.DRAW_IN, 1)
end

function onMobDeath(mob, player, isKiller)
    player:addTitle(tpz.title.THE_VIVISECTOR)
end
