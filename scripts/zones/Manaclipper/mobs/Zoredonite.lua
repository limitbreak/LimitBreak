-----------------------------------
-- Area: Manaclipper
--   NM: Zoredonite
-----------------------------------
mixins = {require("scripts/mixins/job_special")}
mixins = {require("scripts/mixins/families/uragnite")}
-----------------------------------

function onMobInitialize(mob)
    mob:setMobMod(tpz.mobMod.IDLE_DESPAWN, 300)
end

function onMobDeath(mob, player, isKiller)
end

function onMobDespawn(mob)
    mob:setRespawnTime(43200) -- 12 hours
end
