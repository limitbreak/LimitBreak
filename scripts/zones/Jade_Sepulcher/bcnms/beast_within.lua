-----------------------------------
-- Area: Jade Sepulcher
-- BCNM: BLU LB5 - The Beast Within
-----------------------------------
require("scripts/globals/battlefield")
require("scripts/globals/titles")
require("scripts/globals/utils")
require("scripts/globals/quests")
require("scripts/globals/npc_util")
----------------------------------------

function onBattlefieldTick(battlefield, tick)
    tpz.battlefield.onBattlefieldTick(battlefield, tick)
end

function onBattlefieldRegister(player, battlefield)
end

function onBattlefieldEnter(player, battlefield)
end

function onBattlefieldLeave(player, battlefield, leavecode)
    if leavecode == tpz.battlefield.leaveCode.WON then
        local name, clearTime, partySize = battlefield:getRecord()
        player:startEvent(32001, battlefield:getArea(), clearTime, partySize, battlefield:getTimeInside(), 1, battlefield:getLocalVar("[cs]bit"), 0)
    elseif leavecode == tpz.battlefield.leaveCode.LOST then
        player:startEvent(32002)
    end
end

function onEventUpdate(player, csid, option)
end

function onEventFinish(player, csid, option)
 	local beasts = player:getQuestStatus(AHT_URHGAN, tpz.quest.id.ahtUrhgan.THE_BEAST_WITHIN)
   
	if csid == 32001 and player:getMainJob() == tpz.job.BLU and beasts == QUEST_ACCEPTED then
		player:startEvent(6)
    end
	if csid == 6 then
		player:levelCap(75)
		player:showText(player, 7737)
		npcUtil.giveItem(player, 4181)
		npcUtil.completeQuest(player, AHT_URHGAN, tpz.quest.id.ahtUrhgan.THE_BEAST_WITHIN, {
            title = tpz.title.MASTER_OF_AMBITION,
        })
	end
end
