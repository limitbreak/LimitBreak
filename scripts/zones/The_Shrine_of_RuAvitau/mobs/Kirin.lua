-----------------------------------
-- Area: The Shrine of Ru'Avitau
--   NM: Kirin
-----------------------------------
local ID = require("scripts/zones/The_Shrine_of_RuAvitau/IDs")
require("scripts/globals/titles")
require("scripts/globals/mobs")
mixins = {require("scripts/mixins/job_special")}
-----------------------------------

function onMobInitialize( mob )
    mob:setMobMod(tpz.mobMod.IDLE_DESPAWN, 180)
    mob:setMobMod(tpz.mobMod.ADD_EFFECT, 1)
end

function onMobSpawn(mob)
    mob:setMod(tpz.mod.REGAIN, 1000)
    mob:setMod(tpz.mod.WINDRES, -64)
    mob:setMod(tpz.mod.SILENCERES, 35)
    mob:setMod(tpz.mod.STUNRES, 35)
    mob:setMod(tpz.mod.BINDRES, 35)
    mob:setMod(tpz.mod.GRAVITYRES, 35)
    mob:setMP(5000)
    mob:addStatusEffect(tpz.effect.REGEN, 50, 3, 0)
    mob:setLocalVar("numAdds", 1)
    mob:setLocalVar("SpellCount", math.random(4,6))
end

function onAdditionalEffect(mob, target, damage)
    return tpz.mob.onAddEffect(mob, target, damage, tpz.mob.ae.ENSTONE)
end

function onMobFight(mob, target)
    if (mob:checkDistance(target) > 7) and (mob:checkDistance(target) <= 20) and (mob:getLocalVar("AbilityCount") >= mob:getLocalVar("SpellCount")) then
        mob:SetMobAbilityEnabled(false)
        mob:SetMagicCastingEnabled(true)
        mob:castSpell()
    elseif (mob:checkDistance(target) > 7) and (mob:checkDistance(target) <= 20) and (mob:getLocalVar("AbilityCount") < mob:getLocalVar("SpellCount")) then
        mob:SetMobAbilityEnabled(false)
        mob:SetMagicCastingEnabled(false)
    elseif (mob:checkDistance(target) <= 7) and (mob:getLocalVar("AbilityCount") >= mob:getLocalVar("SpellCount")) then
        mob:SetMobAbilityEnabled(false)
        mob:SetMagicCastingEnabled(true)
        mob:castSpell()
    elseif (mob:checkDistance(target) <= 7) and (mob:getLocalVar("AbilityCount") < mob:getLocalVar("SpellCount")) then
        mob:SetMobAbilityEnabled(true)
        mob:SetMagicCastingEnabled(false)
    else
        mob:SetMobAbilityEnabled(false)
        mob:SetMagicCastingEnabled(false)
    end

    if mob:getMP() < 500 then
        mob:setMP(5000)
    end

    -- spawn gods
    local numAdds = mob:getLocalVar("numAdds")
    if (mob:getBattleTime() / 180 == numAdds) then
        local godsRemaining = {}
        for i = 1, 4 do
            if (mob:getLocalVar("add"..i) == 0) then
                table.insert(godsRemaining, i)
            end
        end
        if (#godsRemaining > 0) and mob:getCurrentAction() ~= 34 and mob:actionQueueEmpty() then
            mob:setLocalVar("numAdds", numAdds + 1)
            mob:SetAutoAttackEnabled(false)
            mob:SetMagicCastingEnabled(false)
            mob:SetMobAbilityEnabled(false)
            mob:entityAnimationPacket("casm")
            mob:timer(5000, function(mob)
                mob:entityAnimationPacket("shsm")
                spawnGod(mob)
            end)
        end
    end

    -- ensure all spawned pets are doing stuff
    for i = ID.mob.KIRIN + 1, ID.mob.KIRIN + 4 do
        local god = GetMobByID(i)
        if (god:getCurrentAction() == tpz.act.ROAMING) then
            god:updateEnmity(target)
        end
    end
end

function onMobWeaponSkillPrepare(mob)
    local roll = math.random(1,6)

    if roll == 1 then
        return 797 -- deadly_hold
    elseif roll == 2 then
        return 798 -- tail_swing
    elseif roll == 3 then
        return 799 -- tail_smash
    elseif roll == 4 then
        return 800 -- heat_breath
    elseif roll == 5 then
        return 802 -- great_sandstorm
    elseif roll == 6 then
        return 803 -- great_whirlwind
    end

    if (mob:getLocalVar("AbilityCount") == 0) then
        mob:setLocalVar("SpellCount", math.random(4,6))
    end

    if (mob:getLocalVar("AbilityCount") >= mob:getLocalVar("SpellCount")) then
        return 1
    end
        return 0
end

function onCastStarting(mob, spell)
    mob:setLocalVar("AbilityCount", 0)
end

function onMobWeaponSkill(target, mob, skill, act)
    mob:setLocalVar("AbilityCount", mob:getLocalVar("AbilityCount") + 1)
end

function onMobDeath(mob, player, isKiller)
    player:addTitle( tpz.title.KIRIN_CAPTIVATOR )
    player:showText( mob, ID.text.KIRIN_OFFSET + 1 )
    for i = ID.mob.KIRIN + 1, ID.mob.KIRIN + 4 do
        DespawnMob(i)
    end
end

function onMobDespawn( mob )
    for i = ID.mob.KIRIN + 1, ID.mob.KIRIN + 4 do
        DespawnMob(i)
    end
end

function spawnGod(mob, target)
    local godsRemaining = {}
    for i = 1, 4 do
       if (mob:getLocalVar("add"..i) == 0) then
           table.insert(godsRemaining, i)
       end
    end
    local g = godsRemaining[math.random(#godsRemaining)]
    local god = SpawnMob(ID.mob.KIRIN + g)
    god:updateEnmity(mob:getTarget())
    god:setPos(mob:getXPos(), mob:getYPos(), mob:getZPos())
    mob:setLocalVar("add"..g, 1)
    mob:SetAutoAttackEnabled(true)
    mob:SetMagicCastingEnabled(true)
    mob:SetMobAbilityEnabled(true)
end
