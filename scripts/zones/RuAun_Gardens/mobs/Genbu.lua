-----------------------------------
-- Area: Ru'Aun Gardens
--   NM: Genbu 17309980
-- HP: 19000
-- EVA: 377
-- DEF: 544 +150
-- ATK: 436
-- ACC: 384
-----------------------------------
local ID = require("scripts/zones/RuAun_Gardens/IDs")
mixins = {require("scripts/mixins/job_special")}
require("scripts/globals/mobs")
-----------------------------------

function onMobInitialize(mob)
    mob:setMobMod(tpz.mobMod.ADD_EFFECT, 1)
end

function onMobSpawn(mob)
	mob:addMod(tpz.mod.DEF, 150)
	mob:addMod(tpz.mod.DMGPHYS, -50)
end

function onAdditionalEffect(mob, target, damage)
    return tpz.mob.onAddEffect(mob, target, damage, tpz.mob.ae.ENWATER)
end

function onMobDeath(mob, player, isKiller)
    player:showText(mob, ID.text.SKY_GOD_OFFSET + 6)
end
