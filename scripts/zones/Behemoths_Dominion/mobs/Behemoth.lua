-----------------------------------
-- Area: Behemoth's Dominion
--  HNM: Behemoth
-----------------------------------
local ID = require("scripts/zones/Behemoths_Dominion/IDs")
mixins = {require("scripts/mixins/rage")}
require("scripts/globals/settings")
require("scripts/globals/status")
require("scripts/globals/titles")
-----------------------------------

function onMobSpawn(mob)
    if LandKingSystem_NQ > 0 or LandKingSystem_HQ > 0 then
        GetNPCByID(ID.npc.BEHEMOTH_QM):setStatus(tpz.status.DISAPPEAR)
    end
    if LandKingSystem_HQ == 0 then
        SetDropRate(251, 3342, 0) -- do not drop savory_shank
    end

    mob:setLocalVar("[rage]timer", 1800) -- 30 minutes
    mob:setMobMod(tpz.mobMod.CLAIM_SHIELD, 1)

    mob:addMod(tpz.mod.DEF, -80)
    mob:addMod(tpz.mod.ATT, -220)
    mob:addMod(tpz.mod.EVA, -40)
    mob:addMod(tpz.mod.ACC, -20)
    mob:addMod(tpz.mod.ATTP, -65)
    mob:addMod(tpz.mod.SLEEPRESTRAIT, 90)
    mob:setMod(tpz.mod.RESBUILD_GRAVITY, 10)
    mob:setMod(tpz.mod.DOUBLE_ATTACK, 10)
    mob:setMod(tpz.mod.MDEF, -30)
    mob:setMobMod(tpz.mobMod.GIL_MIN, 20000)
    mob:setMobMod(tpz.mobMod.GIL_MAX, 20000)
end

function onMobFight(mob, target)
    -- Set maximum limits for zone draw in
    local drawInWait = mob:getLocalVar("DrawInWait")

    if (target:getXPos() > -180 and target:getZPos() > 53) and os.time() > drawInWait then -- North Tunnel Draw In
        local rot = target:getRotPos()
        target:setPos(-182.19,-19.83,58.34,rot)
        mob:messageBasic(232, 0, 0, target)
        mob:setLocalVar("DrawInWait", os.time() + 2)
    elseif (target:getXPos() > -230 and target:getZPos() < 5) and os.time() > drawInWait then  -- South Tunnel Draw In
        local rot = target:getRotPos()
        target:setPos(-235.35,-20.01,-4.47,rot)
        mob:messageBasic(232, 0, 0, target)
        mob:setLocalVar("DrawInWait", os.time() + 2)
    end
end

function onMobDeath(mob, player, isKiller)
    player:addTitle(tpz.title.BEHEMOTHS_BANE)
end

function onMobDespawn(mob)
    local kills = GetServerVariable("[PH]King_Behemoth") + 1
    local respawn = (75600 + ((math.random(0, 6)) * 1800)) -- 21 - 24 hours with half hour windows
    local spawnChance = 0

    if kills > 2 then
        -- 4th Day = 15%
        -- 5th Day = 27.5%
        -- 6th Day = 40%
        -- 7th Day = 52.5%
        -- 8th Day = 65%
        -- 9th Day = 77.5%
        -- 10th Day = 90%
        -- 11th Day = 100%
        spawnChance = 150 + ((kills - 3) * 125)
    end

    if kills > 2 and (math.random(1000) <= spawnChance) then
        -- King Behemoth
        SetServerVariable("KingBehemothUP", 1)
        DisallowRespawn(ID.mob.KING_BEHEMOTH, false);
        DisallowRespawn(ID.mob.BEHEMOTH, true);
        UpdateNMSpawnPoint(ID.mob.KING_BEHEMOTH);
        GetMobByID(ID.mob.KING_BEHEMOTH):setRespawnTime(respawn)
        SetServerVariable("HQBehemothRespawn",(os.time() + respawn))
    else
        -- Behemoth
        DisallowRespawn(ID.mob.BEHEMOTH, false)
        DisallowRespawn(ID.mob.KING_BEHEMOTH, true)
        UpdateNMSpawnPoint(ID.mob.BEHEMOTH);
        GetMobByID(ID.mob.BEHEMOTH):setRespawnTime(respawn)
        SetServerVariable("[PH]King_Behemoth", kills)
        SetServerVariable("NQBehemothRespawn",(os.time() + respawn))
    end
end