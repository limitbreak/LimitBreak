-----------------------------------
-- Area: Behemoth's Dominion
--   NM: Picklix Longindex
-- Involved in Quest: The Talekeeper's Gift
-----------------------------------
mixins = {require("scripts/mixins/job_special")}
require("scripts/globals/quests")
-----------------------------------
function onMobSpawn(mob)
	mob:setMod(tpz.mod.STORETP, 500)
	mob:setMod(tpz.mod.SLEEPRES, 200)
	mob:setMod(tpz.mod.LULLABYRES, 200)
end

function onMobFight(mob)
	if mob:getHPP() <= 25 then 
	mob:setMod(tpz.mod.STORETP, 1000)
    end
end
function onMobDeath(mob, player, isKiller)
    if player:getQuestStatus(BASTOK, tpz.quest.id.bastok.THE_TALEKEEPER_S_GIFT) == QUEST_ACCEPTED then
        player:addCharVar("theTalekeepersGiftKilledNM", 1)
    end
end
