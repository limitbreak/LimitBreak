-----------------------------------
-- Area: Phanauet Channel
--  NPC: Luquillaue
-- Type: Adventurer's Assistant
-- !pos 4.066 -4.5 -10.450 1
-----------------------------------
require("scripts/globals/barge")
local timekeeperLocation = tpz.barge.location.BARGE
local timekeeperEventId = 3

function onTrade(player, npc, trade)
end

function onTrigger(player, npc)
    tpz.barge.timekeeperOnTrigger(player, timekeeperLocation, timekeeperEventId)
end

function onEventUpdate(player, csid, option)
end

function onEventFinish(player, csid, option)
end
