-----------------------------------
--
-- Zone: Phanauet_Channel
--
-----------------------------------
local ID = require("scripts/zones/Phanauet_Channel/IDs")
require("scripts/globals/conquest")
require("scripts/globals/barge")
-----------------------------------

function onInitialize(zone)
end

function onZoneIn(player, prevZone)
    local cs = -1
    
    tpz.barge.onZoneIn(player)

    if (player:getXPos() == 0 and player:getYPos() == 0 and player:getZPos() == 0) then
        local position = math.random(-2, 2) + 0.15
        player:setPos(position, -2.000, -1.000, 190)
    end

    return cs
end

function onConquestUpdate(zone, updatetype)
    tpz.conq.onConquestUpdate(zone, updatetype)
end

function onTransportEvent(player, transport)
    player:startEvent(100)
end

function onEventUpdate(player, csid, option)
end

function onEventFinish(player, csid, option)
    if csid == 100 then
        player:setPos(0, 0, 0, 0, tpz.zone.CARPENTERS_LANDING)
    end
end
