-----------------------------------
-- Area: Phanauet Channel (1)
--  NM: Stubborn Dredvodd
-----------------------------------
mixins = 
{
	require("scripts/mixins/job_special")
}
-----------------------------------

function onMobDespawn(mob)
    mob:setRespawnTime(math.random(75600, 86400)) -- 21 - 24 hours
end
