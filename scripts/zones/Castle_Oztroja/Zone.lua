-----------------------------------
--
-- Zone: Castle_Oztroja (151)
--
-----------------------------------
local CASTLE_OZTROJA = require("scripts/zones/Castle_Oztroja/globals")
local ID = require("scripts/zones/Castle_Oztroja/IDs")
require("scripts/globals/conquest")
require("scripts/globals/treasure")
require("scripts/globals/zone")
-----------------------------------

function onInitialize(zone)

    CASTLE_OZTROJA.pickNewCombo() -- update combination for brass door on floor 2
    CASTLE_OZTROJA.pickNewPassword() -- update password for trap door on floor 4

    local TX = GetServerVariable("Tzee_Xicu_UP")
    local tzre = GetServerVariable("Tzee_Xicu_Respawn")
    local yare = GetServerVariable("Yagudo_Avatar_Respawn")

    if TX == 1 then
	UpdateNMSpawnPoint(ID.mob.TZEE_XICU_THE_MANIFEST)
        if os.time() < tzre then
        GetMobByID(ID.mob.TZEE_XICU_THE_MANIFEST):setRespawnTime(tzre - os.time())
        else
            GetMobByID(ID.mob.TZEE_XICU_THE_MANIFEST):setRespawnTime(300)
        end
    else
        UpdateNMSpawnPoint(ID.mob.YAGUDO_AVATAR)
        if os.time() < yare then
            GetMobByID(ID.mob.YAGUDO_AVATAR):setRespawnTime(yare - os.time())
        else
            GetMobByID(ID.mob.YAGUDO_AVATAR):setRespawnTime(300)
        end
    end

    tpz.treasure.initZone(zone)
end


function onZoneIn(player, prevZone)
    local cs = -1
    if player:getXPos() == 0 and player:getYPos() == 0 and player:getZPos() == 0 then
        player:setPos(-162.895, 22.136, -139.923, 2)
    end
    return cs
end

function onConquestUpdate(zone, updatetype)
    tpz.conq.onConquestUpdate(zone, updatetype)
end

function onRegionEnter(player, region)
end

function onGameHour(zone)
    local VanadielHour = VanadielHour()

    -- every game day ...
    if VanadielHour % 24 == 0 then
        CASTLE_OZTROJA.pickNewCombo() -- update combination for brass door on floor 2
        CASTLE_OZTROJA.pickNewPassword() -- update password for trap door on floor 4
    end
end

function onEventUpdate(player, csid, option)
end

function onEventFinish(player, csid, option)
end
