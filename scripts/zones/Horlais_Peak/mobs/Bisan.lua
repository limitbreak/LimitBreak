-----------------------------------
-- Area: Horlais Peak
--  Mob: Bisan
-- BCNM: Carapace Combatants
-----------------------------------

function onMobSpawn(mob)
    mob:setMobMod(tpz.mobMod.NO_MOVE, 1)
    mob:setMobMod(tpz.mobMod.LINK_RADIUS, 30)
    mob:setMobMod(tpz.mobMod.SIGHT_RANGE, 15)
end

function onMobEngaged(mob, target)
    mob:setMobMod(tpz.mobMod.NO_MOVE, 0)
    mob:useMobAbility(341) -- Rhino Guard
end

function onMobDeath(mob, player, isKiller)
end
