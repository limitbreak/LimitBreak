-----------------------------------
-- Area: Spire of Mea
--  Mob: Delver
-----------------------------------
require("scripts/globals/pathfind")
-----------------------------------

function onMobInitialize(mob)
end

function onMobSpawn(mob)
    mob:setMod(tpz.mod.DOUBLE_ATTACK, 20)
	mob:addMod(tpz.mod.DEFP,35)
    mob:delRoamFlag(512)
end

function onMobEngaged(mob,target)
end

function onMobWeaponSkillPrepare(mob, target)
	local roll = math.random()
	if mob:getHPP() <= 35 then
		if roll <= 0.7 then
			return 1234 -- Carousel
			else return 1274 -- Impalement
		end
	end
end

function onMobWeaponSkill(target, mob, skill)
end

function onMobRoam(mob)
	local terrorEndTime = mob:getLocalVar("EmptyTerror")
	if terrorEndTime == 0 then
		return
	elseif terrorEndTime < os.time() then
		mob:setLocalVar("EmptyTerror",0)
		mob:delRoamFlag(512)
		return
	end
	
	-- scripted run around
	mob:addRoamFlag(512) -- ignore attacking
	if not mob:isFollowingPath() then
		mob:disengage()
		local point = {math.random(-249,-230),61,math.random(-8,10)}
		mob:pathThrough(point, tpz.path.flag.RUN)
	end
	
end

function onMobFight(mob,target)
	if mob:getTP() >= 2000 then
		mob:useMobAbility()
	end
	
	local terrorEndTime = mob:getLocalVar("EmptyTerror")
	if terrorEndTime == 0 then
		return
	elseif terrorEndTime < os.time() then
		mob:setLocalVar("EmptyTerror",0)
		mob:delRoamFlag(512)
		return
	end

	-- scripted run around
	mob:addRoamFlag(512) -- ignore attacking
	if not mob:isFollowingPath() then
		mob:disengage()
		local point = {math.random(-249,-230),61,math.random(-8,10)}
		mob:pathThrough(point, tpz.path.flag.RUN)
	end
	
	if mob:getHPP() <= 35 then
       mob:setMod(tpz.mod.STORETP, 250)
    end
	
end

function onMobDeath(mob, player, isKiller)
end
