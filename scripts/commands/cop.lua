---------------------------------------------------------------------------------------------------
-- func: cop
-- courtesy of coreyms/topaz
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 5,
    parameters = "i"
};

function onTrigger(player, zoneId)
    local word  = "";
    local i     = 0;
    local zone  = zoneId;

	if (player:getCharVar( 'inJail' ) == 0) then
		if (zoneId == nil) then
			player:PrintToPlayer("You must enter a zone id.");
			return;
		end
	-- Qufim
		if (zoneId == 1) then
				player:setPos( -249.3749, -20, 320.8015, 128, 126 );
				player:PrintToPlayer("Zone into Delfuts Tower for CS");
			return;
		end
	--  monberaux npc
		if (zoneId == 2) then
				player:setPos( -45, 0, 0, 10, 244 );
				player:PrintToPlayer("Talk with Monberaux");
			return;
		end
	--  Pherimociel in Ru'Lude Gardens
		if (zoneId == 3) then
				player:setPos( -30, 2, 67, 181, 243 );
				player:PrintToPlayer("Talk with Pherimociel");
			return;
		end
	-- Harith Ru'Lude Gardens
		if (zoneId == 4) then
				player:setPos( -1, 2, 133, 114, 243 );
				player:PrintToPlayer("Talk with Harith");
			return;
		end
	-- Spire of Dem zone
		if (zoneId == 5) then
				player:setPos( 0, -2, 289, 225, 19 );
				player:PrintToPlayer("Promy-Dem prepare for fight");
			return;
		end
	-- Spire of holla
		if (zoneId == 6) then
				player:setPos( 1, -1, -271, 192, 17 );
				player:PrintToPlayer("Promy-Holla prepare for fight");
			return;
		end
	-- Spire of Mea
		if (zoneId == 7) then
				player:setPos( -1, -2, 292, 64, 21 );
				player:PrintToPlayer("Promy-Mea prepare for the last fight");
			return;
		end
	-- Lufaise Meadows outside Tav. Stronghold
		if (zoneId == 8) then
				player:setPos( -398, 0, -220, 140, 24 );
				player:PrintToPlayer("Zone into Tav. Stronghold for CS");
			return;
		end
	-- Walnut door inside stronghold
		if (zoneId == 9) then
				player:setPos( 102, -40, -55, 62, 26 );
				player:PrintToPlayer("Touch the Walnut door for CS");
			return;
		end
	-- Inside stronghold outside of Aqueducts
		if (zoneId == 10) then
				player:setPos( 27, -12, 40, 193, 26 );
				player:PrintToPlayer("Touch the door leading to the Aqueducts for CS");
			return;
		end
	-- Near Minotaur
		if (zoneId == 11) then
				player:setPos( 40, 1, 260, 6, 27 );
				player:PrintToPlayer("Watch for aggro, Minotaur fight!!");
			return;
		end
	-- Ladder CS
		if (zoneId == 12) then
				player:setPos( -153, -1, 60, 131, 27 );
				player:PrintToPlayer("Touch ladder for CS");
			return;
		end
	-- Bookshelf ???
		if (zoneId == 13) then
				player:setPos( -73, -24, 90, 200, 27 );
				player:PrintToPlayer("Bookshelf @ F-7, Touch the ??? to open the secret door");
			return;
		end
	-- Secret room
		if (zoneId == 14) then
				player:setPos( -79, -24, 60, 128, 27 );
				player:PrintToPlayer("Touch the Ornate Gate");
			return;
		end
	-- Justinius
		if (zoneId == 15) then
				player:setPos( 76, -34, 68, 203, 26 );
				player:PrintToPlayer("Talk to Justinius J-6");
			return;
		end
    -- Walnut Door (K-7)
        if (zoneId == 16) then
				player:setPos( 112, -41, 42, 254, 26 );
				player:PrintToPlayer("Touch the Walnut door te receive 'Mysterious Amulet' ");
			return;
		end
	-- Justinius
		if (zoneId == 17) then
				player:setPos( 76, -34, 68, 203, 26 );
				player:PrintToPlayer("Talk to Justinius J-6");
			return;
		end
	-- Misareaux Coast - Dilapidated Gate
		if (zoneId == 18) then
				player:setPos( 261, 10, -435, 85, 25 );
				player:PrintToPlayer("Check the Dilapidated Gate for CS");
			return;
		end
        -- Bridge inside stronghold
		if (zoneId == 19) then
				player:setPos( 0, -23, 42, 65, 26 );
				player:PrintToPlayer("Return to Tavnazian Safehold walk toward the Bridge for CS");
			return;
		end
	-- Dilapidated Gate F-7
		if (zoneId == 20) then
				player:setPos( -259, -30, 274, 177, 25 );
				player:PrintToPlayer("Check the Dilapidated Gate and head to D-6 Spatial Displacement");
			return;
		end
 	-- Monarch Linn
		if (zoneId == 21) then
				player:setPos( 12, 0, -539, 127, 31 );
				player:PrintToPlayer("Touch the Spatial Displacement for the Mammets Fight!");
			return;
		end
    -- Port Bastok - Departures entrance
        if (zoneId == 22) then
				player:setPos( -105, -2, -9, 240, 236 );
				player:PrintToPlayer("Walk forward to Departures Entrance for CS");
			return;
		end
	-- Cid
		if (zoneId == 23) then
				player:setPos( -13.7, -10.987, 0, 212, 237 );
				player:PrintToPlayer("Talk with Cid");
			return;
		end
    -- Lower Jeuno - Neptune's Spire
        if (zoneId == 24) then
				player:setPos( 35, -0.1, -14.8, 23, 245 );
				player:PrintToPlayer("Click Neptunes Spire door for CS");
			return;
		end
    -- Ru'Lude Gardens
        if (zoneId == 25) then
                player:setPos( -.005, 2, 30.825, 192, 243 );
				player:PrintToPlayer("Walk up the stairs towards Audience Chamber door for CS");
			return;
		end
	-- Talk to Arnau
		if (zoneId == 26) then
				player:setPos( 147, 0, 137, 231, 231 );
				player:PrintToPlayer("Talk to Arnau");
			return;
		end
    -- Talk to Chasalvige
		if (zoneId == 28) then
				player:setPos( 97.8, 0, 131.272, 171, 231 );
				player:PrintToPlayer("Talk to Chasalvige");
			return;
		end
	-- Carpenter's Landing NM fight
		if (zoneId == 29) then
				player:setPos( -116, -6, -470, 170, 2 );
				player:PrintToPlayer("Talk with Guilloud to spawn NM's, After the fight talk to him again");
			return;
		end
	-- Talk to Hinaree Southern Sandoria
		if (zoneId == 30) then
				player:setPos( -302, -9, 94, 189, 230 );
				player:PrintToPlayer("Talk to Hinaree");
			return;
		end
	-- Windurst Waters and talk to Ohbiru-Dohbiru
		if (zoneId == 31) then
				player:setPos( 22, -5, -193, 44, 238 );
				player:PrintToPlayer("Talk to Ohbiru-Dohbiru - you may have to talk to twice");
			return;
		end
	-- Talk to Yoran-Oran
		if (zoneId == 32) then
				player:setPos( -109, -13, 202, 177, 239 );
				player:PrintToPlayer("Talk to yoran-Oran - you may have to talk to twice");
			return;
		end
	-- Talk to Kyume-Romeh
		if (zoneId == 33) then
				player:setPos(  -57, -3, 22, 216, 238 );
				player:PrintToPlayer("Talk to Kyume-Romeh - you may have to talk to twice");
			return;
		end
	-- Talk to Honoi-Gomoi to recieve a KI
		if (zoneId == 34) then
				player:setPos( -194, -10, -120, 52, 238 );
				player:PrintToPlayer("Talk to Honoi-Gomoi to recieve a KI");
			return;
		end
	-- Talk to Yoran-Oran
		if (zoneId == 35) then
				player:setPos( -109, -13, 202, 177, 239 );
				player:PrintToPlayer("Talk to Yoran-Oran until you get a CS");
			return;
		end
	-- Loose sand Attohwa
		if (zoneId == 36) then
				player:setPos( 480, 20, 37, 174, 7 );
				player:PrintToPlayer("Click the loose sand to pop NM, once killed touch the loose sand for a KI");
			return;
		end
    -- Top of AC mountain
        if (zoneId == 37) then
				player:setPos( 316.76, -25.93, -5.75, 42, 7 );
				player:PrintToPlayer("Click the Cradle of Rebirth to recieve 3 KIs");
			return;
		end
	-- Talk to Yoran-Oran
		if (zoneId == 38) then
				player:setPos( -109, -13, 202, 177, 239 );
				player:PrintToPlayer("Talk to Yoran-Oran until you get a CS");
			return;
		end
	-- Yujuju in Port Windurst
		if (zoneId == 39) then
				player:setPos( 201, -5, 136, 173, 240 );
				player:PrintToPlayer("Talk to Yujuju, You may have to talk twice");
			return;
		end
	-- Tosuka Porika
		if (zoneId == 40) then
				player:setPos( -26, -5, 103, 157, 238 );
				player:PrintToPlayer("Talk to Tosuka Porika");
			return;
		end
	-- Talk to Yoran-Oran
		if (zoneId == 41) then
				player:setPos( -109, -13, 202, 177, 239 );
				player:PrintToPlayer("Talk to Yoran-Oran until you get a CS - you may have to talk to him twice");
			return;
		end
	-- Cid
		if (zoneId == 42) then
				player:setPos( -13.7, -10.987, 0, 212, 237 );
				player:PrintToPlayer("Talk with Cid");
			return;
		end
    -- Lower jueno - Neptune's Spire
        if (zoneId == 43) then
				player:setPos( 35, -0.1, -14.8, 23, 245 );
				player:PrintToPlayer("Click Neptunes Spire door for CS");
			return;
		end
	--  Monberaux npc
		if (zoneId == 44) then
				player:setPos( -45, 0, 0, 10, 244 );
				player:PrintToPlayer("Talk with Monberaux");
			return;
		end
	-- Get a grey chip from the spiders
		if (zoneId == 45) then
				player:setPos( 125, 0, 68, 129, 9);
				player:PrintToPlayer("Get a grey chip from the spiders");
			return;
		end
    -- Lower jueno - neptunes spire
        if (zoneId == 46) then
				player:setPos( 16.9, -0.1, -5.3, 79, 245 );
				player:PrintToPlayer("Talk to Ghebi Damomohe then trade her the grey chip");
			return;
		end
    -- Diablos fight
		if (zoneId == 47) then
				player:setPos( -60, 40, 29, 63, 9 );
				player:PrintToPlayer("Touch the door to receive a CS. Zone into Shrouded Maw and start Diabolos Fight");
			return;
		end
	--  Monberaux npc
		if (zoneId == 48) then
				player:setPos( -45, 0, 0, 10, 244 );
				player:PrintToPlayer("Talk with Monberaux");
			return;
		end
	-- Despachiaire
		if (zoneId == 49) then
				player:setPos( 110, -40, -83, 51, 26 );
				player:PrintToPlayer("Talk to Despachiaire, make sure its CS with Tenzen");
			return;
		end
	-- Justinius
		if (zoneId == 50) then
				player:setPos( 76, -34, 68, 203, 26 );
				player:PrintToPlayer("Talk to Justinius J-6");
			return;
		end
	-- Dilapidated Gate
		if (zoneId == 51) then
				player:setPos( 260, 10, -435, 67, 25 );
				player:PrintToPlayer("Check the Dilapidated Gate");
			return;
		end
	-- Dilaptidated gate to site b
		if (zoneId == 52) then
				player:setPos( -257, -30, 275, 180, 25 );
				player:PrintToPlayer("Check the Dilapidated Gate and head to Rivine Site B");
			return;
		end
	-- BCNM ouryu fight
		if (zoneId == 53) then
				player:setPos( -6, 0, -538, 128, 31 );
				player:PrintToPlayer("BCNM Ouryu fight");
			return;
		end
    -- Justinius
		if (zoneId == 54) then
				player:setPos( 76, -34, 68, 203, 26 );
				player:PrintToPlayer("Talk to Justinius J-6");
			return;
		end
    -- Walnut Door (K-7)
        if (zoneId == 55) then
				player:setPos( 112, -41, 42, 254, 26 );
				player:PrintToPlayer("Touch the Walnut door for a CS");
			return;
		end
	-- Sacrarium entrance
		if (zoneId == 56) then
				player:setPos( 46, -24, 739, 255, 25 );
				player:PrintToPlayer("Click on the door for a CS");
			return;
		end
	-- Wooden Gate G-8
		if (zoneId == 57) then
				player:setPos( 48, -0.6, 10, 124, 28 );
				player:PrintToPlayer("Click the Wooden Gate for a CS");
			return;
		end
	-- First ??? to pop Old Professor
		if (zoneId == 58) then
				player:setPos( 61.6, -2.7, 128.5, 33, 28 );
				player:PrintToPlayer("Click ??? to pop Old Professor.\nIf NM popped proceed to !cop 64");
			return;
		end
	-- Second ??? to pop Old Professor
		if (zoneId == 59) then
				player:setPos( 101, -2.7, 128.5, 33, 28 );
				player:PrintToPlayer("Click ??? to pop Old Professor.\nIf NM popped proceed to !cop 64");
			return;
		end
	-- Third ??? to pop Old Professor
		if (zoneId == 60) then
				player:setPos( 21.6, -2.7, 128.5, 33, 28 );
				player:PrintToPlayer("Click ??? to pop Old Professor.\nIf NM popped proceed to !cop 64");
			return;
		end
	-- Fourth ??? to pop Old Professor
		if (zoneId == 61) then
				player:setPos( 61.6, -2.7, -128.5, 224, 28 );
				player:PrintToPlayer("Click ??? to pop Old Professor.\nIf NM popped proceed to !cop 64");
			return;
		end
	-- Fifth ??? to pop Old Professor
		if (zoneId == 62) then
				player:setPos( 101, -2.7, -128.5, 224, 28 );
				player:PrintToPlayer("Click ??? to pop Old Professor.\nIf NM popped proceed to !cop 64");
			return;
		end
	-- Sixth ??? to pop Old Professor
		if (zoneId == 63) then
				player:setPos( 21.6, -2.7, -128.5, 224, 28 );
				player:PrintToPlayer("Click ??? to pop Old Professor.\nIf NM popped proceed to !cop 64");
			return;
		end
	-- Wooden Gate G-8
		if (zoneId == 64) then
				player:setPos( 48, -0.6, 10, 124, 28 );
				player:PrintToPlayer("Click the Wooden Gate for a CS");
			return;
		end
	-- Despachiaire
		if (zoneId == 65) then
				player:setPos( 107.5, -40, -77, 51, 26 );
				player:PrintToPlayer("Walk towards Despachiaire for a CS with tenzen");
			return;
		end
	-- Sealions Dens Iron Gate
		if (zoneId == 66) then
				player:setPos( 612, 133, 774, 63, 32 );
				player:PrintToPlayer("Touch the Iron Gate for a CS");
			return;
		end
    -- Port Bastok for a CS
        if (zoneId == 67) then
				player:setPos( -105, -2, -9, 240, 236 );
				player:PrintToPlayer("Receieve a cutscene after zoning into Port Bastok");
			return;
		end
	-- Cid
		if (zoneId == 68) then
				player:setPos( -13.7, -10.987, 0, 212, 237 );
				player:PrintToPlayer("Talk with Cid");
			return;
		end
	-- Outside Pso'Xja
		if (zoneId == 69) then
				player:setPos( -340, -100, 137, 191, 111 );
				player:PrintToPlayer("Enter Pso'Xja for a CS");
			return;
		end
	-- Pso'Xja 1st Stone Door
		if (zoneId == 70) then
				player:setPos( -330, 0, 314, 190, 9 );
				player:PrintToPlayer("Click the Stone Door to spawn Nunyunuwi.\nAfter NM dies click the stone door again");
			return;
		end
	-- Pso'Xja 2nd Stone Door
		if (zoneId == 71) then
				player:setPos( -380, 48, 332, 62, 9 );
				player:PrintToPlayer("Click the Stone Door to enter Promy-Vahzl");
			return;
		end
	-- Promy-Vahzl - 3rd Floor Memory Flux
		if (zoneId == 72) then
				player:setPos( 186, 0, -60, 126, 22 );
				player:PrintToPlayer("Click the Memory Flux to spawn Propagator NM.\nAfter NM dies click the Memory Flux for a CS");
			return;
		end
	-- Promy-Vahzl - 4th Floor Memory Flux
		if (zoneId == 73) then
				player:setPos( 414, 0, 140, 0, 22 );
				player:PrintToPlayer("Click the Memory Flux to spawn Solicitor NM.\nAfter NM dies click the Memory Flux for a CS");
			return;
		end
	-- Promy-Vahzl - 5th Floor Memory Flux
		if (zoneId == 74) then
				player:setPos( -334, 0, 140, 127, 22 );
				player:PrintToPlayer("Click the Memory Flux to spawn Ponderer NM.\nAfter NM dies click the Memory Flux for a CS");
			return;
		end
	-- Spire of Vahzl
		if (zoneId == 75) then
				player:setPos( 0, 0, 256, 63, 23 );
				player:PrintToPlayer("Click the Web of Recollections to start Boss Fight");
			return;
		end
	-- Cid
		if (zoneId == 76) then
				player:setPos( -13.7, -10.987, 0, 212, 237 );
				player:PrintToPlayer("Talk with Cid");
			return;
		end
	-- Tenzen path La Theine
		if (zoneId == 77) then
				player:setPos( -180, 8, 246, 196, 102 );
				player:PrintToPlayer("Tenzen path, Click the ??? for CS at G-6");
			return;
		end
	-- Outside Pso'Xja for Tenzen path
		if (zoneId == 78) then
				player:setPos( 266, 0, -18, 140, 111 );
				player:PrintToPlayer("Zone into Pso'Xja for CS");
			return;
		end
	-- Avatar door Tenzen path
		if (zoneId == 79) then
				player:setPos( 281, 32, -60, 121, 9 );
				player:PrintToPlayer("Touch the avatar door for CS");
			return;
		end
	--  Monberaux npc
		if (zoneId == 80) then
				player:setPos( -45, 0, 0, 10, 244 );
				player:PrintToPlayer("Talk with Monberaux for a CS and Envelope from Monberaux KI");
			return;
		end
	--  Pherimociel in Ru'Lude gardens
		if (zoneId == 81) then
				player:setPos( -30, 2, 67, 181, 243 );
				player:PrintToPlayer("Talk with Pherimociel for a CS - may need to talk to him twice");
			return;
		end
	--  Monberaux npc
		if (zoneId == 82) then
				player:setPos( -45, 0, 0, 10, 244 );
				player:PrintToPlayer("Talk with Monberaux(NO CS)");
			return;
		end
	-- Battalia downs ???
		if (zoneId == 83) then
				player:setPos( 423, 8, -162, 46, 105 );
				player:PrintToPlayer("Touch the ??? for cs, then a second time for a KI Delkfutt Recognition Device");
				player:PrintToPlayer("Make sure you examine ??? TWICE");
			return;
		end
	-- Lower Delkfutts gate Tenzen path
		if (zoneId == 84) then
				player:setPos( 459, 0, 130, 54, 184 );
				player:PrintToPlayer("Check the gate to spawn NM, Check after the fight for a cutscene");
			return;
		end
	-- Outside Pso'Xja Tenzen path
		if (zoneId == 85) then
				player:setPos( 59, 0, -324, 67, 111 );
				player:PrintToPlayer("Zone into Pso'Xja to get a CS");
			return;
		end
	-- Avatar Gate
		if (zoneId == 86) then
				player:setPos( 140, 48, -355, 63, 9 );
				player:PrintToPlayer("Check the Avatar Gate");
			return;
		end
	-- Cid
		if (zoneId == 87) then
				player:setPos( -13.7, -10.987, 0, 212, 237 );
				player:PrintToPlayer("Talk with Cid for a CS.\nYOU ARE FINALLY DONE WITH TENZEN PATH!");
			return;
		end
	-- Talk to Hinaree South Sandy
		if (zoneId == 88) then
				player:setPos( -302, -9, 94, 189, 230 );
				player:PrintToPlayer("Talk to Hinaree");
			return;
		end
	-- Zone into Port Sandy
		if (zoneId == 89) then
				player:setPos( -128, 12, 264, 224, 231 );
				player:PrintToPlayer("Talk to Hinaree");
			return;
		end
    -- Yalk to Chasalvige
		if (zoneId == 90) then
				player:setPos( 97.8, 0, 131.272, 171, 231 );
				player:PrintToPlayer("Talk to Chasalvige for a CS");
			return;
		end
    -- Yalk to Kerutoto
		if (zoneId == 91) then
				player:setPos( 10, -4, -159, 226, 238 );
				player:PrintToPlayer("Talk to Kerutoto for a CS");
			return;
		end
	-- Talk to Yoran-Oran
		if (zoneId == 92) then
				player:setPos( -109, -13, 202, 177, 239 );
				player:PrintToPlayer("Talk to Yoran-Oran - you may have to talk to him twice");
			return;
		end
	-- Boneyard Gully fight second path
		if (zoneId == 93) then
				player:setPos( -709, 20, 456, 24, 8 );
				player:PrintToPlayer("Head across the clearing to the north to find a wall of Dark Miasma");
				player:PrintToPlayer("Click on it to enter the Battlefield");
			return;
		end
	-- Bearclaw Pinnacle
		if (zoneId == 94) then
				player:setPos( -680, 18, -540, 129, 6 );
				player:PrintToPlayer("This is another Fight. After head to Cid cop 20");
			return;
		end
	-- Cid
		if (zoneId == 95) then
				player:setPos( -13.7, -10.987, 0, 212, 237 );
				player:PrintToPlayer("Talk with Cid for a CS.\nYOU ARE FINALLY DONE WITH ULMIA PATH!");
			return;
		end
	-- Despachiaire
		if (zoneId == 96) then
				player:setPos( 110, -40, -83, 51, 26 );
				player:PrintToPlayer("Talk to Despachiaire for a CS with Louverance");
			return;
		end
	-- Perih Vashai
		if (zoneId == 97) then
				player:setPos( 117, -3.75, 93, 62, 241 );
				player:PrintToPlayer("Talk to Perih Vashai for a CS with Louverance");
			return;
		end
	-- ??? WARMACHINE
		if (zoneId == 98) then
				player:setPos( -345, -3, -971, 62, 4 );
				player:PrintToPlayer("Talk to ??? Warmachine for a LONG CS. Go grab a drink!");
			return;
		end
	-- Oldton Movalpolos
		if (zoneId == 99) then
				player:setPos( 70, 8, 139, 134, 11 );
				player:PrintToPlayer("You should get a cs, if not zone out and zone back in");
			return;
		end
	-- Mine shaft third path
		if (zoneId == 100) then
				player:setPos( -116, -119, -619, 253, 13 );
				player:PrintToPlayer("Fight, after the fight talk to cid cop 20");
			return;
		end
	-- Cid
		if (zoneId == 101) then
				player:setPos( -13.7, -10.987, 0, 212, 237 );
				player:PrintToPlayer("Talk with Cid for a CS");
			return;
		end
	-- Mine shaft third path
		if (zoneId == 102) then
				player:setPos( -58, -120, -579, 1, 13 );
				player:PrintToPlayer("Trade a Gold Key to the Shaft Entrance for a CS");
			return;
		end
	-- Cid
		if (zoneId == 103) then
				player:setPos( -13.7, -10.987, 0, 212, 237 );
				player:PrintToPlayer("Talk with Cid for a CS");
			return;
		end
    end
end