---------------------------------------------------------------------------------------------------
-- func: zilart
-- desc: guides you through the Zilart missions
-- courtesy of coreyms/topaz
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 5,
    parameters = "i"
};

function onTrigger(player, zoneId)
    local word  = "";
    local i     = 0;
    local zone  = zoneId;

	if (player:getCharVar( 'inJail' ) == 0) then
		if (zoneId == nil) then
			player:PrintToPlayer("You must enter a zone id.");
			return;
		end

	-- Norg entrance pos
		if (zoneId == 1) then
				player:setPos( -60, 0, 412, 54, 176 );
				player:PrintToPlayer("Set your rank to 6 (!setrank 6 <charname>) then zone into Norg.");
			return;
		end

	-- Oaken door
		if (zoneId == 2) then
				player:setPos( 100, -7, -11, 253, 252 );
				player:PrintToPlayer("Click on the door for a cutscene.");
			return;
		end

	-- Jakoh Wahcondalo pos
		if (zoneId == 3) then
				player:setPos( 100, -15, -112, 67, 250 );
				player:PrintToPlayer("Talk to Jakoh Wahcondalo in Kazham to obtain the keyitem Sacrificial Chamber Key.");
			return;
		end

	-- Outside Chamber for fight
		if (zoneId == 4) then
				player:setPos( -83, 46, 61, 125, 160 );
				player:PrintToPlayer("Zone into Sacrificial Chamber for the Tonberry fight.");
			return;
		end

	-- Water Fragment
		if (zoneId == 5) then
				player:setPos( -170, 39, -506, 89, 102 );
                player:PrintToPlayer("Check the Cermet Headstone.");
			return;
		end

	-- Earth Fragment
		if (zoneId == 6 ) then
				player:setPos( -108, 11, -218, 103, 125 );
                player:PrintToPlayer("Check the Cermet Headstone.");
			return;
		end

	-- Ice Fragment
		if (zoneId == 7) then
				player:setPos( 562, 0, 607, 33, 203 );
				player:PrintToPlayer("Check the Cermet Headstone.");
			return;
		end

	-- Wind Fragment (fight)
		if (zoneId == 8) then
				player:setPos( -106, -8, 448, 157, 113 );
				player:PrintToPlayer("Check the Cermet Headstone.\nPrepare for the fight.");
			return;
		end

	-- Light Fragment (fight)
		if (zoneId == 9) then
				player:setPos( 237, 0, 279, 117, 121 );
				player:PrintToPlayer("Check the Cermet Headstone.\nPrepare for the fight.");
			return;
		end

	-- Fire Fragment (fight)
		if (zoneId == 10) then
				player:setPos( 492, 20, 300, 195, 123 );
				player:PrintToPlayer("Check the Cermet Headstone.\nPrepare for the fight.");
			return;
		end

	-- Lightning Fragment (fight)
		if (zoneId == 11) then
				player:setPos( -76, -4, -84, 47, 127 );
                player:PrintToPlayer("Check the Monument for the final fight and fragment.\nYour mission should change now!");
			return;
		end

	-- Chamber of Oracles
		if (zoneId == 12) then
				player:setPos( -981, 17, -288, 195, 208 );
				player:PrintToPlayer("Zone into Chamber of Oracles for the Antican fight.\nOnce you complete this fight put all the fragments into the slots.");
			return;
		end

	-- Top of Delfukts tower
		if (zoneId == 13) then
				player:setPos( -360, -175, -36, 39, 158 );
				player:PrintToPlayer("Walk towards the teleport to zone into the Sellar Fulcrum Again for a battle.\nClick on the Qe'lov Gate for the fight with Kam'lanaut.");
			return;
		end

	-- Oaken door
		if (zoneId == 14) then
				player:setPos( 100, -7, -11, 253, 252 );
				player:PrintToPlayer("Click on the door for a cutscene.");
			return;
		end

	-- Ro'maeve outside hall
		if (zoneId == 15) then
				player:setPos( 0, -32, 123, 62, 122 );
				player:PrintToPlayer("Zone into the Hall of the Gods and head to the door then click it.");
			return;
		end

	-- Oaken door
		if (zoneId == 16) then
				player:setPos( 100, -7, -11, 253, 252 );
				player:PrintToPlayer("Click on the door for a cutscene.");
			return;
		end

	-- Rabao
		if (zoneId == 17) then
				player:setPos( 0, 8, 73, 193, 247 );
				player:PrintToPlayer("Talk to Maryoh and make a deal.");
			return;
		end

	-- Western Altepa
		if (zoneId == 18) then
				player:setPos( -503, 20, -419, 107, 208 );
				player:PrintToPlayer("Touch the ??? for the battle.\nMake sure you get the keyitem Scrap of papyrus after the fight.");
			return;
		end

	-- Rabao
		if (zoneId == 19) then
				player:setPos( 0, 8, 73, 193, 247 );
				player:PrintToPlayer("Talk to Maryoh to obtain the Cerulean Crystal.");
			return;
		end

	-- Ro'maeve outside hall
		if (zoneId == 20) then
				player:setPos( 0, -32, 123, 62, 122 );
				player:PrintToPlayer("Zone into the Hall of the Gods and head to the door then click it.\nAfter the cutscene run straight and click the Shimmering Circle.");
                player:PrintToPlayer("Once the second cutscene is over, walk straight into Ru'Aun Gardens.");
			return;
		end

	-- Divine Might
		if (zoneId == 21) then
				player:setPos( -39, 0, -150, 190, 178 );
				player:PrintToPlayer("Click the unmarked target in the wall for a cutscene./nMake sure you click it a second time after the cutscene.");
			return;
		end

	-- Ark Angels
		if (zoneId == 22) then
				player:setPos( -39, 0, -150, 190, 178 );
				player:PrintToPlayer("To be added soon. Sorry!");
			return;
		end
	else
		player:PrintToPlayer( string.format( "You've been a bad, bad person. You are staying here for a while, kupo!", target ) );
	end
end