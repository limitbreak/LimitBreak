- Updates class list on spells to ERA (e.g: removes RUN, GEO and such from all spells
ROM\118\107.DAT

- Updates cast bar timing to ERA
- Foe Sirvente, Adventurer's Dirge and all Etude and Prelude spells are now party member single target songs, instead of AOE.
ROM\118\114.DAT

- XP Rings text adjusted to ERA values
- Hatchets and Pickaxes available on NPCs will now correctly display stacks of 12 (new DATs required).
- Nation rings mailable flag removed
ROM\118\106.DAT, 108.DAT, 109.DAT, 110.DAT, 114.DAT

- Updates Merit Group2 to ERA
ROM\147\118.DAT

- Guard dialogue for Instant Warp and Reraise Scroll changed to 750 and 500 points respectively
ROM\25\

39 - Southern Sandoria
40 - Northern Sandoria
43 - Bastok Mines
44 - Bastok Markets
45 - Port Bastok
46 - Metalworks
47 - Windurst Waters
49 - Port Windurst
50 - Windurst Woods
52 - Ru'Lude Gardens
53 - Upper Jeuno
54 - Lower Jeuno
55 - Port Jeuno

Search for F0F4F3AE

Šˆ --> µ°°  (reraise)
‰ --> ˆµ°   (warp)
® --> Delete 3 entries (. in pts.)


- ZM8 CS fix for players getting stuck
ROM2\0\16.DAT
ROM2\13\28.DAT, 118.DAT
ROM2\17\112.DAT