-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- ----------
-- GENERAL --
-- ----------

-- Warrior
UPDATE status_effects SET type = "0" WHERE name = "berserk"; -- You should be able to have both active at a time
UPDATE status_effects SET type = "0" WHERE name = "defender"; -- You should be able to have both active at a time

-- Level restriction
UPDATE status_effects SET flags = "276824064", remove_id = "269" WHERE name = "level_restriction";
