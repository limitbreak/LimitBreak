-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- ----------
-- General --
-- ----------

-- Arrapago Reef
INSERT IGNORE INTO bcnm_battlefield VALUES (1091,1,17010722,3); -- Breaking the bonds of fate - COR LB5
INSERT IGNORE INTO bcnm_battlefield VALUES (1091,2,17010723,3); -- Breaking the bonds of fate - COR LB5
INSERT IGNORE INTO bcnm_battlefield VALUES (1091,3,17010724,3); -- Breaking the bonds of fate - COR LB5

-- Balga's Dais
INSERT IGNORE INTO bcnm_battlefield VALUES (113,1,17375443,3); -- Moa Constrictors - KSNM 30
INSERT IGNORE INTO bcnm_battlefield VALUES (113,1,17375444,3); -- Moa Constrictors - KSNM 30
INSERT IGNORE INTO bcnm_battlefield VALUES (113,2,17375446,3); -- Moa Constrictors - KSNM 30
INSERT IGNORE INTO bcnm_battlefield VALUES (113,2,17375447,3); -- Moa Constrictors - KSNM 30
INSERT IGNORE INTO bcnm_battlefield VALUES (113,3,17375449,3); -- Moa Constrictors - KSNM 30
INSERT IGNORE INTO bcnm_battlefield VALUES (113,3,17375450,3); -- Moa Constrictors - KSNM 30

-- Jade Sepulcher
INSERT IGNORE INTO bcnm_battlefield VALUES (1154,1,17051673,3); -- The_Beast_Within - BLU LB5
INSERT IGNORE INTO bcnm_battlefield VALUES (1154,2,17051674,3); -- The_Beast_Within - BLU LB5
INSERT IGNORE INTO bcnm_battlefield VALUES (1154,3,17051675,3); -- The_Beast_Within - BLU LB5

-- Waughroon Shrine
INSERT IGNORE INTO bcnm_battlefield VALUES (68,1,17367063,0); -- A Thief in Norg!? SAM AF3
INSERT IGNORE INTO bcnm_battlefield VALUES (68,2,17367068,0); -- A Thief in Norg!? SAM AF3
INSERT IGNORE INTO bcnm_battlefield VALUES (68,3,17367073,0); -- A Thief in Norg!? SAM AF3

-- CoP 7-5
UPDATE bcnm_battlefield SET conditions = 3 WHERE monsterId = 16908311;
UPDATE bcnm_battlefield SET conditions = 3 WHERE monsterId = 16908312;
UPDATE bcnm_battlefield SET conditions = 3 WHERE monsterId = 16908313;
UPDATE bcnm_battlefield SET conditions = 3 WHERE monsterId = 16908315;
UPDATE bcnm_battlefield SET conditions = 3 WHERE monsterId = 16908316;
UPDATE bcnm_battlefield SET conditions = 3 WHERE monsterId = 16908317;
UPDATE bcnm_battlefield SET conditions = 3 WHERE monsterId = 16908319;
UPDATE bcnm_battlefield SET conditions = 3 WHERE monsterId = 16908320;
UPDATE bcnm_battlefield SET conditions = 3 WHERE monsterId = 16908321;