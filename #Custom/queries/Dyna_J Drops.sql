-- --------------------------------
-- Dynamis-Jeuno SQL Drop Tables --
-- --------------------------------

-- Dynamis Jueno

-- Normal Beastmen Mobs - Vanguards (2543)
DELETE FROM mob_droplist WHERE dropid = "2543" AND droptype != 2; -- Delete
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15102, 72); -- Adds WAR Hands
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15103, 72); -- Adds MNK Hands
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15119, 72); -- Adds WHM Legs
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15135, 72); -- Adds BLM Feet
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15121, 72); -- Adds RDM Legs
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15137, 72); -- Adds THF Feet
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15124, 71); -- Adds DRK Legs
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15141, 71); -- Adds BRD Feet
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15082, 71); -- Adds RNG Head
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15143, 71); -- Adds SAM Feet
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15144, 71); -- Adds NIN Feet
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15115, 71); -- Adds DRG Hands
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 15028, 71); -- Adds COR Hands
INSERT INTO mob_droplist VALUES(2543, 1, 1, 50, 16352, 71); -- Adds PUP Legs
INSERT INTO mob_droplist VALUES(2543, 0, 0, 1000, 1520, 10); -- Adds goblin grease
INSERT INTO mob_droplist VALUES(2543, 0, 0, 1000, 1470, 50); -- Adds sparkling stone
INSERT INTO mob_droplist VALUES(2543, 0, 0, 1000, 1455, 240); -- Adds 1 bill 
INSERT INTO mob_droplist VALUES(2543, 0, 0, 1000, 1449, 150); -- Adds 1 shell 
INSERT INTO mob_droplist VALUES(2543, 0, 0, 1000, 1452, 100); -- Adds 1 coin
INSERT INTO mob_droplist VALUES(2543, 1, 2, 50, 18344, 250); -- Adds relic bow
INSERT INTO mob_droplist VALUES(2543, 1, 2, 50, 18338, 250); -- Adds relic horn 
INSERT INTO mob_droplist VALUES(2543, 1, 2, 50, 18326, 250); -- Adds relic staff
INSERT INTO mob_droplist VALUES(2543, 1, 2, 50, 15066, 250); -- Adds relic shield

-- NM Beastmen Mobs
DELETE FROM mob_droplist WHERE dropid = "143" AND droptype != 2; -- Delete
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15102, 72); -- Adds WAR Hands
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15103, 72); -- Adds MNK Hands
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15119, 72); -- Adds WHM Legs
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15135, 72); -- Adds BLM Feet
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15121, 72); -- Adds RDM Legs
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15137, 72); -- Adds THF Feet
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15124, 71); -- Adds DRK Legs
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15141, 71); -- Adds BRD Feet
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15082, 71); -- Adds RNG Head
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15143, 71); -- Adds SAM Feet
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15144, 71); -- Adds NIN Feet
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15115, 71); -- Adds DRG Hands
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 15028, 71); -- Adds COR Hands
INSERT INTO mob_droplist VALUES(143, 1, 1, 100, 16352, 71); -- Adds PUP Legs
INSERT INTO mob_droplist VALUES(143, 0, 0, 1000, 1520, 10); -- Adds goblin grease
INSERT INTO mob_droplist VALUES(143, 0, 0, 1000, 1470, 50); -- Adds sparkling stone
INSERT INTO mob_droplist VALUES(143, 0, 0, 1000, 1455, 150); -- Adds 1 bill 
INSERT INTO mob_droplist VALUES(143, 0, 0, 1000, 1449, 150); -- Adds 1 shell 
INSERT INTO mob_droplist VALUES(143, 0, 0, 1000, 1452, 150); -- Adds 1 coin 
INSERT INTO mob_droplist VALUES(143, 0, 0, 1000, 1455, 100); -- Adds 1 bill 
INSERT INTO mob_droplist VALUES(143, 0, 0, 1000, 1449, 100); -- Adds 1 shell 
INSERT INTO mob_droplist VALUES(143, 0, 0, 1000, 1452, 100); -- Adds 1 coin 
INSERT INTO mob_droplist VALUES(143, 1, 2, 50, 1456, 333); -- Adds 1 H byne 
INSERT INTO mob_droplist VALUES(143, 1, 2, 50, 1450, 333); -- Adds 1 Lungo 
INSERT INTO mob_droplist VALUES(143, 1, 2, 50, 1453, 334); -- Adds 1 Mont

-- Replicas
DELETE FROM mob_droplist WHERE dropid = "1144"; -- Delete
INSERT INTO mob_droplist VALUES(1144, 0, 0, 1000, 749, 50); -- Add myth coin
INSERT INTO mob_droplist VALUES(1144, 0, 0, 1000, 748, 50); -- Add gold coin
INSERT INTO mob_droplist VALUES(1144, 0, 0, 1000, 1474, 100); -- Add infinity core
INSERT INTO mob_droplist VALUES(1144, 0, 0, 1000, 1470, 50); -- Add sparkling stone
INSERT INTO mob_droplist VALUES(1144, 1, 1, 50, 1456, 333); -- Adds 1 Hbill 
INSERT INTO mob_droplist VALUES(1144, 1, 1, 50, 1450, 333); -- Adds 1 Lungo 
INSERT INTO mob_droplist VALUES(1144, 1, 1, 50, 1453, 334); -- Adds 1 Mont

-- Megaboss
DELETE FROM mob_droplist WHERE dropid = "1085"; -- Delete
INSERT INTO mob_droplist VALUES(1085, 0, 0, 1000, 749, 100); -- Add myth coin
INSERT INTO mob_droplist VALUES(1085, 0, 0, 1000, 748, 100); -- Add gold coin
INSERT INTO mob_droplist VALUES(1085, 0, 0, 1000, 1474, 150); -- Add infinity core
INSERT INTO mob_droplist VALUES(1085, 0, 0, 1000, 1470, 50); -- Add sparkling stone
INSERT INTO mob_droplist VALUES(1085, 0, 0, 1000, 1455, 150); -- add single bill 
INSERT INTO mob_droplist VALUES(1085, 0, 0, 1000, 1449, 150); -- add single shell 
INSERT INTO mob_droplist VALUES(1085, 0, 0, 1000, 1455, 150); -- add single coin 
INSERT INTO mob_droplist VALUES(1085, 0, 0, 1000, 1456, 100); -- add Hbill 
INSERT INTO mob_droplist VALUES(1085, 0, 0, 1000, 1450, 100); -- add Lungo
INSERT INTO mob_droplist VALUES(1085, 0, 0, 1000, 1453, 100); -- add Mont 

-- assign correct drop pools to each mob 
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Smithy";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Alchemist";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Ambusher";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Armorer";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Dragontamer";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Enchanter";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Hitman";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Maestro";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Necromancer";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Pathfinder";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Pitfighter";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Ronin";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Shaman";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Tinkerer";
UPDATE mob_groups SET dropid = "2543" WHERE zoneid = "188" AND name ="Vanguard_Welldigger";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Anvilix_Sootwrists";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Bandrix_Rockjaw";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Blazox_Boneybod";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Bootrix_Jaggedelbow";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Buffrix_Eargone";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Cloktix_Longnail";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Distilix_Stickytoes";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Elixmix_Hooknose";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Eremix_Snottynostril";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Feralox_Honeylips";
UPDATE mob_groups SET dropid = "0" WHERE zoneid = "188" AND name ="Feraloxs_Slime";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Gabblox_Magpietongue";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Hermitrix_Toothrot";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Humnox_Drumbelly";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Jabbrox_Grannyguise";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Jabkix_Pigeonpecs";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Karashix_Swollenskull";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Kikklix_Longlegs";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Lurklox_Dhalmelneck";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Mobpix_Mucousmouth";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Morgmox_Moldnoggin";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Mortilox_Wartpaws";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Prowlox_Barrelbelly";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Quicktrix_Hexhands";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Rutrix_Hamgams";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Scourquix_Scaleskin";
UPDATE mob_groups SET dropid = "0" WHERE zoneid = "188" AND name ="Scourquixs_Wyvern";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Scruffix_Shaggychest";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Slystix_Megapeepers";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Smeltix_Thickhide";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Snypestix_Eaglebeak";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Sparkspox_Sweatbrow";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Ticktox_Beadyeyes";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Trailblix_Goatmug";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Tufflix_Loglimbs";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Tymexox_Ninefingers";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Wasabix_Callusdigit";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Wilywox_Tenderpalm";
UPDATE mob_groups SET dropid = "143" WHERE zoneid = "188" AND name ="Wyrmwix_Snakespecs";
UPDATE mob_groups SET dropid = "1085" WHERE zoneid = "188" AND name ="Goblin_Golem";
UPDATE mob_groups SET dropid = "1144" WHERE zoneid = "188" AND name ="Goblin_Replica";
UPDATE mob_groups SET dropid = "1144" WHERE zoneid = "188" AND name ="Goblin_Statue";

