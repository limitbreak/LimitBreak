-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- ----------
-- General --
-- ----------

UPDATE item_equipment SET jobs = 1185 WHERE name = "crossbow_bolt" AND itemid = 17336; -- Crossbow bolt can be used only by WAR/THF/DRK/RNG
UPDATE item_equipment SET MId = 265 WHERE itemId = 16544; -- ryl.arc._sword was using wrong item model

-- future possible use (OOE)
UPDATE item_equipment SET rslot = 0 WHERE name = "blank_soulplate";
UPDATE item_equipment SET rslot = 0 WHERE name = "h.s._soul_plate";