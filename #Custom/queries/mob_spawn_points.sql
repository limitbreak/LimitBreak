-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- ----------
-- General --
-- ----------

-- The Shrine of Ru'Avitau
UPDATE mob_spawn_points SET pos_x = "739.973", pos_y = "0.400" , pos_z = "-99.555", pos_rot ="1" WHERE mobid = "17506370"; -- Fix faust default pos and rotation.

-- Mammet-19_Epsilon
UPDATE mob_spawn_points SET pos_x = -600.000, pos_y = 82.200, pos_z = 6.176, pos_rot = 253 WHERE mobid = "16904193"; -- Repositioned Mammet-19_Epsilon for era Accuracy
UPDATE mob_spawn_points SET pos_x = -600.000, pos_y = 82.200, pos_z = -6.176, pos_rot = 253 WHERE mobid = "16904194"; -- Repositioned Mammet-19_Epsilon for era Accuracy
UPDATE mob_spawn_points SET pos_x = -600.000, pos_y = 82.200, pos_z = 0.000, pos_rot = 253 WHERE mobid = "16904195"; -- Repositioned Mammet-19_Epsilon for era Accuracy
UPDATE mob_spawn_points SET pos_x = 0.000, pos_y = 2.200, pos_z = 6.176, pos_rot = 253 WHERE mobid = "16904196"; -- Repositioned Mammet-19_Epsilon for era Accuracy
UPDATE mob_spawn_points SET pos_x = 0.000, pos_y = 2.200, pos_z = -6.176, pos_rot = 253 WHERE mobid = "16904197"; -- Repositioned Mammet-19_Epsilon for era Accuracy
UPDATE mob_spawn_points SET pos_x = 0.000, pos_y = 2.200, pos_z = 0.000, pos_rot = 253 WHERE mobid = "16904198"; -- Repositioned Mammet-19_Epsilon for era Accuracy
UPDATE mob_spawn_points SET pos_x = 600.000, pos_y = -77.800, pos_z = 6.176, pos_rot = 253 WHERE mobid = "16904199"; -- Repositioned Mammet-19_Epsilon for era Accuracy
UPDATE mob_spawn_points SET pos_x = 600.000, pos_y = -77.800, pos_z = -6.176, pos_rot = 253 WHERE mobid = "16904200"; -- Repositioned Mammet-19_Epsilon for era Accuracy
UPDATE mob_spawn_points SET pos_x = 600.000, pos_y = -77.800, pos_z = 0.000, pos_rot = 253 WHERE mobid = "16904201"; -- Repositioned Mammet-19_Epsilon for era Accuracy

-- Balga's Dais
UPDATE mob_spawn_points SET pos_x = -140.667, pos_y = 55.587, pos_z = -215.541 WHERE mobid = "17375443"; -- Giant Moa - Moa Constrictors KSNM
UPDATE mob_spawn_points SET pos_x = -144.130, pos_y = 55.662, pos_z = -216.312 WHERE mobid = "17375444"; -- Giant Moa - Moa Constrictors KSNM
UPDATE mob_spawn_points SET pos_x = 18.489, pos_y = -3.855, pos_z = -20.360 WHERE mobid = "17375446"; -- Giant Moa - Moa Constrictors KSNM
UPDATE mob_spawn_points SET pos_x = 16.118, pos_y = -4.307, pos_z = -19.686 WHERE mobid = "17375447"; -- Giant Moa - Moa Constrictors KSNM
UPDATE mob_spawn_points SET pos_x = 0.000, pos_y = 0.000, pos_z = 0.000 WHERE mobid = "17375449"; -- Giant Moa - Moa Constrictors KSNM
UPDATE mob_spawn_points SET pos_x = 0.000, pos_y = 0.000, pos_z = 0.000 WHERE mobid = "17375450"; -- Giant Moa - Moa Constrictors KSNM

-- Ghelsba Outpost
UPDATE mob_spawn_points SET pos_x = -184.000, pos_y = -10.000, pos_z = 45.000, pos_rot = 95 WHERE mobid = "17350933"; -- Kalamainu - Petrifying Pair BCNM
UPDATE mob_spawn_points SET pos_x = -193.000, pos_y = -10.000, pos_z = 55.000, pos_rot = 95 WHERE mobid = "17350934"; -- Kilioa - Petrifying Pair BCNM

-- Horlais Peak
UPDATE mob_spawn_points SET pos_x = -399.000, pos_z = -67.000 WHERE mobid = "17346606"; -- Pilwiz - Carapace Combatants BCNM


-- Jade Sepulcher
INSERT IGNORE INTO `mob_spawn_points` VALUES (17051673,'Raubahn','Raubahn',2270,243,-31,236,159); -- The Beast Within - BLU LB5
INSERT IGNORE INTO `mob_spawn_points` VALUES (17051674,'Raubahn','Raubahn',2270,243,-31,236,159); -- The Beast Within - BLU LB5
INSERT IGNORE INTO `mob_spawn_points` VALUES (17051675,'Raubahn','Raubahn',2270,243,-31,236,159); -- The Beast Within - BLU LB5

-- Korroloka Tunnel
UPDATE mob_spawn_points SET pos_x = -212.056, pos_y = -10.185, pos_z = 178.804 WHERE mobname = "Korroloka_Leech" and mobid = 17486187; -- Slightly repositioned for era accuracy
UPDATE mob_spawn_points SET pos_x = -206.117, pos_y = -11.701, pos_z = 179.106 WHERE mobname = "Korroloka_Leech" and mobid = 17486188; -- Slightly repositioned for era accuracy
UPDATE mob_spawn_points SET pos_x = -204.902, pos_y = -11.361, pos_z = 173.605 WHERE mobname = "Korroloka_Leech" and mobid = 17486189; -- Slightly repositioned for era accuracy

-- Mount Zhayolm
UPDATE mob_spawn_points SET groupid = 56 WHERE mobname = "Wamoura" and mobid = 17027421;  -- Adding missing Wamoura
UPDATE mob_spawn_points SET groupid = 56 WHERE mobname = "Wamoura" and mobid = 17027422;  -- Adding missing Wamoura


-- Quicksand Caves
UPDATE mob_spawn_points SET pos_x = -4.000 WHERE mobname = "Centurio_V-III" and mobid = 17465345; -- ZM6 mobs should be more spread out
UPDATE mob_spawn_points SET pos_x = 4.000 WHERE mobname = "Triarius_V-VIII" and mobid = 17465346; -- ZM6 mobs should be more spread out
UPDATE mob_spawn_points SET pos_x = -3.975 WHERE mobname = "Centurio_V-III" and mobid = 17465348; -- ZM6 mobs should be more spread out
UPDATE mob_spawn_points SET pos_x = 4.025 WHERE mobname = "Triarius_V-VIII" and mobid = 17465349; -- ZM6 mobs should be more spread out
UPDATE mob_spawn_points SET pos_x = -3.945 WHERE mobname = "Centurio_V-III" and mobid = 17465351; -- ZM6 mobs should be more spread out
UPDATE mob_spawn_points SET pos_x = 4.055 WHERE mobname = "Triarius_V-VIII" and mobid = 17465352; -- ZM6 mobs should be more spread out

UPDATE mob_spawn_points SET groupid = 39, pos_x = 672.4598, pos_y = 8.0002, pos_z = -617.2017 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629255; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = 658.8737, pos_y = -2.044, pos_z = -653.5038 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629256; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = 601.4016, pos_y = -5.9207, pos_z = -689.1025 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629261; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = 589.2606, pos_y = -5.6367, pos_z = -671.8018 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629262; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 40, pos_x = 602.7662, pos_y = -5.9207, pos_z = -680.8004 WHERE mobname = "Sabotender_Bailarin" and mobid = 17629264; -- Added Sabotender_Bailarin positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = 540.6154, pos_y = -11.1537, pos_z = -703.0149 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629265; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = 697.7896, pos_y = 9.0821, pos_z = -593.6597 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629266; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = 513.1458, pos_y = 16.245, pos_z = -939.8481 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629312; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = 500.2155, pos_y = 8.1004, pos_z = -912.6168 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629313; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = 470.8165, pos_y = 1.0202, pos_z = -900.7175 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629314; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = 461.4344, pos_y = 1.3031, pos_z = -871.3561 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629340; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = 432.1423, pos_y = -0.0406, pos_z = -858.7776 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629341; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = 492.8908, pos_y = 1.1991, pos_z = -859.9655 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629343; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = 16.6305, pos_y = 0, pos_z = -196.6782 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629345; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = 4.4869, pos_y = -0.0125, pos_z = -219.7973 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629347; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = -26.7504, pos_y = -0.0448, pos_z = -220.134 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629348; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = -18.7748, pos_y = 0.0071, pos_z = -190.9411 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629353; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = -301.1938, pos_y = -6.9023, pos_z = -118.0092 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629354; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = -275.6811, pos_y = -7.0357, pos_z = -99.8262 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629386; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = -248.9263, pos_y = -0.3998, pos_z = -58.7341 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629406; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = -218.9742, pos_y = 5.3132, pos_z = -60.4696 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629407; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = -195.5847, pos_y = 9.6246, pos_z = -103.4818 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629451; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = -212.8878, pos_y = 9.6687, pos_z = -133.536 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629456; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = -181.7648, pos_y = 8.8859, pos_z = -126.5184 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629457; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = -178.2305, pos_y = 12.8125, pos_z = -176.9764 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629459; -- Added Sabotender_Bailaor positions
UPDATE mob_spawn_points SET groupid = 39, pos_x = -164.9852, pos_y = 16.2978, pos_z = -184.8389 WHERE mobname = "Sabotender_Bailaor" and mobid = 17629460; -- Added Sabotender_Bailaor positions

-- Talacca Cove
INSERT IGNORE INTO `mob_spawn_points` VALUES (17010722,'Qultada','Qultada',1980,-180,39,187,61); -- Breaking the Bonds of Fate - Cor LB5
INSERT IGNORE INTO `mob_spawn_points` VALUES (17010723,'Qultada','Qultada',1980,-180,39,187,61); -- Breaking the Bonds of Fate - Cor LB5
INSERT IGNORE INTO `mob_spawn_points` VALUES (17010724,'Qultada','Qultada',1980,-180,39,187,61); -- Breaking the Bonds of Fate - Cor LB5

-- Uleguerand Range
UPDATE mob_spawn_points SET mobname = "Mountain_Worm_NM" WHERE mobid = "16798031"; -- This NM needs a unique name as it shares his with regular worms in Ule. Otherwise it can't be scripted.

-- Zeruhn Mines
INSERT IGNORE INTO `mob_spawn_points` VALUES (17482751,'Giant_Amoeba','Giant Amoeba',8,60.425,8.735,-263.460,181); -- Adding Giant Amoeba NM

-- CoP 5-3U Where Messengers Gather (Boneyard Gully Fight)
UPDATE mob_spawn_points SET pos_x = -565.048, pos_y = 2.755, pos_z = -431.988, pos_rot = 64 WHERE mobid = "16809985"; -- Shikaree_X Arena 1
UPDATE mob_spawn_points SET pos_x = -570.200, pos_y = 3.572, pos_z = -431.988, pos_rot = 64 WHERE mobid = "16809987"; -- Shikaree_Z Arena 1
UPDATE mob_spawn_points SET pos_x = -9.625, pos_y = 3.432, pos_z = 129.045, pos_rot = 64 WHERE mobid = "16809992"; -- Shikaree_X Arena 2
UPDATE mob_spawn_points SET pos_x = -4.473, pos_y = 2.615, pos_z = 129.045, pos_rot = 64 WHERE mobid = "16809990"; -- Shikaree_Z Arena 2
UPDATE mob_spawn_points SET pos_x = 469.424, pos_y = 3.639, pos_z = 611.211, pos_rot = 64 WHERE mobid = "16809997"; -- Shikaree_X Arena 3
UPDATE mob_spawn_points SET pos_x = 474.576, pos_y = 2.822, pos_z = 611.211, pos_rot = 64 WHERE mobid = "16809995"; -- Shikaree_Z Arena 3

-- Spires of Holla
UPDATE mob_spawn_points SET pos_rot = 64 WHERE mobname = "Wreaker"; -- Set all rotations to face forward

-- Ordelles Caves
INSERT IGNORE INTO `mob_spawn_points` VALUES (17568767,'Stroper_Chyme','Stroper Chyme',32,-22.736,28.725,-75.709,64); -- Added 3rd Stroper Chyme
INSERT IGNORE INTO `mob_spawn_points` VALUES (17568766,'Stroper_Chyme','Stroper Chyme',32,-141.509,28.725,-75.709,64); -- Added 4th Stroper Chyme

-- BCNM 20 Wings of Fury
UPDATE mob_spawn_points SET pos_x = -188.2127, pos_y = -10.1180, pos_z = 47.2502, pos_rot = 100 WHERE mobid = "17350929"; -- Colo-colo
UPDATE mob_spawn_points SET pos_x = -182.1980, pos_y = -9.9389, pos_z = 48.6278, pos_rot = 100 WHERE mobid = "17350930"; -- Furies
UPDATE mob_spawn_points SET pos_x = -187.0657, pos_y = -10.4493, pos_z = 53.0073, pos_rot = 100 WHERE mobid = "17350931"; -- Furies

-- SAM AF3
UPDATE mob_spawn_points SET pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 1 WHERE mobname = "Ayakashi";

-- Divine Punishers BCNM
	-- Arena 1
UPDATE mob_spawn_points SET pos_x = -135.5, pos_y = 56.5, pos_z = -224, pos_rot = 190 WHERE mobid = "17375365"; -- Voo_Tolu_the_Ghostfist 1
UPDATE mob_spawn_points SET pos_x = -139.0, pos_y = 56.5, pos_z = -224, pos_rot = 190 WHERE mobid = "17375369"; -- Aa_Nawu_the_Thunderblade 1
UPDATE mob_spawn_points SET pos_x = -142.5, pos_y = 56.5, pos_z = -224, pos_rot = 190 WHERE mobid = "17375370"; -- Yoo_Mihi_the_Haze 1
UPDATE mob_spawn_points SET pos_x = -135.5, pos_y = 56.5, pos_z = -226, pos_rot = 190 WHERE mobid = "17375366"; -- Cuu_Doko_the_Blizzard 1
UPDATE mob_spawn_points SET pos_x = -139.0, pos_y = 56.5, pos_z = -226, pos_rot = 190 WHERE mobid = "17375368"; -- Gii_Jaha_the_Raucous 1
UPDATE mob_spawn_points SET pos_x = -142.5, pos_y = 56.5, pos_z = -226, pos_rot = 190 WHERE mobid = "17375367"; -- Zuu_Xowu_the_Darksmoke 1
	-- Arena 2
UPDATE mob_spawn_points SET pos_x = 24.5, pos_y = -3.5, pos_z = -22.7, pos_rot = 190 WHERE mobid = "17375372"; -- Voo_Tolu_the_Ghostfist 2
UPDATE mob_spawn_points SET pos_x = 21.0, pos_y = -3.5, pos_z = -22.7, pos_rot = 190 WHERE mobid = "17375376"; -- Aa_Nawu_the_Thunderblade 2
UPDATE mob_spawn_points SET pos_x = 17.5, pos_y = -3.5, pos_z = -22.7, pos_rot = 190 WHERE mobid = "17375377"; -- Yoo_Mihi_the_Haze 2
UPDATE mob_spawn_points SET pos_x = 24.5, pos_y = -3.5, pos_z = -25.0, pos_rot = 190 WHERE mobid = "17375373"; -- Cuu_Doko_the_Blizzard 2
UPDATE mob_spawn_points SET pos_x = 21.0, pos_y = -3.5, pos_z = -25.0, pos_rot = 190 WHERE mobid = "17375375"; -- Gii_Jaha_the_Raucous 2
UPDATE mob_spawn_points SET pos_x = 17.5, pos_y = -3.5, pos_z = -25.0, pos_rot = 190 WHERE mobid = "17375374"; -- Zuu_Xowu_the_Darksmoke 2
	-- Arena 3
UPDATE mob_spawn_points SET pos_x = 184.5, pos_y = -63.5, pos_z = 177.0, pos_rot = 190 WHERE mobid = "17375379"; -- Voo_Tolu_the_Ghostfist 3
UPDATE mob_spawn_points SET pos_x = 181.0, pos_y = -63.5, pos_z = 177.0, pos_rot = 190 WHERE mobid = "17375383"; -- Aa_Nawu_the_Thunderblade 3
UPDATE mob_spawn_points SET pos_x = 177.5, pos_y = -63.5, pos_z = 177.0, pos_rot = 190 WHERE mobid = "17375384"; -- Yoo_Mihi_the_Haze 3
UPDATE mob_spawn_points SET pos_x = 184.5, pos_y = -63.5, pos_z = 174.5, pos_rot = 190 WHERE mobid = "17375380"; -- Cuu_Doko_the_Blizzard 3
UPDATE mob_spawn_points SET pos_x = 181.0, pos_y = -63.5, pos_z = 174.5, pos_rot = 190 WHERE mobid = "17375382"; -- Gii_Jaha_the_Raucous 3
UPDATE mob_spawn_points SET pos_x = 177.5, pos_y = -63.5, pos_z = 174.5, pos_rot = 190 WHERE mobid = "17375381"; -- Zuu_Xowu_the_Darksmoke 3

-- Promy Holla Zone Rework
UPDATE mob_spawn_points SET pos_x = 104.9824, pos_y = 0, pos_z = -238.7194, pos_rot = 53 WHERE mobid = "16842996";
UPDATE mob_spawn_points SET pos_x = 104.6884, pos_y = 0, pos_z = -236.7257, pos_rot = 99 WHERE mobid = "16843010";
UPDATE mob_spawn_points SET pos_x = -4.4, pos_y = 0, pos_z = 166.9161, pos_rot = 22 WHERE mobid = "16842762";
UPDATE mob_spawn_points SET pos_x = -6.8225, pos_y = 0, pos_z = 155.3888, pos_rot = 30 WHERE mobid = "16842761";
UPDATE mob_spawn_points SET pos_x = -31.1769, pos_y = 0, pos_z = 128.5388, pos_rot = 61 WHERE mobid = "16842763";
UPDATE mob_spawn_points SET pos_x = -207.4494, pos_y = 0, pos_z = -85.8353, pos_rot = 122 WHERE mobid = "16842829";
UPDATE mob_spawn_points SET pos_x = -214.4471, pos_y = 0, pos_z = -92.8047, pos_rot = 60 WHERE mobid = "16842811";
UPDATE mob_spawn_points SET pos_x = -219.357, pos_y = 0, pos_z = -72.8066, pos_rot = 217 WHERE mobid = "16842810";
UPDATE mob_spawn_points SET pos_x = 202.3691, pos_y = 0, pos_z = -180.1865, pos_rot = 56 WHERE mobid = "16843023";
UPDATE mob_spawn_points SET pos_x = 58.102, pos_y = 0, pos_z = 297.2291, pos_rot = 52 WHERE mobid = "16842921";
UPDATE mob_spawn_points SET pos_x = 168.0189, pos_y = 0, pos_z = 338.3727, pos_rot = 0 WHERE mobid = "16842935";
UPDATE mob_spawn_points SET pos_x = 161.086, pos_y = 0, pos_z = -207.6107, pos_rot = 241 WHERE mobid = "16843015";
UPDATE mob_spawn_points SET pos_x = 159.2483, pos_y = 0, pos_z = -195.7994, pos_rot = 202 WHERE mobid = "16843013";
UPDATE mob_spawn_points SET pos_x = 277.5939, pos_y = 0, pos_z = -150.8496, pos_rot = 102 WHERE mobid = "16843026";
UPDATE mob_spawn_points SET pos_x = 251.4929, pos_y = 0, pos_z = -82.5969, pos_rot = 125 WHERE mobid = "16843031";
UPDATE mob_spawn_points SET pos_x = 211.6537, pos_y = 0, pos_z = -50.2975, pos_rot = 100 WHERE mobid = "16843032";
UPDATE mob_spawn_points SET pos_x = 217.7825, pos_y = 0, pos_z = -40.2925, pos_rot = 80 WHERE mobid = "16843033";
UPDATE mob_spawn_points SET pos_x = 124.7702, pos_y = 0, pos_z = -41.2027, pos_rot = 30 WHERE mobid = "16843034";
UPDATE mob_spawn_points SET pos_x = 2.6838, pos_y = 0, pos_z = -185.5633, pos_rot = 119 WHERE mobid = "16843039";
UPDATE mob_spawn_points SET pos_x = -6.7434, pos_y = 0, pos_z = -174.3297, pos_rot = 213 WHERE mobid = "16842808";
UPDATE mob_spawn_points SET pos_x = -7.31, pos_y = 0, pos_z = -203.4486, pos_rot = 162 WHERE mobid = "16842807";
UPDATE mob_spawn_points SET pos_x = 88.0521, pos_y = 0, pos_z = -95.7068, pos_rot = 126 WHERE mobid = "16842998";

-- Promy Dem Zone Rework
UPDATE mob_spawn_points SET pos_x = -281.4977, pos_y = 0, pos_z = -147.937, pos_rot = 194 WHERE mobid = "16851007";
UPDATE mob_spawn_points SET pos_x = 28.5979, pos_y = 0, pos_z = -242.4267, pos_rot = 90 WHERE mobid = "16851111";
UPDATE mob_spawn_points SET pos_x = 38.9762, pos_y = 0, pos_z = -249.727, pos_rot = 30 WHERE mobid = "16851115";
UPDATE mob_spawn_points SET pos_x = -224.9102, pos_y = 0, pos_z = 298.0087, pos_rot = 53 WHERE mobid = "";
UPDATE mob_spawn_points SET pos_x = -281.4455, pos_y = 0, pos_z = -148.3793, pos_rot = 180 WHERE mobid = "16851021";
UPDATE mob_spawn_points SET pos_x = 65.9505, pos_y = 0, pos_z = 75.2799, pos_rot = 250 WHERE mobid = "16851234";
UPDATE mob_spawn_points SET pos_x = 72.0093, pos_y = 0, pos_z = 109.3414, pos_rot = 170 WHERE mobid = "16851237";
UPDATE mob_spawn_points SET pos_x = 105.1381, pos_y = 0, pos_z = 145.7895, pos_rot = 218 WHERE mobid = "16851238";
UPDATE mob_spawn_points SET pos_x = 92.5125, pos_y = 0, pos_z = 113.3645, pos_rot = 175 WHERE mobid = "16851241";
UPDATE mob_spawn_points SET pos_x = 158.7571, pos_y = 0, pos_z = -260.9823, pos_rot = 50 WHERE mobid = "16850965";
UPDATE mob_spawn_points SET pos_x = 152.3412, pos_y = 0, pos_z = -274.252, pos_rot = 105 WHERE mobid = "16850964";
UPDATE mob_spawn_points SET pos_x = -241.0292, pos_y = 0, pos_z = 321.1828, pos_rot = 214 WHERE mobid = "16851106";
UPDATE mob_spawn_points SET pos_x = -230.8023, pos_y = 0, pos_z = 329.0938, pos_rot = 180 WHERE mobid = "16851103";
UPDATE mob_spawn_points SET pos_x = -159.966, pos_y = 0, pos_z = 253.1593, pos_rot = 64 WHERE mobid = "16851120";
UPDATE mob_spawn_points SET pos_x = -135.457, pos_y = 0, pos_z = 255.6546, pos_rot = 60 WHERE mobid = "16851116";
UPDATE mob_spawn_points SET pos_x = -39.9547, pos_y = 0, pos_z = 197.9651, pos_rot = 180 WHERE mobid = "16851128";
UPDATE mob_spawn_points SET pos_x = 137.2684, pos_y = 0, pos_z = 144.2999, pos_rot = 13 WHERE mobid = "16851233";
UPDATE mob_spawn_points SET pos_x = 131.069, pos_y = 0, pos_z = 210.4403, pos_rot = 203 WHERE mobid = "16851182";

-- Promy Mea Zone Rework
UPDATE mob_spawn_points SET pos_x = -7.9353, pos_y = 0, pos_z = 205.5068, pos_rot = 173 WHERE mobid = "16859403";
UPDATE mob_spawn_points SET pos_x = 15.6043, pos_y = 0, pos_z = 211.0029, pos_rot = 239 WHERE mobid = "16859405";
UPDATE mob_spawn_points SET pos_x = 32.8088, pos_y = 0, pos_z = 233.1867, pos_rot = 60 WHERE mobid = "16859414";
UPDATE mob_spawn_points SET pos_x = 18.6954, pos_y = 0, pos_z = 268.584, pos_rot = 90 WHERE mobid = "16859432";
UPDATE mob_spawn_points SET pos_x = -264.5457, pos_y = 0, pos_z = 15.9821, pos_rot = 214 WHERE mobid = "16859239";
UPDATE mob_spawn_points SET pos_x = -56.3249, pos_y = 0, pos_z = 29.2449, pos_rot = 152 WHERE mobid = "16859245";
UPDATE mob_spawn_points SET pos_x = -39.6501, pos_y = 0, pos_z = 37.2909, pos_rot = 148 WHERE mobid = "16859235";
UPDATE mob_spawn_points SET pos_x = -66.8032, pos_y = 0, pos_z = 53.6121, pos_rot = 101 WHERE mobid = "16859237";
UPDATE mob_spawn_points SET pos_x = 51.262, pos_y = 0, pos_z = 225.4719, pos_rot = 113 WHERE mobid = "16859400";
UPDATE mob_spawn_points SET pos_x = 0.3369, pos_y = 0, pos_z = 205.426, pos_rot = 82 WHERE mobid = "16859440";
UPDATE mob_spawn_points SET pos_x = -11.6207, pos_y = 0, pos_z = 272.4629, pos_rot = 13 WHERE mobid = "16859406";
UPDATE mob_spawn_points SET pos_x = -5.8938, pos_y = 0, pos_z = 290.7134, pos_rot = 165 WHERE mobid = "16859439";
UPDATE mob_spawn_points SET pos_x = 28.1076, pos_y = 0, pos_z = 281.0533, pos_rot = 88 WHERE mobid = "16859424";
UPDATE mob_spawn_points SET pos_x = 63.3434, pos_y = 0, pos_z = 231.1371, pos_rot = 220 WHERE mobid = "16859418";

-- Bostaunieux Oubliette
UPDATE mob_spawn_points SET pos_x = -21.04, pos_y = 0.9251, pos_z = -340.1736, pos_rot = 127 WHERE mobid = "17461307";

-- CoP 6
UPDATE mob_spawn_points SET pos_rot = 62 WHERE mobname = "Snoll_Tzar";

-- CoP 7-5
UPDATE mob_spawn_points SET pos_x = -639.985, pos_y = -231.347, pos_z = 525.910, pos_rot = 65	WHERE mobname = "Tenzen" AND mobid = 16908310;
UPDATE mob_spawn_points SET pos_x = 0.0000, pos_y = -151.347, pos_z = 166.000, pos_rot = 65	WHERE mobname = "Tenzen" AND mobid = 16908314;
UPDATE mob_spawn_points SET pos_x = 639.980, pos_y = -71.347, pos_z = -193.848, pos_rot = 65	WHERE mobname = "Tenzen" AND mobid = 16908318;

-- CoP 6-4
UPDATE mob_spawn_points SET pos_rot = 190 WHERE mobname = "Omega" AND mobid = 16908294;
UPDATE mob_spawn_points SET pos_rot = 190 WHERE mobname = "Ultima" AND mobid = 16908295;
UPDATE mob_spawn_points SET pos_rot = 190 WHERE mobname = "Omega" AND mobid = 16908301;
UPDATE mob_spawn_points SET pos_rot = 190 WHERE mobname = "Ultima" AND mobid = 16908302;
UPDATE mob_spawn_points SET pos_rot = 190 WHERE mobname = "Omega" AND mobid = 16908308;
UPDATE mob_spawn_points SET pos_rot = 190 WHERE mobname = "Ultima" AND mobid = 16908309;

-- Sea Positions/Groups
UPDATE mob_spawn_points SET mobname = "Ulaern", polutils_name = "Ul\'aern", groupid = 1, pos_x = -24.763, pos_y = -0.501, pos_z = -501.989, pos_rot = 4 WHERE mobid = 16912385;
UPDATE mob_spawn_points SET mobname = "Ulaern", polutils_name = "Ul\'aern", groupid = 1, pos_x = -39.68, pos_y = -0.5, pos_z = -581.753, pos_rot = 141 WHERE mobid = 16912386;
UPDATE mob_spawn_points SET mobname = "Ulaern", polutils_name = "Ul\'aern", groupid = 59, pos_x = -77.701, pos_y = -0.5, pos_z = -501.892, pos_rot = 201 WHERE mobid = 16912387;
UPDATE mob_spawn_points SET mobname = "Ulaern", polutils_name = "Ul\'aern", groupid = 3, pos_x = -45.578, pos_y = -0.5, pos_z = -584.298, pos_rot = 174 WHERE mobid = 16912388;
UPDATE mob_spawn_points SET mobname = "Ulaern", polutils_name = "Ul\'aern", groupid = 4, pos_x = -29.022, pos_y = -0.5, pos_z = -553.204, pos_rot = 122 WHERE mobid = 16912389;
UPDATE mob_spawn_points SET mobname = "Ulaern", polutils_name = "Ul\'aern", groupid = 60, pos_x = -50.796, pos_y = -0.499, pos_z = -582.007, pos_rot = 234 WHERE mobid = 16912390;
UPDATE mob_spawn_points SET mobname = "Ulaern", polutils_name = "Ul\'aern", groupid = 6, pos_x = -20.686, pos_y = -0.5, pos_z = -567.651, pos_rot = 159 WHERE mobid = 16912391;
UPDATE mob_spawn_points SET mobname = "Ulaern", polutils_name = "Ul\'aern", groupid = 2, pos_x = -0.802, pos_y = -1.055, pos_z = -542.784, pos_rot = 199 WHERE mobid = 16912392;
UPDATE mob_spawn_points SET mobname = "Ulaern", polutils_name = "Ul\'aern", groupid = 2, pos_x = 22.807, pos_y = -0.5, pos_z = -548.529, pos_rot = 193 WHERE mobid = 16912393;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = -1.608, pos_y = -0.5, pos_z = -554.041, pos_rot = 139 WHERE mobid = 16912394;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = 0.86, pos_y = -0.5, pos_z = -555.119, pos_rot = 108 WHERE mobid = 16912395;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = -8.315, pos_y = -0.5, pos_z = -552.733, pos_rot = 139 WHERE mobid = 16912396;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = 11.8, pos_y = -0.5, pos_z = -579.298, pos_rot = 181 WHERE mobid = 16912397;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = 44.08, pos_y = -0.5, pos_z = -581.038, pos_rot = 110 WHERE mobid = 16912398;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = -22.492, pos_y = -0.5, pos_z = -514.307, pos_rot = 223 WHERE mobid = 16912399;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = -62.027, pos_y = -0.5, pos_z = -538.811, pos_rot = 16 WHERE mobid = 16912400;
UPDATE mob_spawn_points SET mobname = "Ulphuabo", polutils_name = "Ul\'phuabo", groupid = 9, pos_x = -58.025, pos_y = -0.5, pos_z = -572.647, pos_rot = 44 WHERE mobid = 16912401;
UPDATE mob_spawn_points SET mobname = "Ulphuabo", polutils_name = "Ul\'phuabo", groupid = 9, pos_x = 23.438, pos_y = -0.237, pos_z = -549.093, pos_rot = 159 WHERE mobid = 16912402;
UPDATE mob_spawn_points SET mobname = "Ulaern", polutils_name = "Ul\'aern", groupid = 10, pos_x = -87.005, pos_y = -0.5, pos_z = -668.973, pos_rot = 94 WHERE mobid = 16912403;
UPDATE mob_spawn_points SET mobname = "Ulaern", polutils_name = "Ul\'aern", groupid = 59, pos_x = -20.498, pos_y = -0.5, pos_z = -624.702, pos_rot = 238 WHERE mobid = 16912404;
UPDATE mob_spawn_points SET mobname = "Ulaern", polutils_name = "Ul\'aern", groupid = 11, pos_x = -22.923, pos_y = -0.5, pos_z = -740.9, pos_rot = 45 WHERE mobid = 16912405;
UPDATE mob_spawn_points SET mobname = "Ulaern", polutils_name = "Ul\'aern", groupid = 12, pos_x = -40.198, pos_y = -0.5, pos_z = -720.885, pos_rot = 6 WHERE mobid = 16912406;
UPDATE mob_spawn_points SET mobname = "Aerns_Wynav", polutils_name = "Aern\'s Wynav", groupid = 13, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 210 WHERE mobid = 16912407;
UPDATE mob_spawn_points SET mobname = "Aerns_Xzomit", polutils_name = "Aern\'s Xzomit", groupid = 14, pos_x = -72.695, pos_y = -0.017, pos_z = -643.771, pos_rot = 94 WHERE mobid = 16912408;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = -34.735, pos_y = -0.5, pos_z = -659.095, pos_rot = 238 WHERE mobid = 16912409;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = -34.067, pos_y = -0.5, pos_z = -658.984, pos_rot = 237 WHERE mobid = 16912410;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 57, pos_x = -38.326, pos_y = -0.5, pos_z = -658.327, pos_rot = 246 WHERE mobid = 16912411;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = -32.037, pos_y = -0.5, pos_z = -696.974, pos_rot = 165 WHERE mobid = 16912412;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = -56.806, pos_y = -0.499, pos_z = -637.281, pos_rot = 200 WHERE mobid = 16912413;
UPDATE mob_spawn_points SET mobname = "Ulphuabo", polutils_name = "Ul\'phuabo", groupid = 9, pos_x = -12.123, pos_y = -0.5, pos_z = -658.614, pos_rot = 9 WHERE mobid = 16912414;
UPDATE mob_spawn_points SET mobname = "Ulaern", polutils_name = "Ul\'aern", groupid = 15, pos_x = 53.087, pos_y = -0.5, pos_z = -726.582, pos_rot = 85 WHERE mobid = 16912415;
UPDATE mob_spawn_points SET mobname = "Ulaern", polutils_name = "Ul\'aern", groupid = 16, pos_x = 99.068, pos_y = -0.5, pos_z = -683.215, pos_rot = 65 WHERE mobid = 16912416;
UPDATE mob_spawn_points SET mobname = "Ulaern", polutils_name = "Ul\'aern", groupid = 5, pos_x = 27.853, pos_y = -0.5, pos_z = -712.223, pos_rot = 106 WHERE mobid = 16912417;
UPDATE mob_spawn_points SET mobname = "Ulaern", polutils_name = "Ul\'aern", groupid = 17, pos_x = 38.617, pos_y = -0.5, pos_z = -665.826, pos_rot = 181 WHERE mobid = 16912418;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = 93.923, pos_y = -0.5, pos_z = -674.789, pos_rot = 123 WHERE mobid = 16912419;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = 53.203, pos_y = -0.5, pos_z = -703.2, pos_rot = 66 WHERE mobid = 16912420;
UPDATE mob_spawn_points SET mobname = "Aerns_Elemental", polutils_name = "Aern\'s Elemental", groupid = 18, pos_x = -0.282, pos_y = -4.256, pos_z = -725.572, pos_rot = 211 WHERE mobid = 16912421;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = 31.636, pos_y = -0.5, pos_z = -659.626, pos_rot = 142 WHERE mobid = 16912422;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = 33.251, pos_y = -0.5, pos_z = -659.38, pos_rot = 140 WHERE mobid = 16912423;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 57, pos_x = 25.142, pos_y = -0.5, pos_z = -659.67, pos_rot = 154 WHERE mobid = 16912424;
UPDATE mob_spawn_points SET mobname = "Ulphuabo", polutils_name = "Ul\'phuabo", groupid = 9, pos_x = 28.091, pos_y = -0.5, pos_z = -685.779, pos_rot = 154 WHERE mobid = 16912425;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = 139.714, pos_y = -0.86, pos_z = -663.798, pos_rot = 164 WHERE mobid = 16912426;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = 141.232, pos_y = -1.05, pos_z = -666.071, pos_rot = 164 WHERE mobid = 16912427;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = 186.926, pos_y = -0.499, pos_z = -580.286, pos_rot = 223 WHERE mobid = 16912428;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = 210.69, pos_y = -0.5, pos_z = -562.239, pos_rot = 238 WHERE mobid = 16912429;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = 158.189, pos_y = -0.499, pos_z = -657.018, pos_rot = 31 WHERE mobid = 16912430;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = 157.581, pos_y = -0.499, pos_z = -656.016, pos_rot = 34 WHERE mobid = 16912431;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = 100.029, pos_y = -0.757, pos_z = -603.27, pos_rot = 229 WHERE mobid = 16912432;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = 94.734, pos_y = -0.707, pos_z = -605.512, pos_rot = 243 WHERE mobid = 16912433;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 57, pos_x = 138.119, pos_y = -0.664, pos_z = -661.993, pos_rot = 160 WHERE mobid = 16912434;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 57, pos_x = 202.714, pos_y = -0.499, pos_z = -550.121, pos_rot = 151 WHERE mobid = 16912435;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 57, pos_x = 171.315, pos_y = -0.499, pos_z = -670.27, pos_rot = 30 WHERE mobid = 16912436;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 57, pos_x = 130.299, pos_y = -0.546, pos_z = -629.094, pos_rot = 107 WHERE mobid = 16912437;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = 172.548, pos_y = -0.499, pos_z = -635.509, pos_rot = 25 WHERE mobid = 16912438;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = 206.409, pos_y = -0.499, pos_z = -608.949, pos_rot = 126 WHERE mobid = 16912439;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = 157.499, pos_y = -0.499, pos_z = -664.463, pos_rot = 13 WHERE mobid = 16912440;
UPDATE mob_spawn_points SET mobname = "Ulphuabo", polutils_name = "Ul\'phuabo", groupid = 9, pos_x = 190.93, pos_y = -0.499, pos_z = -640.37, pos_rot = 234 WHERE mobid = 16912441;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = 234.833, pos_y = -0.499, pos_z = -735.856, pos_rot = 151 WHERE mobid = 16912442;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = 235.61, pos_y = -0.499, pos_z = -698.517, pos_rot = 94 WHERE mobid = 16912443;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = 236.188, pos_y = -0.499, pos_z = -697.019, pos_rot = 92 WHERE mobid = 16912444;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = 230.462, pos_y = -0.499, pos_z = -667.986, pos_rot = 62 WHERE mobid = 16912445;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = 230.494, pos_y = -0.499, pos_z = -664.068, pos_rot = 62 WHERE mobid = 16912446;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 57, pos_x = 228.724, pos_y = -0.499, pos_z = -703.536, pos_rot = 84 WHERE mobid = 16912447;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 57, pos_x = 255.556, pos_y = -0.499, pos_z = -688.614, pos_rot = 49 WHERE mobid = 16912448;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = 281.07, pos_y = -0.499, pos_z = -572.823, pos_rot = 106 WHERE mobid = 16912449;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = 279.134, pos_y = -0.499, pos_z = -572.686, pos_rot = 118 WHERE mobid = 16912450;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = 308.827, pos_y = -0.499, pos_z = -611.402, pos_rot = 116 WHERE mobid = 16912451;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = 306.326, pos_y = -0.499, pos_z = -612.428, pos_rot = 77 WHERE mobid = 16912452;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = 320.136, pos_y = -0.499, pos_z = -554.129, pos_rot = 42 WHERE mobid = 16912453;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = 319.462, pos_y = -0.499, pos_z = -553.309, pos_rot = 43 WHERE mobid = 16912454;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 57, pos_x = 274.168, pos_y = -0.499, pos_z = -572.618, pos_rot = 217 WHERE mobid = 16912455;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 57, pos_x = 303.175, pos_y = -0.499, pos_z = -617.959, pos_rot = 84 WHERE mobid = 16912456;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 57, pos_x = 321.431, pos_y = -0.499, pos_z = -560.639, pos_rot = 42 WHERE mobid = 16912457;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = 307.136, pos_y = -0.499, pos_z = -585.903, pos_rot = 72 WHERE mobid = 16912458;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = 257.372, pos_y = -0.499, pos_z = -616.559, pos_rot = 203 WHERE mobid = 16912459;
UPDATE mob_spawn_points SET mobname = "Ulphuabo", polutils_name = "Ul\'phuabo", groupid = 9, pos_x = 271.481, pos_y = -0.499, pos_z = -614.603, pos_rot = 125 WHERE mobid = 16912460;
UPDATE mob_spawn_points SET mobname = "Ulphuabo", polutils_name = "Ul\'phuabo", groupid = 9, pos_x = 185.878, pos_y = -0.499, pos_z = -487.157, pos_rot = 19 WHERE mobid = 16912461;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = 388.248, pos_y = -0.499, pos_z = -375.957, pos_rot = 114 WHERE mobid = 16912462;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = 374.112, pos_y = -0.499, pos_z = -433.206, pos_rot = 251 WHERE mobid = 16912463;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = 231.123, pos_y = -0.499, pos_z = -421.224, pos_rot = 142 WHERE mobid = 16912464;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = 342.278, pos_y = -0.499, pos_z = -354.816, pos_rot = 72 WHERE mobid = 16912465;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = 257.716, pos_y = -0.499, pos_z = -431.252, pos_rot = 8 WHERE mobid = 16912466;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = 419.838, pos_y = -0.5, pos_z = -361.984, pos_rot = 30 WHERE mobid = 16912467;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = 280.324, pos_y = -0.499, pos_z = -440.002, pos_rot = 175 WHERE mobid = 16912468;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = 277.908, pos_y = -0.499, pos_z = -407.427, pos_rot = 189 WHERE mobid = 16912469;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 57, pos_x = 283.345, pos_y = -0.499, pos_z = -429.312, pos_rot = 190 WHERE mobid = 16912470;
UPDATE mob_spawn_points SET mobname = "Ulphuabo", polutils_name = "Ul\'phuabo", groupid = 9, pos_x = 371.045, pos_y = -0.499, pos_z = -365.368, pos_rot = 41 WHERE mobid = 16912471;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 19, pos_x = 584.122, pos_y = -0.5, pos_z = -363.099, pos_rot = 225 WHERE mobid = 16912472;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 20, pos_x = 557.388, pos_y = -0.409, pos_z = -351.886, pos_rot = 115 WHERE mobid = 16912473;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 21, pos_x = 478.811, pos_y = -0.5, pos_z = -349.119, pos_rot = 13 WHERE mobid = 16912474;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 22, pos_x = 485.983, pos_y = -0.5, pos_z = -391.483, pos_rot = 38 WHERE mobid = 16912475;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 549.112, pos_y = -0.5, pos_z = -391.86, pos_rot = 129 WHERE mobid = 16912476;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 530.503, pos_y = -0.5, pos_z = -349.26, pos_rot = 105 WHERE mobid = 16912477;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 513.708, pos_y = -0.5, pos_z = -389.999, pos_rot = 227 WHERE mobid = 16912478;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 541.597, pos_y = -0.5, pos_z = -324.671, pos_rot = 46 WHERE mobid = 16912479;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 486.97, pos_y = -0.5, pos_z = -373.175, pos_rot = 44 WHERE mobid = 16912480;
UPDATE mob_spawn_points SET mobname = "Aerns_Wynav", polutils_name = "Aern\'s Wynav", groupid = 13, pos_x = 531.341, pos_y = -0.278, pos_z = -392.442, pos_rot = 217 WHERE mobid = 16912481;
UPDATE mob_spawn_points SET mobname = "Aerns_Elemental", polutils_name = "Aern\'s Elemental", groupid = 18, pos_x = 517.486, pos_y = -0.5, pos_z = -378.694, pos_rot = 135 WHERE mobid = 16912482;
UPDATE mob_spawn_points SET mobname = "Aerns_Xzomit", polutils_name = "Aern\'s Xzomit", groupid = 14, pos_x = 557.229, pos_y = -0.208, pos_z = -351.301, pos_rot = 213 WHERE mobid = 16912483;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 514.293, pos_y = -0.5, pos_z = -339.126, pos_rot = 88 WHERE mobid = 16912484;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 509.832, pos_y = -0.5, pos_z = -367.248, pos_rot = 186 WHERE mobid = 16912485;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 482.654, pos_y = -0.5, pos_z = -343.645, pos_rot = 132 WHERE mobid = 16912486;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 32, pos_x = 552.028, pos_y = -1.297, pos_z = -514.484, pos_rot = 27 WHERE mobid = 16912487;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 19, pos_x = 534.536, pos_y = -0.235, pos_z = -540.016, pos_rot = 2 WHERE mobid = 16912488;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 26, pos_x = 522.335, pos_y = -0.189, pos_z = -445.636, pos_rot = 242 WHERE mobid = 16912489;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 27, pos_x = 504.906, pos_y = -0.5, pos_z = -526.107, pos_rot = 198 WHERE mobid = 16912490;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 27, pos_x = 520.481, pos_y = -0.5, pos_z = -477.321, pos_rot = 67 WHERE mobid = 16912491;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 28, pos_x = 528.92, pos_y = -0.5, pos_z = -487.953, pos_rot = 214 WHERE mobid = 16912492;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 28, pos_x = 486.896, pos_y = -0.5, pos_z = -527.579, pos_rot = 247 WHERE mobid = 16912493;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 29, pos_x = 489.885, pos_y = -0.5, pos_z = -482.846, pos_rot = 151 WHERE mobid = 16912494;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 29, pos_x = 492.151, pos_y = -0.864, pos_z = -427.77, pos_rot = 239 WHERE mobid = 16912495;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 527.093, pos_y = -0.5, pos_z = -469.416, pos_rot = 87 WHERE mobid = 16912496;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 510.851, pos_y = -0.5, pos_z = -434.323, pos_rot = 137 WHERE mobid = 16912497;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 484.563, pos_y = -0.5, pos_z = -486.664, pos_rot = 188 WHERE mobid = 16912498;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 30, pos_x = 455.566, pos_y = -1.217, pos_z = -597.924, pos_rot = 251 WHERE mobid = 16912499;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 31, pos_x = 561.407, pos_y = -1.533, pos_z = -527.602, pos_rot = 66 WHERE mobid = 16912500;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 35, pos_x = 430.611, pos_y = -1.386, pos_z = -606.282, pos_rot = 119 WHERE mobid = 16912501;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 36, pos_x = 574.313, pos_y = -1.34, pos_z = -518.119, pos_rot = 251 WHERE mobid = 16912502;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 25, pos_x = 446.59, pos_y = -1.515, pos_z = -601.785, pos_rot = 91 WHERE mobid = 16912503;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 558.81, pos_y = -0.5, pos_z = -307.693, pos_rot = 77 WHERE mobid = 16912504;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 597.811, pos_y = -0.633, pos_z = -295.037, pos_rot = 44 WHERE mobid = 16912505;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 647.028, pos_y = -0.5, pos_z = -314.034, pos_rot = 98 WHERE mobid = 16912506;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 654.164, pos_y = -0.5, pos_z = -248.764, pos_rot = 13 WHERE mobid = 16912507;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 590.385, pos_y = -0.5, pos_z = -329.055, pos_rot = 95 WHERE mobid = 16912508;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 584.589, pos_y = -0.5, pos_z = -236.11, pos_rot = 72 WHERE mobid = 16912509;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 662.464, pos_y = -0.5, pos_z = -281.033, pos_rot = 247 WHERE mobid = 16912510;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 643.544, pos_y = -0.5, pos_z = -229.51, pos_rot = 56 WHERE mobid = 16912511;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 648.985, pos_y = -0.5, pos_z = -222.272, pos_rot = 22 WHERE mobid = 16912512;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 635.936, pos_y = -0.5, pos_z = -328.447, pos_rot = 183 WHERE mobid = 16912513;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 619.156, pos_y = -0.5, pos_z = -290.792, pos_rot = 83 WHERE mobid = 16912514;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 611.415, pos_y = -0.5, pos_z = -324.39, pos_rot = 92 WHERE mobid = 16912515;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 667.167, pos_y = -0.5, pos_z = -248.265, pos_rot = 10 WHERE mobid = 16912516;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 31, pos_x = 688.807, pos_y = -0.5, pos_z = -202.295, pos_rot = 29 WHERE mobid = 16912517;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 25, pos_x = 0, pos_y = 0, pos_z = 0, pos_rot = 0 WHERE mobid = 16912518;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 21, pos_x = 679.939, pos_y = -0.5, pos_z = -186.798, pos_rot = 177 WHERE mobid = 16912519;
UPDATE mob_spawn_points SET mobname = "Aerns_Wynav", polutils_name = "Aern\'s Wynav", groupid = 13, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 1 WHERE mobid = 16912520;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 667.203, pos_y = -0.794, pos_z = -151.731, pos_rot = 107 WHERE mobid = 16912521;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 691.255, pos_y = -0.529, pos_z = -187.634, pos_rot = 32 WHERE mobid = 16912522;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 641.313, pos_y = -0.515, pos_z = -163.142, pos_rot = 61 WHERE mobid = 16912523;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 669.907, pos_y = -1.584, pos_z = -122.855, pos_rot = 111 WHERE mobid = 16912524;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 635.329, pos_y = -0.5, pos_z = -88.719, pos_rot = 14 WHERE mobid = 16912525;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 696.887, pos_y = -0.552, pos_z = -187.067, pos_rot = 52 WHERE mobid = 16912526;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 35, pos_x = 667.92, pos_y = -1.58, pos_z = -149.632, pos_rot = 40 WHERE mobid = 16912527;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 20, pos_x = 674.08, pos_y = -1.727, pos_z = -157.43, pos_rot = 54 WHERE mobid = 16912528;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 22, pos_x = 692.106, pos_y = -1.249, pos_z = -158.938, pos_rot = 238 WHERE mobid = 16912529;
UPDATE mob_spawn_points SET mobname = "Aerns_Elemental", polutils_name = "Aern\'s Elemental", groupid = 18, pos_x = 620.98, pos_y = -0.5, pos_z = -54.43, pos_rot = 196 WHERE mobid = 16912530;
UPDATE mob_spawn_points SET mobname = "Aerns_Xzomit", polutils_name = "Aern\'s Xzomit", groupid = 14, pos_x = 646.482, pos_y = -0.098, pos_z = -137.696, pos_rot = 167 WHERE mobid = 16912531;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 659.916, pos_y = -0.5, pos_z = -203.267, pos_rot = 26 WHERE mobid = 16912532;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 608.26, pos_y = -0.5, pos_z = -156.262, pos_rot = 90 WHERE mobid = 16912533;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 660.275, pos_y = -0.5, pos_z = -68.882, pos_rot = 228 WHERE mobid = 16912534;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 666.883, pos_y = -0.5, pos_z = -72.905, pos_rot = 24 WHERE mobid = 16912535;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 613.269, pos_y = -0.5, pos_z = -149.021, pos_rot = 201 WHERE mobid = 16912536;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 663.199, pos_y = -0.5, pos_z = -204.67, pos_rot = 7 WHERE mobid = 16912537;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 636.248, pos_y = -0.5, pos_z = -11.439, pos_rot = 43 WHERE mobid = 16912538;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 626.623, pos_y = -0.5, pos_z = -10.162, pos_rot = 22 WHERE mobid = 16912539;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 632.519, pos_y = -0.5, pos_z = 19.204, pos_rot = 102 WHERE mobid = 16912540;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 615.192, pos_y = -0.5, pos_z = 27.472, pos_rot = 108 WHERE mobid = 16912541;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 585.911, pos_y = -0.5, pos_z = -33.039, pos_rot = 218 WHERE mobid = 16912542;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 603.255, pos_y = -0.5, pos_z = -7.476, pos_rot = 113 WHERE mobid = 16912543;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 595.922, pos_y = -0.5, pos_z = 1.021, pos_rot = 122 WHERE mobid = 16912544;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = -217.498, pos_y = -0.5, pos_z = -665.529, pos_rot = 203 WHERE mobid = 16912545;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = -219, pos_y = -0.5, pos_z = -666.906, pos_rot = 206 WHERE mobid = 16912546;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 57, pos_x = -217.99, pos_y = -0.5, pos_z = -659.432, pos_rot = 214 WHERE mobid = 16912547;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = -334.022, pos_y = -0.381, pos_z = -530.541, pos_rot = 251 WHERE mobid = 16912548;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 7, pos_x = -335.897, pos_y = -0.395, pos_z = -529.831, pos_rot = 254 WHERE mobid = 16912549;
UPDATE mob_spawn_points SET mobname = "Ulxzomit", polutils_name = "Ul\'xzomit", groupid = 57, pos_x = -334.009, pos_y = -0.5, pos_z = -537.181, pos_rot = 28 WHERE mobid = 16912550;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = -188.063, pos_y = -0.5, pos_z = -528.951, pos_rot = 21 WHERE mobid = 16912551;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = -147.425, pos_y = -0.5, pos_z = -572.89, pos_rot = 225 WHERE mobid = 16912552;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = -156.234, pos_y = -0.5, pos_z = -579.625, pos_rot = 27 WHERE mobid = 16912553;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = -174.934, pos_y = -0.5, pos_z = -534.294, pos_rot = 31 WHERE mobid = 16912554;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = -198.153, pos_y = -0.5, pos_z = -535.221, pos_rot = 148 WHERE mobid = 16912555;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = -362.593, pos_y = -0.499, pos_z = -581.003, pos_rot = 41 WHERE mobid = 16912556;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = -329.058, pos_y = -0.5, pos_z = -593.865, pos_rot = 227 WHERE mobid = 16912557;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = -364.139, pos_y = -0.5, pos_z = -560.755, pos_rot = 51 WHERE mobid = 16912558;
UPDATE mob_spawn_points SET mobname = "Ulhpemde", polutils_name = "Ul\'hpemde", groupid = 8, pos_x = -411.282, pos_y = -0.5, pos_z = -541.439, pos_rot = 202 WHERE mobid = 16912559;
UPDATE mob_spawn_points SET mobname = "Ulphuabo", polutils_name = "Ul\'phuabo", groupid = 9, pos_x = -168.196, pos_y = -0.5, pos_z = -573.727, pos_rot = 249 WHERE mobid = 16912560;
UPDATE mob_spawn_points SET mobname = "Ulphuabo", polutils_name = "Ul\'phuabo", groupid = 9, pos_x = -355.018, pos_y = -0.5, pos_z = -556.595, pos_rot = 25 WHERE mobid = 16912561;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 32, pos_x = -510.296, pos_y = -0.64, pos_z = -345.052, pos_rot = 161 WHERE mobid = 16912562;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 32, pos_x = -565.841, pos_y = -0.313, pos_z = -387.083, pos_rot = 232 WHERE mobid = 16912563;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 30, pos_x = -483.372, pos_y = -0.499, pos_z = -418.633, pos_rot = 88 WHERE mobid = 16912564;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 30, pos_x = -526.716, pos_y = -0.5, pos_z = -458.874, pos_rot = 120 WHERE mobid = 16912565;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 36, pos_x = -617.956, pos_y = -0.501, pos_z = -438.834, pos_rot = 90 WHERE mobid = 16912566;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 33, pos_x = -527.819, pos_y = -0.5, pos_z = -321.648, pos_rot = 53 WHERE mobid = 16912567;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 33, pos_x = -598.126, pos_y = -0.5, pos_z = -460.28, pos_rot = 91 WHERE mobid = 16912568;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 29, pos_x = -561.278, pos_y = -0.499, pos_z = -316.129, pos_rot = 141 WHERE mobid = 16912569;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 25, pos_x = -504.233, pos_y = -0.5, pos_z = -441.207, pos_rot = 10 WHERE mobid = 16912570;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 21, pos_x = -505.733, pos_y = -0.518, pos_z = -371.254, pos_rot = 139 WHERE mobid = 16912571;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 21, pos_x = -596.3, pos_y = -0.5, pos_z = -438.041, pos_rot = 131 WHERE mobid = 16912572;
UPDATE mob_spawn_points SET mobname = "Aerns_Wynav", polutils_name = "Aern\'s Wynav", groupid = 13, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 213 WHERE mobid = 16912573;
UPDATE mob_spawn_points SET mobname = "Aerns_Wynav", polutils_name = "Aern\'s Wynav", groupid = 13, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 197 WHERE mobid = 16912574;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = -554.438, pos_y = -0.5, pos_z = -333.371, pos_rot = 85 WHERE mobid = 16912575;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = -538.145, pos_y = -0.436, pos_z = -355.887, pos_rot = 180 WHERE mobid = 16912576;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = -529.222, pos_y = -0.499, pos_z = -310.212, pos_rot = 17 WHERE mobid = 16912577;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = -379.072, pos_y = -0.5, pos_z = -338.679, pos_rot = 195 WHERE mobid = 16912578;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = -378.271, pos_y = -0.602, pos_z = -302.401, pos_rot = 45 WHERE mobid = 16912579;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = -388.754, pos_y = -0.499, pos_z = -349.926, pos_rot = 210 WHERE mobid = 16912580;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = -355.436, pos_y = -1.601, pos_z = -410.789, pos_rot = 116 WHERE mobid = 16912581;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = -293.927, pos_y = -0.445, pos_z = -418.631, pos_rot = 44 WHERE mobid = 16912582;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = -257.996, pos_y = -0.519, pos_z = -422.68, pos_rot = 181 WHERE mobid = 16912583;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 19, pos_x = -533.074, pos_y = -0.5, pos_z = -176.443, pos_rot = 143 WHERE mobid = 16912584;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 26, pos_x = 0, pos_y = 0, pos_z = 0, pos_rot = 0 WHERE mobid = 16912585;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 27, pos_x = -485.739, pos_y = -0.5, pos_z = -113.174, pos_rot = 132 WHERE mobid = 16912586;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 28, pos_x = -516.898, pos_y = -0.573, pos_z = -225.663, pos_rot = 11 WHERE mobid = 16912587;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 22, pos_x = -441.44, pos_y = -0.499, pos_z = -138.98, pos_rot = 232 WHERE mobid = 16912588;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -468.178, pos_y = -0.5, pos_z = -133.725, pos_rot = 198 WHERE mobid = 16912589;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -444.238, pos_y = -0.499, pos_z = -143.084, pos_rot = 207 WHERE mobid = 16912590;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -491.605, pos_y = -0.499, pos_z = -242.672, pos_rot = 91 WHERE mobid = 16912591;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -446.037, pos_y = -0.499, pos_z = -151, pos_rot = 45 WHERE mobid = 16912592;
UPDATE mob_spawn_points SET mobname = "Aerns_Elemental", polutils_name = "Aern\'s Elemental", groupid = 18, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 1 WHERE mobid = 16912593;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = -481.759, pos_y = -0.5, pos_z = -237.964, pos_rot = 25 WHERE mobid = 16912594;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = -453.395, pos_y = -0.5, pos_z = -181.644, pos_rot = 86 WHERE mobid = 16912595;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = -511.711, pos_y = -0.5, pos_z = -149.7, pos_rot = 164 WHERE mobid = 16912596;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = -495.049, pos_y = -0.5, pos_z = -163.479, pos_rot = 39 WHERE mobid = 16912597;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = -527.81, pos_y = -0.749, pos_z = -123.384, pos_rot = 0 WHERE mobid = 16912598;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -636.537, pos_y = -0.5, pos_z = -227.542, pos_rot = 254 WHERE mobid = 16912599;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -648.821, pos_y = -0.5, pos_z = -258.818, pos_rot = 96 WHERE mobid = 16912600;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -713.883, pos_y = -0.499, pos_z = -259.451, pos_rot = 125 WHERE mobid = 16912601;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -708.732, pos_y = -0.499, pos_z = -244.867, pos_rot = 57 WHERE mobid = 16912602;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -700.527, pos_y = -0.499, pos_z = -205.911, pos_rot = 121 WHERE mobid = 16912603;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -679.182, pos_y = -0.499, pos_z = -206.506, pos_rot = 79 WHERE mobid = 16912604;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 30, pos_x = -691.572, pos_y = -1.531, pos_z = -120.841, pos_rot = 255 WHERE mobid = 16912605;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 31, pos_x = -566.106, pos_y = -0.468, pos_z = -108.021, pos_rot = 5 WHERE mobid = 16912606;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 35, pos_x = -594.217, pos_y = -0.497, pos_z = -72.221, pos_rot = 39 WHERE mobid = 16912607;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 36, pos_x = -578.799, pos_y = -0.315, pos_z = -96.729, pos_rot = 88 WHERE mobid = 16912608;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 20, pos_x = -703.417, pos_y = -0.228, pos_z = -109.922, pos_rot = 68 WHERE mobid = 16912609;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -638.469, pos_y = -0.499, pos_z = -87.864, pos_rot = 170 WHERE mobid = 16912610;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -635.631, pos_y = -0.5, pos_z = -103.855, pos_rot = 81 WHERE mobid = 16912611;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -576.21, pos_y = -0.429, pos_z = -102.11, pos_rot = 95 WHERE mobid = 16912612;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -591.075, pos_y = -0.639, pos_z = -72.299, pos_rot = 161 WHERE mobid = 16912613;
UPDATE mob_spawn_points SET mobname = "Aerns_Xzomit", polutils_name = "Aern\'s Xzomit", groupid = 14, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 141 WHERE mobid = 16912614;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = -619.041, pos_y = -0.5, pos_z = -113.369, pos_rot = 55 WHERE mobid = 16912615;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = -597.635, pos_y = -0.5, pos_z = -143.193, pos_rot = 25 WHERE mobid = 16912616;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = -558.931, pos_y = -0.499, pos_z = -87.441, pos_rot = 35 WHERE mobid = 16912617;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 31, pos_x = -531.017, pos_y = -0.5, pos_z = 17.035, pos_rot = 155 WHERE mobid = 16912618;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 35, pos_x = -538.245, pos_y = -0.5, pos_z = 0.383, pos_rot = 232 WHERE mobid = 16912619;
UPDATE mob_spawn_points SET mobname = "Omyovra", polutils_name = "Om\'yovra", groupid = 37, pos_x = -568.849, pos_y = -1.342, pos_z = -393.761, pos_rot = 23 WHERE mobid = 16912620;
UPDATE mob_spawn_points SET mobname = "Omyovra", polutils_name = "Om\'yovra", groupid = 37, pos_x = -501.644, pos_y = -0.5, pos_z = -242.736, pos_rot = 97 WHERE mobid = 16912621;
UPDATE mob_spawn_points SET mobname = "Omyovra", polutils_name = "Om\'yovra", groupid = 37, pos_x = 648.269, pos_y = -0.5, pos_z = -171.665, pos_rot = 32 WHERE mobid = 16912622;
UPDATE mob_spawn_points SET mobname = "Omyovra", polutils_name = "Om\'yovra", groupid = 37, pos_x = 576.067, pos_y = -0.5, pos_z = -343.374, pos_rot = 193 WHERE mobid = 16912623;
UPDATE mob_spawn_points SET mobname = "Ulyovra", polutils_name = "Ul\'yovra", groupid = 38, pos_x = 175.465, pos_y = -0.499, pos_z = -490.461, pos_rot = 213 WHERE mobid = 16912624;
UPDATE mob_spawn_points SET mobname = "Ulyovra", polutils_name = "Ul\'yovra", groupid = 38, pos_x = -227.017, pos_y = -0.5, pos_z = -623.492, pos_rot = 33 WHERE mobid = 16912625;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 24, pos_x = 29.655, pos_y = -0.5, pos_z = 512.233, pos_rot = 108 WHERE mobid = 16912626;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 26.649, pos_y = -0.5, pos_z = 510.888, pos_rot = 104 WHERE mobid = 16912627;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 24, pos_x = 69.943, pos_y = -0.5, pos_z = 442.369, pos_rot = 167 WHERE mobid = 16912628;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 68.706, pos_y = -0.5, pos_z = 444.535, pos_rot = 147 WHERE mobid = 16912629;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 98.499, pos_y = -0.5, pos_z = 491.033, pos_rot = 246 WHERE mobid = 16912630;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 24, pos_x = -84.122, pos_y = -0.519, pos_z = 452.188, pos_rot = 221 WHERE mobid = 16912631;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = -81.344, pos_y = -0.513, pos_z = 456.178, pos_rot = 220 WHERE mobid = 16912632;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 24, pos_x = -90.558, pos_y = -0.699, pos_z = 446.179, pos_rot = 248 WHERE mobid = 16912633;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = -90.672, pos_y = -0.648, pos_z = 450.451, pos_rot = 199 WHERE mobid = 16912634;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = -121.231, pos_y = -0.5, pos_z = 494.926, pos_rot = 84 WHERE mobid = 16912635;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 169.127, pos_y = -0.5, pos_z = 575.573, pos_rot = 119 WHERE mobid = 16912636;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 201.917, pos_y = -0.5, pos_z = 563.212, pos_rot = 161 WHERE mobid = 16912637;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 165.99, pos_y = -0.5, pos_z = 536.493, pos_rot = 9 WHERE mobid = 16912638;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 247.274, pos_y = -0.5, pos_z = 552.601, pos_rot = 90 WHERE mobid = 16912639;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 159.116, pos_y = -0.5, pos_z = 558.932, pos_rot = 161 WHERE mobid = 16912640;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 166.92, pos_y = -0.5, pos_z = 558.446, pos_rot = 239 WHERE mobid = 16912641;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 260.161, pos_y = -0.5, pos_z = 395.108, pos_rot = 202 WHERE mobid = 16912642;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 276.634, pos_y = -0.5, pos_z = 458.106, pos_rot = 142 WHERE mobid = 16912643;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 251.554, pos_y = -0.5, pos_z = 455.069, pos_rot = 167 WHERE mobid = 16912644;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 265.175, pos_y = -0.5, pos_z = 384.937, pos_rot = 128 WHERE mobid = 16912645;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 285.281, pos_y = -0.5, pos_z = 428.781, pos_rot = 244 WHERE mobid = 16912646;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 270.537, pos_y = -0.5, pos_z = 443.558, pos_rot = 56 WHERE mobid = 16912647;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 30, pos_x = 212.363, pos_y = -0.423, pos_z = 716.395, pos_rot = 170 WHERE mobid = 16912648;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 31, pos_x = 195.159, pos_y = -1.624, pos_z = 676.767, pos_rot = 213 WHERE mobid = 16912649;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 35, pos_x = 293.688, pos_y = -0.746, pos_z = 627.447, pos_rot = 9 WHERE mobid = 16912650;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 226.463, pos_y = -0.5, pos_z = 734.492, pos_rot = 100 WHERE mobid = 16912651;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 277.81, pos_y = -0.498, pos_z = 645.411, pos_rot = 218 WHERE mobid = 16912652;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 236.198, pos_y = -0.5, pos_z = 714.801, pos_rot = 140 WHERE mobid = 16912653;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 276.197, pos_y = -0.5, pos_z = 617.746, pos_rot = 46 WHERE mobid = 16912654;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 29, pos_x = 136.343, pos_y = -0.5, pos_z = 737.884, pos_rot = 244 WHERE mobid = 16912655;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 36, pos_x = 144.075, pos_y = -1.44, pos_z = 656.22, pos_rot = 37 WHERE mobid = 16912656;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 130.605, pos_y = -0.5, pos_z = 733.199, pos_rot = 78 WHERE mobid = 16912657;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 96.866, pos_y = -0.527, pos_z = 660.843, pos_rot = 15 WHERE mobid = 16912658;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 78.645, pos_y = -0.5, pos_z = 688.778, pos_rot = 237 WHERE mobid = 16912659;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = -232.663, pos_y = -0.5, pos_z = 487.804, pos_rot = 163 WHERE mobid = 16912660;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = -219.169, pos_y = -0.5, pos_z = 513.884, pos_rot = 178 WHERE mobid = 16912661;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -218.1, pos_y = -0.5, pos_z = 470.682, pos_rot = 156 WHERE mobid = 16912662;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -218.126, pos_y = -0.5, pos_z = 452.273, pos_rot = 177 WHERE mobid = 16912663;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = -202.295, pos_y = -0.5, pos_z = 454.983, pos_rot = 203 WHERE mobid = 16912664;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 33, pos_x = -295.052, pos_y = -0.646, pos_z = 532.331, pos_rot = 202 WHERE mobid = 16912665;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 20, pos_x = -237.08, pos_y = -1.573, pos_z = 560.509, pos_rot = 196 WHERE mobid = 16912666;
UPDATE mob_spawn_points SET mobname = "Aerns_Xzomit", polutils_name = "Aern\'s Xzomit", groupid = 14, pos_x = -286.831, pos_y = -0.004, pos_z = 567.712, pos_rot = 210 WHERE mobid = 16912667;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -247.069, pos_y = -1.216, pos_z = 574.732, pos_rot = 84 WHERE mobid = 16912668;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -298.273, pos_y = -0.992, pos_z = 523.899, pos_rot = 209 WHERE mobid = 16912669;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = -295.655, pos_y = -0.5, pos_z = 601.667, pos_rot = 205 WHERE mobid = 16912670;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 26, pos_x = -203.593, pos_y = -0.5, pos_z = 678.112, pos_rot = 210 WHERE mobid = 16912671;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 25, pos_x = -214.432, pos_y = -0.5, pos_z = 661.436, pos_rot = 104 WHERE mobid = 16912672;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 21, pos_x = -230.217, pos_y = -0.5, pos_z = 641.781, pos_rot = 13 WHERE mobid = 16912673;
UPDATE mob_spawn_points SET mobname = "Aerns_Wynav", polutils_name = "Aern\'s Wynav", groupid = 13, pos_x = -193.827, pos_y = -0.188, pos_z = 650.976, pos_rot = 27 WHERE mobid = 16912674;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -190.999, pos_y = -0.5, pos_z = 633.837, pos_rot = 225 WHERE mobid = 16912675;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -217.207, pos_y = -0.5, pos_z = 682.52, pos_rot = 194 WHERE mobid = 16912676;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = -187.065, pos_y = -0.5, pos_z = 646.009, pos_rot = 228 WHERE mobid = 16912677;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 19, pos_x = -64.857, pos_y = -0.549, pos_z = 653.249, pos_rot = 43 WHERE mobid = 16912678;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 27, pos_x = -108.855, pos_y = -1.507, pos_z = 599.185, pos_rot = 241 WHERE mobid = 16912679;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -97.418, pos_y = -0.5, pos_z = 662.194, pos_rot = 76 WHERE mobid = 16912680;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -72.146, pos_y = -0.504, pos_z = 676.288, pos_rot = 44 WHERE mobid = 16912681;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 24, pos_x = -696.979, pos_y = -0.64, pos_z = 58.933, pos_rot = 74 WHERE mobid = 16912682;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = -697.431, pos_y = -1.472, pos_z = 38.302, pos_rot = 243 WHERE mobid = 16912683;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 24, pos_x = -646.78, pos_y = -0.5, pos_z = 59.568, pos_rot = 118 WHERE mobid = 16912684;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = -649.702, pos_y = -0.5, pos_z = 58.031, pos_rot = 118 WHERE mobid = 16912685;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = -650.447, pos_y = -0.5, pos_z = 88.5, pos_rot = 131 WHERE mobid = 16912686;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 24, pos_x = -563.735, pos_y = -0.5, pos_z = 70.601, pos_rot = 3 WHERE mobid = 16912687;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = -557.37, pos_y = -0.5, pos_z = 72.451, pos_rot = 19 WHERE mobid = 16912688;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 24, pos_x = -508.231, pos_y = -0.547, pos_z = 104.414, pos_rot = 148 WHERE mobid = 16912689;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = -505.938, pos_y = -0.561, pos_z = 102.98, pos_rot = 147 WHERE mobid = 16912690;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 24, pos_x = -500.929, pos_y = -0.755, pos_z = 73.326, pos_rot = 243 WHERE mobid = 16912691;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = -492.369, pos_y = -1.341, pos_z = 76.107, pos_rot = 250 WHERE mobid = 16912692;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = -529.749, pos_y = -0.5, pos_z = 84.333, pos_rot = 107 WHERE mobid = 16912693;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 32, pos_x = -495.251, pos_y = -0.5, pos_z = 164.194, pos_rot = 7 WHERE mobid = 16912694;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 26, pos_x = -423.102, pos_y = -0.658, pos_z = 216.188, pos_rot = 239 WHERE mobid = 16912695;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 35, pos_x = -443.654, pos_y = -0.374, pos_z = 165.081, pos_rot = 45 WHERE mobid = 16912696;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = -517.354, pos_y = -0.5, pos_z = 160.224, pos_rot = 200 WHERE mobid = 16912697;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = -482.993, pos_y = -0.497, pos_z = 191.983, pos_rot = 253 WHERE mobid = 16912698;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = -430.988, pos_y = -1.119, pos_z = 213.717, pos_rot = 16 WHERE mobid = 16912699;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = -423.068, pos_y = -0.5, pos_z = 167.57, pos_rot = 221 WHERE mobid = 16912700;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = -469.357, pos_y = -0.5, pos_z = 140.714, pos_rot = 243 WHERE mobid = 16912701;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = -453.854, pos_y = -0.494, pos_z = 134.494, pos_rot = 242 WHERE mobid = 16912702;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = -478.113, pos_y = -0.445, pos_z = 122.513, pos_rot = 0 WHERE mobid = 16912703;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = -479.044, pos_y = -0.5, pos_z = 202.431, pos_rot = 161 WHERE mobid = 16912704;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = -404.723, pos_y = -0.5, pos_z = 200.846, pos_rot = 40 WHERE mobid = 16912705;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 31, pos_x = -550.495, pos_y = -0.5, pos_z = 212.56, pos_rot = 19 WHERE mobid = 16912706;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = -592.707, pos_y = -1.345, pos_z = 220.858, pos_rot = 255 WHERE mobid = 16912707;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = -553.296, pos_y = -0.5, pos_z = 215.848, pos_rot = 24 WHERE mobid = 16912708;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 29, pos_x = -362.753, pos_y = -0.5, pos_z = 242.756, pos_rot = 41 WHERE mobid = 16912709;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 19, pos_x = -383.28, pos_y = -0.5, pos_z = 272.449, pos_rot = 213 WHERE mobid = 16912710;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 33, pos_x = -400.284, pos_y = -0.5, pos_z = 257.42, pos_rot = 36 WHERE mobid = 16912711;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 22, pos_x = -398.919, pos_y = -0.5, pos_z = 258.2, pos_rot = 67 WHERE mobid = 16912712;
UPDATE mob_spawn_points SET mobname = "Aerns_Elemental", polutils_name = "Aern\'s Elemental", groupid = 18, pos_x = -415.886, pos_y = -0.5, pos_z = 357.405, pos_rot = 254 WHERE mobid = 16912713;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = -395.471, pos_y = -0.5, pos_z = 289.67, pos_rot = 17 WHERE mobid = 16912714;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 27, pos_x = -494.36, pos_y = -1.264, pos_z = 393.844, pos_rot = 90 WHERE mobid = 16912715;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 28, pos_x = -552.073, pos_y = -0.5, pos_z = 332.07, pos_rot = 179 WHERE mobid = 16912716;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 25, pos_x = -535.9, pos_y = -0.504, pos_z = 309.027, pos_rot = 81 WHERE mobid = 16912717;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -514.463, pos_y = -1.266, pos_z = 290.303, pos_rot = 51 WHERE mobid = 16912718;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -498.895, pos_y = -1.334, pos_z = 300.131, pos_rot = 110 WHERE mobid = 16912719;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = -526.051, pos_y = -0.5, pos_z = 307.038, pos_rot = 92 WHERE mobid = 16912720;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = -514.1, pos_y = -0.5, pos_z = 320.928, pos_rot = 97 WHERE mobid = 16912721;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 32, pos_x = -343.779, pos_y = -0.5, pos_z = 390.829, pos_rot = 147 WHERE mobid = 16912722;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 27, pos_x = -349.882, pos_y = -0.5, pos_z = 407.292, pos_rot = 148 WHERE mobid = 16912723;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 20, pos_x = -266.187, pos_y = -0.5, pos_z = 370.375, pos_rot = 19 WHERE mobid = 16912724;
UPDATE mob_spawn_points SET mobname = "Aerns_Xzomit", polutils_name = "Aern\'s Xzomit", groupid = 14, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 147 WHERE mobid = 16912725;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 21, pos_x = -369.003, pos_y = -0.661, pos_z = 387.122, pos_rot = 191 WHERE mobid = 16912726;
UPDATE mob_spawn_points SET mobname = "Aerns_Wynav", polutils_name = "Aern\'s Wynav", groupid = 13, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 61 WHERE mobid = 16912727;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -342.049, pos_y = -0.5, pos_z = 348.817, pos_rot = 168 WHERE mobid = 16912728;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -387.658, pos_y = -0.5, pos_z = 430.303, pos_rot = 207 WHERE mobid = 16912729;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = -389.861, pos_y = -0.5, pos_z = 427.976, pos_rot = 127 WHERE mobid = 16912730;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = -285.84, pos_y = -0.5, pos_z = 396.132, pos_rot = 111 WHERE mobid = 16912731;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 36, pos_x = -471.096, pos_y = -0.5, pos_z = 415.58, pos_rot = 223 WHERE mobid = 16912732;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -413.258, pos_y = -0.5, pos_z = 435.382, pos_rot = 97 WHERE mobid = 16912733;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = -512.973, pos_y = -0.5, pos_z = 462.456, pos_rot = 11 WHERE mobid = 16912734;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = -426.8, pos_y = -0.5, pos_z = 434.866, pos_rot = 6 WHERE mobid = 16912735;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -395.323, pos_y = -0.5, pos_z = 535.139, pos_rot = 164 WHERE mobid = 16912736;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -445.392, pos_y = -0.5, pos_z = 547.909, pos_rot = 51 WHERE mobid = 16912737;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = -394.847, pos_y = -0.5, pos_z = 572.99, pos_rot = 228 WHERE mobid = 16912738;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = -406.515, pos_y = -0.5, pos_z = 524.24, pos_rot = 21 WHERE mobid = 16912739;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = -415.159, pos_y = -0.5, pos_z = 548.45, pos_rot = 28 WHERE mobid = 16912740;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = -416.871, pos_y = -0.5, pos_z = 600.716, pos_rot = 149 WHERE mobid = 16912741;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 36, pos_x = 500.29, pos_y = -0.499, pos_z = 141.607, pos_rot = 139 WHERE mobid = 16912742;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 33, pos_x = 517.305, pos_y = -0.499, pos_z = 108.494, pos_rot = 183 WHERE mobid = 16912743;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 524.257, pos_y = -0.499, pos_z = 131.856, pos_rot = 148 WHERE mobid = 16912744;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 31, pos_x = 584.736, pos_y = -0.508, pos_z = 226.501, pos_rot = 229 WHERE mobid = 16912745;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 35, pos_x = 555.815, pos_y = -0.689, pos_z = 235.573, pos_rot = 160 WHERE mobid = 16912746;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 21, pos_x = 460.107, pos_y = -0.522, pos_z = 221.29, pos_rot = 243 WHERE mobid = 16912747;
UPDATE mob_spawn_points SET mobname = "Aerns_Wynav", polutils_name = "Aern\'s Wynav", groupid = 13, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 119 WHERE mobid = 16912748;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 424.752, pos_y = -1.487, pos_z = 209.996, pos_rot = 133 WHERE mobid = 16912749;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 438.435, pos_y = -1.332, pos_z = 185.208, pos_rot = 66 WHERE mobid = 16912750;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 523.71, pos_y = -0.499, pos_z = 186.573, pos_rot = 101 WHERE mobid = 16912751;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 537.658, pos_y = -0.499, pos_z = 178.216, pos_rot = 235 WHERE mobid = 16912752;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 29, pos_x = 477.907, pos_y = -0.499, pos_z = -56.199, pos_rot = 20 WHERE mobid = 16912753;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 36, pos_x = 466.7, pos_y = -1.27, pos_z = 28.871, pos_rot = 226 WHERE mobid = 16912754;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 28, pos_x = 445.078, pos_y = -0.499, pos_z = 24.895, pos_rot = 168 WHERE mobid = 16912755;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 25, pos_x = 437.262, pos_y = -0.499, pos_z = -23.764, pos_rot = 12 WHERE mobid = 16912756;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 489.171, pos_y = -0.499, pos_z = 93.929, pos_rot = 217 WHERE mobid = 16912757;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 436.408, pos_y = -0.499, pos_z = -5.134, pos_rot = 114 WHERE mobid = 16912758;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 484.868, pos_y = -0.499, pos_z = -43.088, pos_rot = 27 WHERE mobid = 16912759;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 30, pos_x = 476.229, pos_y = -0.499, pos_z = -147.628, pos_rot = 142 WHERE mobid = 16912760;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 19, pos_x = 494.957, pos_y = -0.499, pos_z = -128.209, pos_rot = 207 WHERE mobid = 16912761;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 22, pos_x = 462.281, pos_y = -0.499, pos_z = -140.259, pos_rot = 103 WHERE mobid = 16912762;
UPDATE mob_spawn_points SET mobname = "Aerns_Elemental", polutils_name = "Aern\'s Elemental", groupid = 18, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 1 WHERE mobid = 16912763;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 487.365, pos_y = -0.499, pos_z = -164.737, pos_rot = 178 WHERE mobid = 16912764;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 26, pos_x = 399.487, pos_y = -0.499, pos_z = -229.361, pos_rot = 117 WHERE mobid = 16912765;
UPDATE mob_spawn_points SET mobname = "Omaern", polutils_name = "Om\'aern", groupid = 33, pos_x = 428.827, pos_y = -0.499, pos_z = -238.025, pos_rot = 153 WHERE mobid = 16912766;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 421.741, pos_y = -0.499, pos_z = -256.876, pos_rot = 227 WHERE mobid = 16912767;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 426.047, pos_y = -1.147, pos_z = 292.489, pos_rot = 92 WHERE mobid = 16912768;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 409.422, pos_y = -0.5, pos_z = 340.614, pos_rot = 127 WHERE mobid = 16912769;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 461.613, pos_y = -0.499, pos_z = 337.323, pos_rot = 232 WHERE mobid = 16912770;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 420.45, pos_y = -0.5, pos_z = 392.122, pos_rot = 26 WHERE mobid = 16912771;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 341.439, pos_y = -1.356, pos_z = 315.597, pos_rot = 220 WHERE mobid = 16912772;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 344.352, pos_y = -1.356, pos_z = 319.384, pos_rot = 95 WHERE mobid = 16912773;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 398.537, pos_y = -0.499, pos_z = 350.755, pos_rot = 202 WHERE mobid = 16912774;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 396.858, pos_y = -0.5, pos_z = 444.553, pos_rot = 25 WHERE mobid = 16912775;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 466.047, pos_y = -1.493, pos_z = 479.573, pos_rot = 215 WHERE mobid = 16912776;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 406.581, pos_y = -0.5, pos_z = 460.916, pos_rot = 246 WHERE mobid = 16912777;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 459.773, pos_y = -0.499, pos_z = 432.229, pos_rot = 245 WHERE mobid = 16912778;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 372.695, pos_y = -0.371, pos_z = 458.869, pos_rot = 124 WHERE mobid = 16912779;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 24, pos_x = 510.074, pos_y = -1.448, pos_z = 312.868, pos_rot = 190 WHERE mobid = 16912780;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 510.011, pos_y = -1.388, pos_z = 309.621, pos_rot = 156 WHERE mobid = 16912781;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 485.093, pos_y = -0.499, pos_z = 368.595, pos_rot = 91 WHERE mobid = 16912782;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 512.964, pos_y = -1.475, pos_z = 304.569, pos_rot = 43 WHERE mobid = 16912783;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 483.854, pos_y = -0.499, pos_z = 371.323, pos_rot = 78 WHERE mobid = 16912784;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 24, pos_x = 575.002, pos_y = -0.658, pos_z = 373.674, pos_rot = 117 WHERE mobid = 16912785;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 569.641, pos_y = -0.906, pos_z = 375.913, pos_rot = 139 WHERE mobid = 16912786;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 24, pos_x = 558.796, pos_y = -1.409, pos_z = 377.646, pos_rot = 248 WHERE mobid = 16912787;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 563.587, pos_y = -1.299, pos_z = 379.054, pos_rot = 244 WHERE mobid = 16912788;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 24, pos_x = 635.136, pos_y = -1.373, pos_z = 381.201, pos_rot = 14 WHERE mobid = 16912789;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 647.567, pos_y = -1.375, pos_z = 388.922, pos_rot = 127 WHERE mobid = 16912790;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 610.443, pos_y = -0.499, pos_z = 411.45, pos_rot = 31 WHERE mobid = 16912791;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 24, pos_x = 616.903, pos_y = -0.569, pos_z = 465.139, pos_rot = 69 WHERE mobid = 16912792;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 613.227, pos_y = -1.008, pos_z = 466.451, pos_rot = 59 WHERE mobid = 16912793;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 24, pos_x = 524.916, pos_y = -0.425, pos_z = 529.555, pos_rot = 24 WHERE mobid = 16912794;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 528.915, pos_y = -0.434, pos_z = 528.533, pos_rot = 29 WHERE mobid = 16912795;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 593.253, pos_y = -0.534, pos_z = 467.709, pos_rot = 137 WHERE mobid = 16912796;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 615.271, pos_y = -0.877, pos_z = 464.601, pos_rot = 60 WHERE mobid = 16912797;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 600.075, pos_y = -0.499, pos_z = 421.382, pos_rot = 135 WHERE mobid = 16912798;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 541.465, pos_y = -0.499, pos_z = 428.426, pos_rot = 173 WHERE mobid = 16912799;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 382.887, pos_y = -0.5, pos_z = 498.094, pos_rot = 95 WHERE mobid = 16912800;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 348.822, pos_y = -0.5, pos_z = 493.751, pos_rot = 44 WHERE mobid = 16912801;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 396.501, pos_y = -0.5, pos_z = 532.293, pos_rot = 135 WHERE mobid = 16912802;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 386.42, pos_y = -0.5, pos_z = 468.337, pos_rot = 230 WHERE mobid = 16912803;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 382.042, pos_y = -0.5, pos_z = 519.342, pos_rot = 87 WHERE mobid = 16912804;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 385.099, pos_y = -0.49, pos_z = 576.991, pos_rot = 54 WHERE mobid = 16912805;
UPDATE mob_spawn_points SET mobname = "Omxzomit", polutils_name = "Om\'xzomit", groupid = 58, pos_x = 385.046, pos_y = -0.499, pos_z = 581.937, pos_rot = 72 WHERE mobid = 16912806;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 395.137, pos_y = -0.489, pos_z = 576.25, pos_rot = 168 WHERE mobid = 16912807;
UPDATE mob_spawn_points SET mobname = "Omhpemde", polutils_name = "Om\'hpemde", groupid = 23, pos_x = 454.778, pos_y = -0.499, pos_z = 560.785, pos_rot = 112 WHERE mobid = 16912808;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 402.174, pos_y = -0.493, pos_z = 571.392, pos_rot = 103 WHERE mobid = 16912809;
UPDATE mob_spawn_points SET mobname = "Omphuabo", polutils_name = "Om\'phuabo", groupid = 34, pos_x = 0, pos_y = 0, pos_z = 0, pos_rot = 0 WHERE mobid = 16912810;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 39, pos_x = 300.008, pos_y = -0.5, pos_z = 440.956, pos_rot = 235 WHERE mobid = 16912811;
UPDATE mob_spawn_points SET mobname = "Omyovra", polutils_name = "Om\'yovra", groupid = 37, pos_x = 221.999, pos_y = -0.5, pos_z = 532.245, pos_rot = 223 WHERE mobid = 16912812;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 39, pos_x = -131.443, pos_y = -1.613, pos_z = 587.186, pos_rot = 227 WHERE mobid = 16912813;
UPDATE mob_spawn_points SET mobname = "Omyovra", polutils_name = "Om\'yovra", groupid = 37, pos_x = -275.745, pos_y = -0.594, pos_z = 536.166, pos_rot = 245 WHERE mobid = 16912814;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 39, pos_x = 133.624, pos_y = -0.5, pos_z = 718.888, pos_rot = 96 WHERE mobid = 16912815;
UPDATE mob_spawn_points SET mobname = "Omyovra", polutils_name = "Om\'yovra", groupid = 37, pos_x = 52.772, pos_y = -0.5, pos_z = 653.554, pos_rot = 133 WHERE mobid = 16912816;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 39, pos_x = -328.663, pos_y = -0.5, pos_z = 438.076, pos_rot = 6 WHERE mobid = 16912817;
UPDATE mob_spawn_points SET mobname = "Omyovra", polutils_name = "Om\'yovra", groupid = 37, pos_x = -342.497, pos_y = -0.501, pos_z = 407.63, pos_rot = 30 WHERE mobid = 16912818;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 39, pos_x = -604.573, pos_y = -1.416, pos_z = 340.526, pos_rot = 160 WHERE mobid = 16912819;
UPDATE mob_spawn_points SET mobname = "Omyovra", polutils_name = "Om\'yovra", groupid = 37, pos_x = -552.364, pos_y = -0.5, pos_z = 305.422, pos_rot = 152 WHERE mobid = 16912820;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 39, pos_x = -360.523, pos_y = -0.5, pos_z = 221.878, pos_rot = 138 WHERE mobid = 16912821;
UPDATE mob_spawn_points SET mobname = "Omyovra", polutils_name = "Om\'yovra", groupid = 37, pos_x = -385.62, pos_y = -0.5, pos_z = 252.695, pos_rot = 64 WHERE mobid = 16912822;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 39, pos_x = 392.709, pos_y = -0.5, pos_z = 415.565, pos_rot = 24 WHERE mobid = 16912823;
UPDATE mob_spawn_points SET mobname = "Omyovra", polutils_name = "Om\'yovra", groupid = 37, pos_x = 498.035, pos_y = -0.499, pos_z = 344.175, pos_rot = 120 WHERE mobid = 16912824;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 39, pos_x = 474.343, pos_y = -0.5, pos_z = 68.152, pos_rot = 237 WHERE mobid = 16912825;
UPDATE mob_spawn_points SET mobname = "Omyovra", polutils_name = "Om\'yovra", groupid = 37, pos_x = 480.635, pos_y = -0.496, pos_z = 57.664, pos_rot = 69 WHERE mobid = 16912826;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 39, pos_x = 451.779, pos_y = -0.5, pos_z = -245.322, pos_rot = 145 WHERE mobid = 16912827;
UPDATE mob_spawn_points SET mobname = "Omyovra", polutils_name = "Om\'yovra", groupid = 37, pos_x = 471.569, pos_y = -0.499, pos_z = -102.718, pos_rot = 131 WHERE mobid = 16912828;
UPDATE mob_spawn_points SET mobname = "Ruaern", polutils_name = "Ru\'aern", groupid = 40, pos_x = -0.85, pos_y = -4.256, pos_z = -730.659, pos_rot = 179 WHERE mobid = 16912829;
UPDATE mob_spawn_points SET mobname = "Ruaern", polutils_name = "Ru\'aern", groupid = 41, pos_x = 1.361, pos_y = -3.785, pos_z = -720.858, pos_rot = 60 WHERE mobid = 16912830;
UPDATE mob_spawn_points SET mobname = "Ruaern", polutils_name = "Ru\'aern", groupid = 42, pos_x = 1.755, pos_y = -1.331, pos_z = -704.503, pos_rot = 194 WHERE mobid = 16912831;
UPDATE mob_spawn_points SET mobname = "Ruaern", polutils_name = "Ru\'aern", groupid = 43, pos_x = -676.769, pos_y = -4.256, pos_z = -219.395, pos_rot = 211 WHERE mobid = 16912832;
UPDATE mob_spawn_points SET mobname = "Ruaern", polutils_name = "Ru\'aern", groupid = 44, pos_x = -673.647, pos_y = -4.256, pos_z = -217.548, pos_rot = 132 WHERE mobid = 16912833;
UPDATE mob_spawn_points SET mobname = "Ruaern", polutils_name = "Ru\'aern", groupid = 45, pos_x = -678.09, pos_y = -4.256, pos_z = -220.056, pos_rot = 26 WHERE mobid = 16912834;
UPDATE mob_spawn_points SET mobname = "Ruaern", polutils_name = "Ru\'aern", groupid = 46, pos_x = 676.133, pos_y = -4.256, pos_z = -218.238, pos_rot = 25 WHERE mobid = 16912835;
UPDATE mob_spawn_points SET mobname = "Ruaern", polutils_name = "Ru\'aern", groupid = 47, pos_x = 675.76, pos_y = -4.256, pos_z = -220.229, pos_rot = 144 WHERE mobid = 16912836;
UPDATE mob_spawn_points SET mobname = "Ruaern", polutils_name = "Ru\'aern", groupid = 48, pos_x = 669.889, pos_y = -4.256, pos_z = -217.53, pos_rot = 143 WHERE mobid = 16912837;
UPDATE mob_spawn_points SET mobname = "Jailer_of_Hope", polutils_name = "Jailer of Hope", groupid = 49, pos_x = -683.592, pos_y = -0.5, pos_z = -61.571, pos_rot = 233 WHERE mobid = 16912838;
UPDATE mob_spawn_points SET mobname = "Jailer_of_Justice", polutils_name = "Jailer of Justice", groupid = 50, pos_x = -279.194, pos_y = -0.5, pos_z = -465.362, pos_rot = 246 WHERE mobid = 16912839;
UPDATE mob_spawn_points SET mobname = "Qnxzomit", polutils_name = "Qn\'xzomit", groupid = 51, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 246 WHERE mobid = 16912840;
UPDATE mob_spawn_points SET mobname = "Qnxzomit", polutils_name = "Qn\'xzomit", groupid = 51, pos_x = -295.663, pos_y = -0.06, pos_z = -448.272, pos_rot = 149 WHERE mobid = 16912841;
UPDATE mob_spawn_points SET mobname = "Qnxzomit", polutils_name = "Qn\'xzomit", groupid = 51, pos_x = -287.525, pos_y = -0.5, pos_z = -464.5, pos_rot = 69 WHERE mobid = 16912842;
UPDATE mob_spawn_points SET mobname = "Qnxzomit", polutils_name = "Qn\'xzomit", groupid = 51, pos_x = -284.194, pos_y = -0.5, pos_z = -470.361, pos_rot = 246 WHERE mobid = 16912843;
UPDATE mob_spawn_points SET mobname = "Qnxzomit", polutils_name = "Qn\'xzomit", groupid = 51, pos_x = -295.663, pos_y = -0.06, pos_z = -448.272, pos_rot = 149 WHERE mobid = 16912844;
UPDATE mob_spawn_points SET mobname = "Qnxzomit", polutils_name = "Qn\'xzomit", groupid = 51, pos_x = -287.525, pos_y = -0.5, pos_z = -464.5, pos_rot = 69 WHERE mobid = 16912845;
UPDATE mob_spawn_points SET mobname = "Jailer_of_Prudence", polutils_name = "Jailer of Prudence", groupid = 52, pos_x = 706.114, pos_y = -1.021, pos_z = 24.11, pos_rot = 85 WHERE mobid = 16912846;
UPDATE mob_spawn_points SET mobname = "Jailer_of_Prudence", polutils_name = "Jailer of Prudence", groupid = 52, pos_x = 709.324, pos_y = -1.289, pos_z = 22.126, pos_rot = 85 WHERE mobid = 16912847;
UPDATE mob_spawn_points SET mobname = "Jailer_of_Love", polutils_name = "Jailer of Love", groupid = 53, pos_x = 437.76, pos_y = -1.643, pos_z = -598.571, pos_rot = 4 WHERE mobid = 16912848;
UPDATE mob_spawn_points SET mobname = "Ruphuabo", polutils_name = "Ru\'phuabo", groupid = 54, pos_x = 477.48, pos_y = -1.111, pos_z = -565.864, pos_rot = 39 WHERE mobid = 16912849;
UPDATE mob_spawn_points SET mobname = "Ruphuabo", polutils_name = "Ru\'phuabo", groupid = 54, pos_x = 477.55, pos_y = -1.114, pos_z = -566.001, pos_rot = 38 WHERE mobid = 16912850;
UPDATE mob_spawn_points SET mobname = "Ruphuabo", polutils_name = "Ru\'phuabo", groupid = 54, pos_x = 477.622, pos_y = -1.11, pos_z = -566.194, pos_rot = 37 WHERE mobid = 16912851;
UPDATE mob_spawn_points SET mobname = "Ruphuabo", polutils_name = "Ru\'phuabo", groupid = 54, pos_x = 480.104, pos_y = -1.506, pos_z = -561.952, pos_rot = 252 WHERE mobid = 16912852;
UPDATE mob_spawn_points SET mobname = "Ruphuabo", polutils_name = "Ru\'phuabo", groupid = 54, pos_x = 480.242, pos_y = -1.498, pos_z = -561.964, pos_rot = 252 WHERE mobid = 16912853;
UPDATE mob_spawn_points SET mobname = "Ruphuabo", polutils_name = "Ru\'phuabo", groupid = 54, pos_x = 480.11, pos_y = -1.466, pos_z = -562.257, pos_rot = 248 WHERE mobid = 16912854;
UPDATE mob_spawn_points SET mobname = "Ruphuabo", polutils_name = "Ru\'phuabo", groupid = 54, pos_x = 476.961, pos_y = -1.279, pos_z = -560.968, pos_rot = 247 WHERE mobid = 16912855;
UPDATE mob_spawn_points SET mobname = "Ruphuabo", polutils_name = "Ru\'phuabo", groupid = 54, pos_x = 477.169, pos_y = -1.295, pos_z = -560.921, pos_rot = 247 WHERE mobid = 16912856;
UPDATE mob_spawn_points SET mobname = "Ruphuabo", polutils_name = "Ru\'phuabo", groupid = 54, pos_x = 477.55, pos_y = -1.328, pos_z = -561.215, pos_rot = 242 WHERE mobid = 16912857;
UPDATE mob_spawn_points SET mobname = "Qnxzomit_jol", polutils_name = "Qn\'xzomit", groupid = 61, pos_x = 482.157, pos_y = -1.504, pos_z = -561.017, pos_rot = 92 WHERE mobid = 16912858;
UPDATE mob_spawn_points SET mobname = "Qnxzomit_jol", polutils_name = "Qn\'xzomit", groupid = 61, pos_x = 478.351, pos_y = -1.358, pos_z = -561.169, pos_rot = 31 WHERE mobid = 16912859;
UPDATE mob_spawn_points SET mobname = "Qnxzomit_jol", polutils_name = "Qn\'xzomit", groupid = 61, pos_x = 479.613, pos_y = -1.395, pos_z = -561.4, pos_rot = 46 WHERE mobid = 16912860;
UPDATE mob_spawn_points SET mobname = "Qnxzomit_jol", polutils_name = "Qn\'xzomit", groupid = 61, pos_x = 480.343, pos_y = -1.461, pos_z = -562.054, pos_rot = 250 WHERE mobid = 16912861;
UPDATE mob_spawn_points SET mobname = "Qnxzomit_jol", polutils_name = "Qn\'xzomit", groupid = 61, pos_x = 480.538, pos_y = -1.38, pos_z = -561.452, pos_rot = 4 WHERE mobid = 16912862;
UPDATE mob_spawn_points SET mobname = "Qnxzomit_jol", polutils_name = "Qn\'xzomit", groupid = 61, pos_x = 480.093, pos_y = -1.473, pos_z = -561.951, pos_rot = 252 WHERE mobid = 16912863;
UPDATE mob_spawn_points SET mobname = "Qnxzomit_jol", polutils_name = "Qn\'xzomit", groupid = 61, pos_x = 479.049, pos_y = -1.47, pos_z = -560.512, pos_rot = 248 WHERE mobid = 16912864;
UPDATE mob_spawn_points SET mobname = "Qnxzomit_jol", polutils_name = "Qn\'xzomit", groupid = 61, pos_x = 478.548, pos_y = -1.473, pos_z = -560.292, pos_rot = 40 WHERE mobid = 16912865;
UPDATE mob_spawn_points SET mobname = "Qnxzomit_jol", polutils_name = "Qn\'xzomit", groupid = 61, pos_x = 477.643, pos_y = -1.564, pos_z = -561.005, pos_rot = 26 WHERE mobid = 16912866;
UPDATE mob_spawn_points SET mobname = "Qnhpemde", polutils_name = "Qn\'hpemde", groupid = 55, pos_x = 480.014, pos_y = -1.804, pos_z = -560.765, pos_rot = 13 WHERE mobid = 16912867;
UPDATE mob_spawn_points SET mobname = "Qnhpemde", polutils_name = "Qn\'hpemde", groupid = 55, pos_x = 479.857, pos_y = -1.848, pos_z = -560.908, pos_rot = 10 WHERE mobid = 16912868;
UPDATE mob_spawn_points SET mobname = "Qnhpemde", polutils_name = "Qn\'hpemde", groupid = 55, pos_x = 479.691, pos_y = -1.85, pos_z = -560.698, pos_rot = 41 WHERE mobid = 16912869;
UPDATE mob_spawn_points SET mobname = "Qnhpemde", polutils_name = "Qn\'hpemde", groupid = 55, pos_x = 480.161, pos_y = -1.889, pos_z = -560.058, pos_rot = 59 WHERE mobid = 16912870;
UPDATE mob_spawn_points SET mobname = "Qnhpemde", polutils_name = "Qn\'hpemde", groupid = 55, pos_x = 479.138, pos_y = -1.738, pos_z = -560.684, pos_rot = 119 WHERE mobid = 16912871;
UPDATE mob_spawn_points SET mobname = "Qnhpemde", polutils_name = "Qn\'hpemde", groupid = 55, pos_x = 481.643, pos_y = -1.676, pos_z = -560.652, pos_rot = 83 WHERE mobid = 16912872;
UPDATE mob_spawn_points SET mobname = "Qnhpemde", polutils_name = "Qn\'hpemde", groupid = 55, pos_x = 479.042, pos_y = -1.84, pos_z = -559.745, pos_rot = 28 WHERE mobid = 16912873;
UPDATE mob_spawn_points SET mobname = "Qnhpemde", polutils_name = "Qn\'hpemde", groupid = 55, pos_x = 478.761, pos_y = -1.55, pos_z = -559.849, pos_rot = 25 WHERE mobid = 16912874;
UPDATE mob_spawn_points SET mobname = "Qnhpemde", polutils_name = "Qn\'hpemde", groupid = 55, pos_x = 479.025, pos_y = -1.534, pos_z = -559.847, pos_rot = 27 WHERE mobid = 16912875;
UPDATE mob_spawn_points SET mobname = "Absolute_Virtue", polutils_name = "Absolute Virtue", groupid = 56, pos_x = 461.266, pos_y = -1.643, pos_z = -580.192, pos_rot = 4 WHERE mobid = 16912876;
UPDATE mob_spawn_points SET mobname = "Aerns_Wynav", polutils_name = "Aern\'s Wynav", groupid = 13, pos_x = 463.115, pos_y = -1.346, pos_z = -571.896, pos_rot = 181 WHERE mobid = 16912877;
UPDATE mob_spawn_points SET mobname = "Aerns_Wynav", polutils_name = "Aern\'s Wynav", groupid = 13, pos_x = 463.214, pos_y = -1.336, pos_z = -573.022, pos_rot = 202 WHERE mobid = 16912878;
UPDATE mob_spawn_points SET mobname = "Aerns_Wynav", polutils_name = "Aern\'s Wynav", groupid = 13, pos_x = 463.557, pos_y = -1.36, pos_z = -574.178, pos_rot = 193 WHERE mobid = 16912879;
UPDATE mob_spawn_points SET mobname = "Aerns_Wynav", polutils_name = "Aern\'s Wynav", groupid = 13, pos_x = 463.115, pos_y = -1.346, pos_z = -571.896, pos_rot = 181 WHERE mobid = 16912880;
UPDATE mob_spawn_points SET mobname = "Aerns_Wynav", polutils_name = "Aern\'s Wynav", groupid = 13, pos_x = 463.214, pos_y = -1.336, pos_z = -573.022, pos_rot = 202 WHERE mobid = 16912881;
UPDATE mob_spawn_points SET mobname = "Aerns_Wynav", polutils_name = "Aern\'s Wynav", groupid = 13, pos_x = 463.557, pos_y = -1.36, pos_z = -574.178, pos_rot = 193 WHERE mobid = 16912882;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -459.476, pos_y = -0.5, pos_z = 687.234, pos_rot = 25 WHERE mobid = 16916481;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -460, pos_y = -0.5, pos_z = 740, pos_rot = 120 WHERE mobid = 16916482;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -432.507, pos_y = 0, pos_z = 738.099, pos_rot = 124 WHERE mobid = 16916483;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -408.862, pos_y = 0, pos_z = 737.161, pos_rot = 57 WHERE mobid = 16916484;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -380, pos_y = -0.5, pos_z = 740, pos_rot = 228 WHERE mobid = 16916485;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -371.766, pos_y = -0.5, pos_z = 702.747, pos_rot = 54 WHERE mobid = 16916486;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -380, pos_y = -0.5, pos_z = 660, pos_rot = 220 WHERE mobid = 16916487;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -460, pos_y = -0.5, pos_z = 660, pos_rot = 244 WHERE mobid = 16916488;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -420, pos_y = -1.5, pos_z = 757, pos_rot = 64 WHERE mobid = 16916489;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -388, pos_y = -0.5, pos_z = 700, pos_rot = 0 WHERE mobid = 16916490;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -394, pos_y = -0.5, pos_z = 700, pos_rot = 0 WHERE mobid = 16916491;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -452, pos_y = -0.5, pos_z = 700, pos_rot = 128 WHERE mobid = 16916492;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -446, pos_y = -0.5, pos_z = 700, pos_rot = 128 WHERE mobid = 16916493;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -419.274, pos_y = 0, pos_z = 659.806, pos_rot = 2 WHERE mobid = 16916494;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -419.736, pos_y = 0, pos_z = 584.878, pos_rot = 254 WHERE mobid = 16916495;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -422.432, pos_y = -0.625, pos_z = 619.533, pos_rot = 65 WHERE mobid = 16916496;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -420.598, pos_y = -0.45, pos_z = 612.99, pos_rot = 62 WHERE mobid = 16916497;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -113.072, pos_y = -0.5, pos_z = 500.165, pos_rot = 108 WHERE mobid = 16916498;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -60, pos_y = -0.5, pos_z = 500, pos_rot = 194 WHERE mobid = 16916499;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -67.099, pos_y = -0.5, pos_z = 450.18, pos_rot = 79 WHERE mobid = 16916500;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -60.853, pos_y = -0.5, pos_z = 471.768, pos_rot = 211 WHERE mobid = 16916501;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -60, pos_y = -0.5, pos_z = 420, pos_rot = 14 WHERE mobid = 16916502;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -98.187, pos_y = -0.5, pos_z = 411.89, pos_rot = 104 WHERE mobid = 16916503;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -140, pos_y = -0.5, pos_z = 420, pos_rot = 12 WHERE mobid = 16916504;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -140, pos_y = -0.5, pos_z = 500, pos_rot = 244 WHERE mobid = 16916505;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -100, pos_y = -0.5, pos_z = 492, pos_rot = 192 WHERE mobid = 16916506;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -100, pos_y = -0.5, pos_z = 486, pos_rot = 192 WHERE mobid = 16916507;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -43, pos_y = -1.5, pos_z = 460, pos_rot = 128 WHERE mobid = 16916508;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -100, pos_y = -0.5, pos_z = 428, pos_rot = 64 WHERE mobid = 16916509;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -100, pos_y = -0.5, pos_z = 434, pos_rot = 64 WHERE mobid = 16916510;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -124.715, pos_y = -0.569, pos_z = 458.137, pos_rot = 26 WHERE mobid = 16916511;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -201.825, pos_y = -0.449, pos_z = 460, pos_rot = 127 WHERE mobid = 16916512;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -176.115, pos_y = -0.627, pos_z = 459.107, pos_rot = 190 WHERE mobid = 16916513;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -213.462, pos_y = -0.5, pos_z = 90.375, pos_rot = 235 WHERE mobid = 16916514;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -220, pos_y = -0.5, pos_z = 60, pos_rot = 204 WHERE mobid = 16916515;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -257.461, pos_y = -0.5, pos_z = 68.077, pos_rot = 152 WHERE mobid = 16916516;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -250.422, pos_y = -0.5, pos_z = 67.975, pos_rot = 31 WHERE mobid = 16916517;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -300, pos_y = -0.5, pos_z = 60, pos_rot = 100 WHERE mobid = 16916518;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -300.22, pos_y = -0.5, pos_z = 113.813, pos_rot = 231 WHERE mobid = 16916519;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -300, pos_y = -0.5, pos_z = 140, pos_rot = 20 WHERE mobid = 16916520;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -220, pos_y = -0.5, pos_z = 140, pos_rot = 216 WHERE mobid = 16916521;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -262.46, pos_y = -0.625, pos_z = 126.211, pos_rot = 32 WHERE mobid = 16916522;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -228, pos_y = -0.5, pos_z = 100, pos_rot = 0 WHERE mobid = 16916523;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -234, pos_y = -0.5, pos_z = 100, pos_rot = 0 WHERE mobid = 16916524;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -260, pos_y = -1.5, pos_z = 43, pos_rot = 192 WHERE mobid = 16916525;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -292, pos_y = -0.5, pos_z = 100, pos_rot = 128 WHERE mobid = 16916526;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -286, pos_y = -0.5, pos_z = 100, pos_rot = 128 WHERE mobid = 16916527;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -260.522, pos_y = -0.449, pos_z = 229.995, pos_rot = 193 WHERE mobid = 16916528;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -260.516, pos_y = -0.449, pos_z = 209.732, pos_rot = 193 WHERE mobid = 16916529;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -255.978, pos_y = -0.5, pos_z = 264.477, pos_rot = 240 WHERE mobid = 16916530;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -540.467, pos_y = -0.5, pos_z = 86.848, pos_rot = 83 WHERE mobid = 16916531;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -540, pos_y = -0.5, pos_z = 60, pos_rot = 198 WHERE mobid = 16916532;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -580.321, pos_y = -0.5, pos_z = 61.94, pos_rot = 107 WHERE mobid = 16916533;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -582.122, pos_y = -0.5, pos_z = 65.773, pos_rot = 235 WHERE mobid = 16916534;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -620, pos_y = -0.5, pos_z = 60, pos_rot = 82 WHERE mobid = 16916535;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -627.83, pos_y = -0.5, pos_z = 97.12, pos_rot = 186 WHERE mobid = 16916536;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -620, pos_y = -0.5, pos_z = 140, pos_rot = 180 WHERE mobid = 16916537;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -540, pos_y = -0.5, pos_z = 140, pos_rot = 244 WHERE mobid = 16916538;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -548, pos_y = -0.5, pos_z = 100, pos_rot = 0 WHERE mobid = 16916539;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -554, pos_y = -0.5, pos_z = 100, pos_rot = 0 WHERE mobid = 16916540;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -580, pos_y = -1.5, pos_z = 43, pos_rot = 192 WHERE mobid = 16916541;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -612, pos_y = -0.5, pos_z = 100, pos_rot = 128 WHERE mobid = 16916542;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -606, pos_y = -0.5, pos_z = 100, pos_rot = 128 WHERE mobid = 16916543;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -579.424, pos_y = -0.449, pos_z = 192.245, pos_rot = 192 WHERE mobid = 16916544;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -579.407, pos_y = -0.449, pos_z = 202.605, pos_rot = 192 WHERE mobid = 16916545;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -578.077, pos_y = -0.5, pos_z = 257.78, pos_rot = 113 WHERE mobid = 16916546;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -745.466, pos_y = -0.5, pos_z = 415.081, pos_rot = 238 WHERE mobid = 16916547;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -780, pos_y = -0.5, pos_z = 420, pos_rot = 8 WHERE mobid = 16916548;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -771.539, pos_y = -0.5, pos_z = 462.67, pos_rot = 53 WHERE mobid = 16916549;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -763.496, pos_y = -0.5, pos_z = 460.12, pos_rot = 227 WHERE mobid = 16916550;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -780, pos_y = -0.5, pos_z = 500, pos_rot = 32 WHERE mobid = 16916551;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -731.097, pos_y = -0.5, pos_z = 508.222, pos_rot = 32 WHERE mobid = 16916552;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -700, pos_y = -0.5, pos_z = 500, pos_rot = 220 WHERE mobid = 16916553;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -700, pos_y = -0.5, pos_z = 420, pos_rot = 8 WHERE mobid = 16916554;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -740, pos_y = -0.5, pos_z = 492, pos_rot = 192 WHERE mobid = 16916555;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -740, pos_y = -0.5, pos_z = 486, pos_rot = 192 WHERE mobid = 16916556;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -717.713, pos_y = -0.501, pos_z = 460.307, pos_rot = 97 WHERE mobid = 16916557;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -740, pos_y = -0.5, pos_z = 428, pos_rot = 64 WHERE mobid = 16916558;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -740, pos_y = -0.5, pos_z = 434, pos_rot = 64 WHERE mobid = 16916559;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -797, pos_y = -1.5, pos_z = 460, pos_rot = 0 WHERE mobid = 16916560;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -663.696, pos_y = -0.623, pos_z = 461.88, pos_rot = 128 WHERE mobid = 16916561;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -620.467, pos_y = 0, pos_z = 460.072, pos_rot = 111 WHERE mobid = 16916562;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 4, pos_x = -361.967, pos_y = -0.499, pos_z = 530.738, pos_rot = 127 WHERE mobid = 16916563;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 5, pos_x = -340.935, pos_y = -0.5, pos_z = 530.809, pos_rot = 174 WHERE mobid = 16916564;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 5, pos_x = -379.893, pos_y = -0.482, pos_z = 544.504, pos_rot = 186 WHERE mobid = 16916565;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 6, pos_x = -380.369, pos_y = -0.46, pos_z = 536.796, pos_rot = 186 WHERE mobid = 16916566;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 5, pos_x = -342.098, pos_y = -0.458, pos_z = 538.817, pos_rot = 6 WHERE mobid = 16916567;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 7, pos_x = -355.377, pos_y = -0.5, pos_z = 537.818, pos_rot = 206 WHERE mobid = 16916568;
UPDATE mob_spawn_points SET mobname = "Aerns_Elemental", polutils_name = "Aern\'s Elemental", groupid = 8, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 140 WHERE mobid = 16916569;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -334.869, pos_y = -0.5, pos_z = 494.648, pos_rot = 81 WHERE mobid = 16916570;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -295.599, pos_y = -0.5, pos_z = 503.828, pos_rot = 239 WHERE mobid = 16916571;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -331.126, pos_y = -0.5, pos_z = 499.578, pos_rot = 255 WHERE mobid = 16916572;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 9, pos_x = -310.587, pos_y = -0.499, pos_z = 461.147, pos_rot = 67 WHERE mobid = 16916573;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 10, pos_x = -269.885, pos_y = -0.5, pos_z = 454.917, pos_rot = 181 WHERE mobid = 16916574;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 11, pos_x = -267.858, pos_y = -0.499, pos_z = 451.066, pos_rot = 128 WHERE mobid = 16916575;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 11, pos_x = -309.873, pos_y = -0.499, pos_z = 467.716, pos_rot = 194 WHERE mobid = 16916576;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 12, pos_x = -300.165, pos_y = -0.499, pos_z = 468.973, pos_rot = 250 WHERE mobid = 16916577;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 13, pos_x = -298.866, pos_y = -0.5, pos_z = 451.997, pos_rot = 114 WHERE mobid = 16916578;
UPDATE mob_spawn_points SET mobname = "Aerns_Wynav", polutils_name = "Aern\'s Wynav", groupid = 14, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 5 WHERE mobid = 16916579;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -260, pos_y = -1.5, pos_z = 497, pos_rot = 64 WHERE mobid = 16916580;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -259.996, pos_y = -1, pos_z = 422.713, pos_rot = 193 WHERE mobid = 16916581;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -335.807, pos_y = -0.5, pos_z = 416.267, pos_rot = 16 WHERE mobid = 16916582;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -299.295, pos_y = -0.5, pos_z = 420.352, pos_rot = 142 WHERE mobid = 16916583;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 15, pos_x = -301.791, pos_y = -0.459, pos_z = 341.44, pos_rot = 253 WHERE mobid = 16916584;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 10, pos_x = -307.192, pos_y = -0.5, pos_z = 371.113, pos_rot = 3 WHERE mobid = 16916585;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 16, pos_x = -291.326, pos_y = -0.499, pos_z = 341.48, pos_rot = 62 WHERE mobid = 16916586;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 17, pos_x = -292.19, pos_y = -0.496, pos_z = 380.507, pos_rot = 250 WHERE mobid = 16916587;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 5, pos_x = -300.891, pos_y = -0.474, pos_z = 358.24, pos_rot = 62 WHERE mobid = 16916588;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 18, pos_x = -293.163, pos_y = -0.499, pos_z = 350.425, pos_rot = 255 WHERE mobid = 16916589;
UPDATE mob_spawn_points SET mobname = "Aerns_Euvhi", polutils_name = "Aern\'s Euvhi", groupid = 19, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 1 WHERE mobid = 16916590;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -333.73, pos_y = -0.5, pos_z = 339.464, pos_rot = 211 WHERE mobid = 16916591;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -341.745, pos_y = -0.5, pos_z = 297.393, pos_rot = 75 WHERE mobid = 16916592;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 17, pos_x = -330.757, pos_y = -0.499, pos_z = 260.924, pos_rot = 69 WHERE mobid = 16916593;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 10, pos_x = -350.598, pos_y = -0.5, pos_z = 259.456, pos_rot = 129 WHERE mobid = 16916594;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 17, pos_x = -290.249, pos_y = -0.499, pos_z = 251.838, pos_rot = 55 WHERE mobid = 16916595;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 5, pos_x = -299.706, pos_y = -0.455, pos_z = 258.682, pos_rot = 135 WHERE mobid = 16916596;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 5, pos_x = -350.542, pos_y = -0.496, pos_z = 258.777, pos_rot = 127 WHERE mobid = 16916597;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 13, pos_x = -349.412, pos_y = -0.5, pos_z = 258.956, pos_rot = 101 WHERE mobid = 16916598;
UPDATE mob_spawn_points SET mobname = "Aerns_Wynav", polutils_name = "Aern\'s Wynav", groupid = 14, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 60 WHERE mobid = 16916599;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -300.003, pos_y = -1, pos_z = 297.135, pos_rot = 63 WHERE mobid = 16916600;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -300, pos_y = -1.5, pos_z = 223, pos_rot = 192 WHERE mobid = 16916601;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 15, pos_x = -498.334, pos_y = -0.5, pos_z = 268.135, pos_rot = 133 WHERE mobid = 16916602;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 5, pos_x = -499.512, pos_y = -0.498, pos_z = 268.03, pos_rot = 185 WHERE mobid = 16916603;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 5, pos_x = -524.983, pos_y = -0.5, pos_z = 261.058, pos_rot = 112 WHERE mobid = 16916604;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 20, pos_x = -529.845, pos_y = -0.5, pos_z = 268.233, pos_rot = 1 WHERE mobid = 16916605;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 11, pos_x = -518.352, pos_y = -0.499, pos_z = 268.298, pos_rot = 132 WHERE mobid = 16916606;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 18, pos_x = -548.104, pos_y = -0.495, pos_z = 259.705, pos_rot = 128 WHERE mobid = 16916607;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -539.95, pos_y = -1.5, pos_z = 297.31, pos_rot = 65 WHERE mobid = 16916608;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -540, pos_y = -1.5, pos_z = 223, pos_rot = 192 WHERE mobid = 16916609;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -500.439, pos_y = -0.541, pos_z = 301.782, pos_rot = 169 WHERE mobid = 16916610;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -503.295, pos_y = -0.5, pos_z = 345.6, pos_rot = 111 WHERE mobid = 16916611;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 4, pos_x = -538.834, pos_y = -0.487, pos_z = 330.483, pos_rot = 67 WHERE mobid = 16916612;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 15, pos_x = -530.809, pos_y = -0.5, pos_z = 369.683, pos_rot = 60 WHERE mobid = 16916613;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 5, pos_x = -540.564, pos_y = -0.5, pos_z = 361.08, pos_rot = 158 WHERE mobid = 16916614;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 9, pos_x = -539.526, pos_y = -0.5, pos_z = 370.931, pos_rot = 126 WHERE mobid = 16916615;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 5, pos_x = -549.001, pos_y = -0.499, pos_z = 380.206, pos_rot = 190 WHERE mobid = 16916616;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 7, pos_x = -548.339, pos_y = -0.499, pos_z = 330.241, pos_rot = 130 WHERE mobid = 16916617;
UPDATE mob_spawn_points SET mobname = "Aerns_Elemental", polutils_name = "Aern\'s Elemental", groupid = 8, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 26 WHERE mobid = 16916618;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -500.942, pos_y = -0.5, pos_z = 421.341, pos_rot = 35 WHERE mobid = 16916619;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -506.52, pos_y = -0.5, pos_z = 420.828, pos_rot = 239 WHERE mobid = 16916620;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 5, pos_x = -550.781, pos_y = -0.499, pos_z = 457.064, pos_rot = 63 WHERE mobid = 16916621;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 5, pos_x = -529.797, pos_y = -0.5, pos_z = 463.771, pos_rot = 247 WHERE mobid = 16916622;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 21, pos_x = -580.375, pos_y = -0.454, pos_z = 460.368, pos_rot = 125 WHERE mobid = 16916623;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 9, pos_x = -576.762, pos_y = -0.499, pos_z = 468.885, pos_rot = 129 WHERE mobid = 16916624;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 16, pos_x = -541.297, pos_y = -0.499, pos_z = 450.878, pos_rot = 3 WHERE mobid = 16916625;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 7, pos_x = -591.94, pos_y = -0.5, pos_z = 456.78, pos_rot = 191 WHERE mobid = 16916626;
UPDATE mob_spawn_points SET mobname = "Aerns_Euvhi", polutils_name = "Aern\'s Euvhi", groupid = 19, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 0 WHERE mobid = 16916627;
UPDATE mob_spawn_points SET mobname = "Aerns_Elemental", polutils_name = "Aern\'s Elemental", groupid = 8, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 1 WHERE mobid = 16916628;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -579.99, pos_y = -1.5, pos_z = 497.39, pos_rot = 65 WHERE mobid = 16916629;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -580, pos_y = -1.5, pos_z = 423, pos_rot = 192 WHERE mobid = 16916630;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -540.345, pos_y = -0.5, pos_z = 505.982, pos_rot = 206 WHERE mobid = 16916631;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = -498.604, pos_y = -0.5, pos_z = 499.12, pos_rot = 120 WHERE mobid = 16916632;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = -540.854, pos_y = 0, pos_z = 499.545, pos_rot = 21 WHERE mobid = 16916633;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 5, pos_x = -509.446, pos_y = -0.493, pos_z = 538.992, pos_rot = 121 WHERE mobid = 16916634;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 21, pos_x = -466.171, pos_y = -0.499, pos_z = 547.847, pos_rot = 129 WHERE mobid = 16916635;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 17, pos_x = 0, pos_y = 0, pos_z = 0, pos_rot = 0 WHERE mobid = 16916636;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 5, pos_x = 0, pos_y = 0, pos_z = 0, pos_rot = 0 WHERE mobid = 16916637;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 11, pos_x = -499.896, pos_y = -0.454, pos_z = 539.353, pos_rot = 252 WHERE mobid = 16916638;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 13, pos_x = -509.901, pos_y = -0.499, pos_z = 545.771, pos_rot = 193 WHERE mobid = 16916639;
UPDATE mob_spawn_points SET mobname = "Aerns_Wynav", polutils_name = "Aern\'s Wynav", groupid = 14, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 0 WHERE mobid = 16916640;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 5, pos_x = 0, pos_y = 0, pos_z = 0, pos_rot = 0 WHERE mobid = 16916641;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 10, pos_x = -427.53, pos_y = -0.373, pos_z = 551.194, pos_rot = 117 WHERE mobid = 16916642;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 10, pos_x = 0, pos_y = 0, pos_z = 0, pos_rot = 0 WHERE mobid = 16916643;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 11, pos_x = -420.661, pos_y = -0.494, pos_z = 550.621, pos_rot = 56 WHERE mobid = 16916644;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 16, pos_x = -426.989, pos_y = -0.499, pos_z = 510.598, pos_rot = 133 WHERE mobid = 16916645;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 5, pos_x = -417.645, pos_y = -0.499, pos_z = 551.989, pos_rot = 130 WHERE mobid = 16916646;
UPDATE mob_spawn_points SET mobname = "Aerns_Euvhi", polutils_name = "Aern\'s Euvhi", groupid = 19, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 0 WHERE mobid = 16916647;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -382.84, pos_y = -1.5, pos_z = 500.073, pos_rot = 66 WHERE mobid = 16916648;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = -457, pos_y = -1.5, pos_z = 500, pos_rot = 0 WHERE mobid = 16916649;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = 372, pos_y = -0.5, pos_z = 700, pos_rot = 94 WHERE mobid = 16916650;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 383.949, pos_y = -0.5, pos_z = 745.138, pos_rot = 232 WHERE mobid = 16916651;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 419.614, pos_y = -0.5, pos_z = 724.254, pos_rot = 66 WHERE mobid = 16916652;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 429.207, pos_y = -0.5, pos_z = 735.376, pos_rot = 83 WHERE mobid = 16916653;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 459.027, pos_y = -0.5, pos_z = 739.405, pos_rot = 189 WHERE mobid = 16916654;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = 468, pos_y = -0.5, pos_z = 700, pos_rot = 230 WHERE mobid = 16916655;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 459.601, pos_y = -0.5, pos_z = 653.424, pos_rot = 77 WHERE mobid = 16916656;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 386.217, pos_y = -0.5, pos_z = 660.37, pos_rot = 37 WHERE mobid = 16916657;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = 420, pos_y = -1.5, pos_z = 757, pos_rot = 64 WHERE mobid = 16916658;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 452, pos_y = -0.5, pos_z = 700, pos_rot = 0 WHERE mobid = 16916659;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 446, pos_y = -0.5, pos_z = 700, pos_rot = 0 WHERE mobid = 16916660;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 419.989, pos_y = -0.51, pos_z = 672.085, pos_rot = 82 WHERE mobid = 16916661;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 388, pos_y = -0.5, pos_z = 700, pos_rot = 128 WHERE mobid = 16916662;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 0, pos_y = 0, pos_z = 0, pos_rot = 0 WHERE mobid = 16916663;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 419.999, pos_y = -0.5, pos_z = 591.919, pos_rot = 58 WHERE mobid = 16916664;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 419.815, pos_y = -0.475, pos_z = 625.911, pos_rot = 208 WHERE mobid = 16916665;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 421.889, pos_y = -0.594, pos_z = 615.689, pos_rot = 214 WHERE mobid = 16916666;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 420.21, pos_y = 0, pos_z = 577.126, pos_rot = 191 WHERE mobid = 16916667;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = 740, pos_y = -0.5, pos_z = 508, pos_rot = 58 WHERE mobid = 16916668;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 775.911, pos_y = -0.5, pos_z = 495.024, pos_rot = 79 WHERE mobid = 16916669;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 780.138, pos_y = -0.5, pos_z = 446.967, pos_rot = 25 WHERE mobid = 16916670;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 778.904, pos_y = -0.5, pos_z = 471.695, pos_rot = 107 WHERE mobid = 16916671;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 780.51, pos_y = -0.5, pos_z = 426.906, pos_rot = 204 WHERE mobid = 16916672;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = 740, pos_y = -0.5, pos_z = 412, pos_rot = 142 WHERE mobid = 16916673;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 695.983, pos_y = -0.499, pos_z = 415.565, pos_rot = 101 WHERE mobid = 16916674;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 697.064, pos_y = -0.503, pos_z = 504.219, pos_rot = 51 WHERE mobid = 16916675;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 740, pos_y = -0.5, pos_z = 492, pos_rot = 192 WHERE mobid = 16916676;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 740, pos_y = -0.5, pos_z = 486, pos_rot = 192 WHERE mobid = 16916677;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = 797.332, pos_y = 0, pos_z = 460.056, pos_rot = 90 WHERE mobid = 16916678;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 740, pos_y = -0.5, pos_z = 428, pos_rot = 64 WHERE mobid = 16916679;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 740, pos_y = -0.5, pos_z = 434, pos_rot = 64 WHERE mobid = 16916680;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 669.78, pos_y = -0.451, pos_z = 460.527, pos_rot = 3 WHERE mobid = 16916681;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 619.703, pos_y = 0, pos_z = 459.799, pos_rot = 58 WHERE mobid = 16916682;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = 628, pos_y = -0.5, pos_z = 100, pos_rot = 56 WHERE mobid = 16916683;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 626.763, pos_y = -0.5, pos_z = 60.368, pos_rot = 211 WHERE mobid = 16916684;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 591.921, pos_y = 0, pos_z = 62.64, pos_rot = 163 WHERE mobid = 16916685;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 567.511, pos_y = 0, pos_z = 63.181, pos_rot = 0 WHERE mobid = 16916686;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 543.979, pos_y = -0.5, pos_z = 64.854, pos_rot = 222 WHERE mobid = 16916687;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = 532, pos_y = -0.5, pos_z = 100, pos_rot = 130 WHERE mobid = 16916688;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 536.473, pos_y = -0.5, pos_z = 140.215, pos_rot = 174 WHERE mobid = 16916689;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 619.569, pos_y = -0.5, pos_z = 139.567, pos_rot = 128 WHERE mobid = 16916690;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 579.202, pos_y = -0.511, pos_z = 127.288, pos_rot = 217 WHERE mobid = 16916691;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 612, pos_y = -0.5, pos_z = 100, pos_rot = 0 WHERE mobid = 16916692;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 606, pos_y = -0.5, pos_z = 100, pos_rot = 0 WHERE mobid = 16916693;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = 580, pos_y = -1.5, pos_z = 43, pos_rot = 192 WHERE mobid = 16916694;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 548, pos_y = -0.5, pos_z = 100, pos_rot = 128 WHERE mobid = 16916695;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 554, pos_y = -0.5, pos_z = 100, pos_rot = 128 WHERE mobid = 16916696;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 580.104, pos_y = -0.449, pos_z = 209.352, pos_rot = 191 WHERE mobid = 16916697;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 580.17, pos_y = 0, pos_z = 179.474, pos_rot = 191 WHERE mobid = 16916698;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 580.329, pos_y = -0.5, pos_z = 266.563, pos_rot = 195 WHERE mobid = 16916699;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = 308, pos_y = -0.5, pos_z = 100, pos_rot = 232 WHERE mobid = 16916700;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 300.301, pos_y = -0.5, pos_z = 53.104, pos_rot = 50 WHERE mobid = 16916701;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 248.553, pos_y = 0, pos_z = 63.962, pos_rot = 94 WHERE mobid = 16916702;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 272.251, pos_y = 0, pos_z = 62.6, pos_rot = 219 WHERE mobid = 16916703;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 221.391, pos_y = -0.5, pos_z = 64.11, pos_rot = 182 WHERE mobid = 16916704;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = 212, pos_y = -0.5, pos_z = 100, pos_rot = 24 WHERE mobid = 16916705;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 216.671, pos_y = -0.5, pos_z = 136.377, pos_rot = 92 WHERE mobid = 16916706;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 293.213, pos_y = -0.499, pos_z = 140.51, pos_rot = 42 WHERE mobid = 16916707;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 292, pos_y = -0.5, pos_z = 100, pos_rot = 0 WHERE mobid = 16916708;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 286, pos_y = -0.5, pos_z = 100, pos_rot = 0 WHERE mobid = 16916709;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = 260, pos_y = -1.5, pos_z = 43, pos_rot = 192 WHERE mobid = 16916710;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 228, pos_y = -0.5, pos_z = 100, pos_rot = 128 WHERE mobid = 16916711;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 234, pos_y = -0.5, pos_z = 100, pos_rot = 128 WHERE mobid = 16916712;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 258.157, pos_y = -0.603, pos_z = 217.772, pos_rot = 88 WHERE mobid = 16916713;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 259.404, pos_y = -0.481, pos_z = 179.908, pos_rot = 212 WHERE mobid = 16916714;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 259.553, pos_y = -0.5, pos_z = 253.605, pos_rot = 48 WHERE mobid = 16916715;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = 100, pos_y = -0.5, pos_z = 412, pos_rot = 90 WHERE mobid = 16916716;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 59.378, pos_y = -0.503, pos_z = 413.459, pos_rot = 48 WHERE mobid = 16916717;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 59.678, pos_y = -0.5, pos_z = 472.802, pos_rot = 167 WHERE mobid = 16916718;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 65.799, pos_y = -0.5, pos_z = 461.594, pos_rot = 42 WHERE mobid = 16916719;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 59.321, pos_y = -0.5, pos_z = 493.433, pos_rot = 59 WHERE mobid = 16916720;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = 100, pos_y = -0.5, pos_z = 508, pos_rot = 24 WHERE mobid = 16916721;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 134.551, pos_y = -0.5, pos_z = 504.524, pos_rot = 128 WHERE mobid = 16916722;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 139.13, pos_y = -0.508, pos_z = 412.67, pos_rot = 19 WHERE mobid = 16916723;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 100, pos_y = -0.5, pos_z = 492, pos_rot = 192 WHERE mobid = 16916724;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 100, pos_y = -0.5, pos_z = 486, pos_rot = 192 WHERE mobid = 16916725;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 100, pos_y = -0.5, pos_z = 428, pos_rot = 64 WHERE mobid = 16916726;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 100, pos_y = -0.5, pos_z = 434, pos_rot = 64 WHERE mobid = 16916727;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = 43, pos_y = -1.5, pos_z = 460, pos_rot = 0 WHERE mobid = 16916728;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 173.599, pos_y = -0.449, pos_z = 459.391, pos_rot = 127 WHERE mobid = 16916729;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 196.363, pos_y = -0.449, pos_z = 459.426, pos_rot = 0 WHERE mobid = 16916730;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 4, pos_x = 0, pos_y = 0, pos_z = 0, pos_rot = 0 WHERE mobid = 16916731;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 5, pos_x = 450.296, pos_y = -0.499, pos_z = 537.254, pos_rot = 194 WHERE mobid = 16916732;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 5, pos_x = 491.027, pos_y = -0.499, pos_z = 532.009, pos_rot = 69 WHERE mobid = 16916733;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 6, pos_x = 512.187, pos_y = -0.499, pos_z = 544.636, pos_rot = 185 WHERE mobid = 16916734;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 21, pos_x = 513.137, pos_y = -0.499, pos_z = 538.309, pos_rot = 201 WHERE mobid = 16916735;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 7, pos_x = 483.904, pos_y = -0.5, pos_z = 542.385, pos_rot = 174 WHERE mobid = 16916736;
UPDATE mob_spawn_points SET mobname = "Aerns_Elemental", polutils_name = "Aern\'s Elemental", groupid = 8, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 1 WHERE mobid = 16916737;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 507.726, pos_y = -0.5, pos_z = 500.261, pos_rot = 44 WHERE mobid = 16916738;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 536.023, pos_y = -0.5, pos_z = 505.133, pos_rot = 143 WHERE mobid = 16916739;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 9, pos_x = 0, pos_y = 0, pos_z = 0, pos_rot = 0 WHERE mobid = 16916740;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 9, pos_x = 0, pos_y = 0, pos_z = 0, pos_rot = 0 WHERE mobid = 16916741;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 10, pos_x = 540.309, pos_y = -0.499, pos_z = 451.916, pos_rot = 70 WHERE mobid = 16916742;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 11, pos_x = 589.263, pos_y = -0.493, pos_z = 461.498, pos_rot = 0 WHERE mobid = 16916743;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 12, pos_x = 580.153, pos_y = -0.46, pos_z = 463.355, pos_rot = 67 WHERE mobid = 16916744;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 13, pos_x = 549.018, pos_y = -0.496, pos_z = 460.996, pos_rot = 2 WHERE mobid = 16916745;
UPDATE mob_spawn_points SET mobname = "Aerns_Wynav", polutils_name = "Aern\'s Wynav", groupid = 14, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 1 WHERE mobid = 16916746;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = 540, pos_y = -1.5, pos_z = 423, pos_rot = 192 WHERE mobid = 16916747;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 579.621, pos_y = -0.5, pos_z = 427.569, pos_rot = 160 WHERE mobid = 16916748;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 592.384, pos_y = -4.438, pos_z = 383.096, pos_rot = 246 WHERE mobid = 16916749;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 577.819, pos_y = -4.499, pos_z = 371.81, pos_rot = 202 WHERE mobid = 16916750;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 539.428, pos_y = -6.5, pos_z = 372.871, pos_rot = 55 WHERE mobid = 16916751;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 553.602, pos_y = -4, pos_z = 339.501, pos_rot = 255 WHERE mobid = 16916752;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 541.21, pos_y = -4, pos_z = 328.205, pos_rot = 219 WHERE mobid = 16916753;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 539.451, pos_y = -6.5, pos_z = 372.693, pos_rot = 64 WHERE mobid = 16916754;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 499.412, pos_y = -0.5, pos_z = 346.783, pos_rot = 225 WHERE mobid = 16916755;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 4, pos_x = 530.047, pos_y = -0.499, pos_z = 250.603, pos_rot = 127 WHERE mobid = 16916756;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 10, pos_x = 519.475, pos_y = -0.497, pos_z = 251.08, pos_rot = 62 WHERE mobid = 16916757;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 11, pos_x = 489.708, pos_y = -0.496, pos_z = 258.62, pos_rot = 127 WHERE mobid = 16916758;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 12, pos_x = 529.5, pos_y = -0.499, pos_z = 250.378, pos_rot = 4 WHERE mobid = 16916759;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 13, pos_x = 0, pos_y = 0, pos_z = 0, pos_rot = 0 WHERE mobid = 16916760;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 13, pos_x = 0, pos_y = 0, pos_z = 0, pos_rot = 0 WHERE mobid = 16916761;
UPDATE mob_spawn_points SET mobname = "Aerns_Wynav", polutils_name = "Aern\'s Wynav", groupid = 14, pos_x = 0, pos_y = 0, pos_z = 0, pos_rot = 0 WHERE mobid = 16916762;
UPDATE mob_spawn_points SET mobname = "Aerns_Wynav", polutils_name = "Aern\'s Wynav", groupid = 14, pos_x = 0, pos_y = 0, pos_z = 0, pos_rot = 0 WHERE mobid = 16916763;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = 540, pos_y = -1.5, pos_z = 297, pos_rot = 64 WHERE mobid = 16916764;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 455.904, pos_y = -0.5, pos_z = 263.539, pos_rot = 189 WHERE mobid = 16916765;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 460.533, pos_y = -0.5, pos_z = 220.042, pos_rot = 30 WHERE mobid = 16916766;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 420.896, pos_y = -0.5, pos_z = 230.677, pos_rot = 200 WHERE mobid = 16916767;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 432.615, pos_y = -6.5, pos_z = 188.587, pos_rot = 190 WHERE mobid = 16916768;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 405.141, pos_y = -6.5, pos_z = 192.108, pos_rot = 254 WHERE mobid = 16916769;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 416.176, pos_y = -6.5, pos_z = 173.915, pos_rot = 32 WHERE mobid = 16916770;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 380.359, pos_y = -0.5, pos_z = 213.219, pos_rot = 22 WHERE mobid = 16916771;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 382.402, pos_y = -0.5, pos_z = 258.008, pos_rot = 57 WHERE mobid = 16916772;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 428.145, pos_y = -0.5, pos_z = 220.594, pos_rot = 121 WHERE mobid = 16916773;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 15, pos_x = 331.271, pos_y = -0.496, pos_z = 260.938, pos_rot = 125 WHERE mobid = 16916774;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 6, pos_x = 340.193, pos_y = -0.499, pos_z = 269.732, pos_rot = 131 WHERE mobid = 16916775;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 5, pos_x = 0, pos_y = 0, pos_z = 0, pos_rot = 0 WHERE mobid = 16916776;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 21, pos_x = 309.811, pos_y = -0.5, pos_z = 268.648, pos_rot = 191 WHERE mobid = 16916777;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 11, pos_x = 299.386, pos_y = -0.455, pos_z = 260.186, pos_rot = 192 WHERE mobid = 16916778;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 5, pos_x = 0, pos_y = 0, pos_z = 0, pos_rot = 0 WHERE mobid = 16916779;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = 299.99, pos_y = -1.5, pos_z = 297.211, pos_rot = 65 WHERE mobid = 16916780;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 339.515, pos_y = -0.5, pos_z = 347.395, pos_rot = 187 WHERE mobid = 16916781;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 300.96, pos_y = -4.609, pos_z = 332.341, pos_rot = 128 WHERE mobid = 16916782;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 291.315, pos_y = -4.5, pos_z = 338.783, pos_rot = 68 WHERE mobid = 16916783;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 303.718, pos_y = -6.5, pos_z = 384.779, pos_rot = 253 WHERE mobid = 16916784;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 259.804, pos_y = -4.5, pos_z = 372.308, pos_rot = 30 WHERE mobid = 16916785;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi_open", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 261.809, pos_y = -4.5, pos_z = 366.123, pos_rot = 51 WHERE mobid = 16916786;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 299.51, pos_y = -6.5, pos_z = 372.571, pos_rot = 64 WHERE mobid = 16916787;
UPDATE mob_spawn_points SET mobname = "Eoeuvhi", polutils_name = "Eo\'euvhi", groupid = 3, pos_x = 252.814, pos_y = -0.499, pos_z = 419.99, pos_rot = 97 WHERE mobid = 16916788;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 4, pos_x = 269.351, pos_y = -0.499, pos_z = 460.312, pos_rot = 58 WHERE mobid = 16916789;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 15, pos_x = 259.77, pos_y = -0.464, pos_z = 461.541, pos_rot = 69 WHERE mobid = 16916790;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 5, pos_x = 310.616, pos_y = -0.499, pos_z = 466.51, pos_rot = 188 WHERE mobid = 16916791;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 5, pos_x = 251.179, pos_y = -0.493, pos_z = 458.547, pos_rot = 127 WHERE mobid = 16916792;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 7, pos_x = 307.854, pos_y = -0.416, pos_z = 464.423, pos_rot = 37 WHERE mobid = 16916793;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 7, pos_x = 257.263, pos_y = -0.465, pos_z = 460.441, pos_rot = 20 WHERE mobid = 16916794;
UPDATE mob_spawn_points SET mobname = "Aerns_Elemental", polutils_name = "Aern\'s Elemental", groupid = 8, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 1 WHERE mobid = 16916795;
UPDATE mob_spawn_points SET mobname = "Aerns_Elemental", polutils_name = "Aern\'s Elemental", groupid = 8, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 1 WHERE mobid = 16916796;
UPDATE mob_spawn_points SET mobname = "Eozdei", polutils_name = "Eo\'zdei", groupid = 2, pos_x = 0, pos_y = 0, pos_z = 0, pos_rot = 0 WHERE mobid = 16916797;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 300.049, pos_y = -0.5, pos_z = 492.941, pos_rot = 25 WHERE mobid = 16916798;
UPDATE mob_spawn_points SET mobname = "Eoghrah", polutils_name = "Eo\'ghrah", groupid = 1, pos_x = 338.607, pos_y = -0.5, pos_z = 498.809, pos_rot = 51 WHERE mobid = 16916799;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 5, pos_x = 392.075, pos_y = -0.499, pos_z = 541.988, pos_rot = 61 WHERE mobid = 16916800;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 5, pos_x = 327.221, pos_y = -0.5, pos_z = 531.471, pos_rot = 107 WHERE mobid = 16916801;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 17, pos_x = 388.705, pos_y = -0.209, pos_z = 541.737, pos_rot = 251 WHERE mobid = 16916802;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 21, pos_x = 370.264, pos_y = -0.499, pos_z = 545.329, pos_rot = 59 WHERE mobid = 16916803;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 11, pos_x = 380.172, pos_y = -0.488, pos_z = 547.261, pos_rot = 199 WHERE mobid = 16916804;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 13, pos_x = 330.612, pos_y = -0.494, pos_z = 541.529, pos_rot = 121 WHERE mobid = 16916805;
UPDATE mob_spawn_points SET mobname = "Aerns_Wynav", polutils_name = "Aern\'s Wynav", groupid = 14, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 1 WHERE mobid = 16916806;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 5, pos_x = 0, pos_y = 0, pos_z = 0, pos_rot = 0 WHERE mobid = 16916807;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 10, pos_x = 428.594, pos_y = -0.499, pos_z = 519.912, pos_rot = 197 WHERE mobid = 16916808;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 16, pos_x = 411.679, pos_y = -0.497, pos_z = 500.796, pos_rot = 126 WHERE mobid = 16916809;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 18, pos_x = 419.406, pos_y = -0.494, pos_z = 550.289, pos_rot = 192 WHERE mobid = 16916810;
UPDATE mob_spawn_points SET mobname = "Eoaern", polutils_name = "Eo\'aern", groupid = 5, pos_x = 0, pos_y = 0, pos_z = 0, pos_rot = 0 WHERE mobid = 16916811;
UPDATE mob_spawn_points SET mobname = "Aerns_Euvhi", polutils_name = "Aern\'s Euvhi", groupid = 19, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 0 WHERE mobid = 16916812;
UPDATE mob_spawn_points SET mobname = "Ixghrah", polutils_name = "Ix\'ghrah", groupid = 22, pos_x = 419.673, pos_y = -0.5, pos_z = 403.349, pos_rot = 224 WHERE mobid = 16916813;
UPDATE mob_spawn_points SET mobname = "Jailer_of_Temperance", polutils_name = "Jailer of Temperance", groupid = 23, pos_x = -426.739, pos_y = -0.5, pos_z = 687.728, pos_rot = 140 WHERE mobid = 16916814;
UPDATE mob_spawn_points SET mobname = "Ixaern_MNK", polutils_name = "Ix\'aern", groupid = 24, pos_x = 112.783, pos_y = -0.5, pos_z = 453.875, pos_rot = 193 WHERE mobid = 16916815;
UPDATE mob_spawn_points SET mobname = "Qnaern", polutils_name = "Qn\'aern", groupid = 25, pos_x = 460, pos_y = -0.5, pos_z = 543, pos_rot = 128 WHERE mobid = 16916816;
UPDATE mob_spawn_points SET mobname = "Qnaern", polutils_name = "Qn\'aern", groupid = 26, pos_x = 460, pos_y = -0.5, pos_z = 537, pos_rot = 128 WHERE mobid = 16916817;
UPDATE mob_spawn_points SET mobname = "Qnzdei", polutils_name = "Qn\'zdei", groupid = 1, pos_x = -429, pos_y = -1, pos_z = 427, pos_rot = 216 WHERE mobid = 16920577;
UPDATE mob_spawn_points SET mobname = "Qnzdei", polutils_name = "Qn\'zdei", groupid = 1, pos_x = -411, pos_y = -1, pos_z = 427, pos_rot = 216 WHERE mobid = 16920578;
UPDATE mob_spawn_points SET mobname = "Qnzdei", polutils_name = "Qn\'zdei", groupid = 1, pos_x = -429, pos_y = -1, pos_z = 413, pos_rot = 131 WHERE mobid = 16920579;
UPDATE mob_spawn_points SET mobname = "Qnzdei", polutils_name = "Qn\'zdei", groupid = 1, pos_x = -411, pos_y = -1, pos_z = 413, pos_rot = 144 WHERE mobid = 16920580;
UPDATE mob_spawn_points SET mobname = "Qnzdei", polutils_name = "Qn\'zdei", groupid = 1, pos_x = -467, pos_y = -1, pos_z = 371, pos_rot = 17 WHERE mobid = 16920581;
UPDATE mob_spawn_points SET mobname = "Qnzdei", polutils_name = "Qn\'zdei", groupid = 1, pos_x = -467, pos_y = -1, pos_z = 389, pos_rot = 61 WHERE mobid = 16920582;
UPDATE mob_spawn_points SET mobname = "Qnzdei", polutils_name = "Qn\'zdei", groupid = 1, pos_x = -453, pos_y = -1, pos_z = 371, pos_rot = 76 WHERE mobid = 16920583;
UPDATE mob_spawn_points SET mobname = "Qnzdei", polutils_name = "Qn\'zdei", groupid = 1, pos_x = -453, pos_y = -1, pos_z = 389, pos_rot = 170 WHERE mobid = 16920584;
UPDATE mob_spawn_points SET mobname = "Qnzdei", polutils_name = "Qn\'zdei", groupid = 1, pos_x = -373, pos_y = -1, pos_z = 389, pos_rot = 28 WHERE mobid = 16920585;
UPDATE mob_spawn_points SET mobname = "Qnzdei", polutils_name = "Qn\'zdei", groupid = 1, pos_x = -373, pos_y = -1, pos_z = 371, pos_rot = 218 WHERE mobid = 16920586;
UPDATE mob_spawn_points SET mobname = "Qnzdei", polutils_name = "Qn\'zdei", groupid = 1, pos_x = -387, pos_y = -1, pos_z = 389, pos_rot = 205 WHERE mobid = 16920587;
UPDATE mob_spawn_points SET mobname = "Qnzdei", polutils_name = "Qn\'zdei", groupid = 1, pos_x = -387, pos_y = -1, pos_z = 371, pos_rot = 182 WHERE mobid = 16920588;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 2, pos_x = -548.509, pos_y = -0.589, pos_z = 339.32, pos_rot = 244 WHERE mobid = 16920589;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 3, pos_x = -547.53, pos_y = -0.599, pos_z = 325.165, pos_rot = 102 WHERE mobid = 16920590;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 4, pos_x = -553.217, pos_y = -0.599, pos_z = 380.165, pos_rot = 252 WHERE mobid = 16920591;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 5, pos_x = -535.541, pos_y = -0.584, pos_z = 381.429, pos_rot = 119 WHERE mobid = 16920592;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 4, pos_x = -429.625, pos_y = -0.598, pos_z = 468.718, pos_rot = 189 WHERE mobid = 16920593;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 6, pos_x = -415.463, pos_y = -0.593, pos_z = 479.03, pos_rot = 252 WHERE mobid = 16920594;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 7, pos_x = -413.488, pos_y = -0.599, pos_z = 514.165, pos_rot = 184 WHERE mobid = 16920595;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 8, pos_x = -420.218, pos_y = -0.6, pos_z = 516.003, pos_rot = 116 WHERE mobid = 16920596;
UPDATE mob_spawn_points SET mobname = "Aerns_Wynav", polutils_name = "Aern\'s Wynav", groupid = 9, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 0 WHERE mobid = 16920597;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 10, pos_x = -300.026, pos_y = -0.599, pos_z = 396.202, pos_rot = 161 WHERE mobid = 16920598;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 11, pos_x = -300.09, pos_y = -0.563, pos_z = 360.661, pos_rot = 69 WHERE mobid = 16920599;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 4, pos_x = -308.556, pos_y = -0.599, pos_z = 347.083, pos_rot = 189 WHERE mobid = 16920600;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 12, pos_x = -307.286, pos_y = -0.599, pos_z = 329.338, pos_rot = 139 WHERE mobid = 16920601;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -504.728, pos_y = -0.582, pos_z = 245.963, pos_rot = 127 WHERE mobid = 16920602;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -336.642, pos_y = -0.579, pos_z = 246.126, pos_rot = 128 WHERE mobid = 16920603;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -489.188, pos_y = -0.597, pos_z = 468.245, pos_rot = 171 WHERE mobid = 16920604;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -326.892, pos_y = -0.589, pos_z = 461.949, pos_rot = 52 WHERE mobid = 16920605;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 14, pos_x = -499.184, pos_y = -3.56, pos_z = 298.118, pos_rot = 199 WHERE mobid = 16920606;
UPDATE mob_spawn_points SET mobname = "Aerns_Euvhi", polutils_name = "Aern\'s Euvhi", groupid = 15, pos_x = -418.138, pos_y = -0.579, pos_z = 276.082, pos_rot = 65 WHERE mobid = 16920607;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 16, pos_x = -497.752, pos_y = -3.801, pos_z = 293.193, pos_rot = 127 WHERE mobid = 16920608;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 17, pos_x = -505.033, pos_y = -4.579, pos_z = 426.579, pos_rot = 134 WHERE mobid = 16920609;
UPDATE mob_spawn_points SET mobname = "Aerns_Elemental", polutils_name = "Aern\'s Elemental", groupid = 18, pos_x = -593.929, pos_y = 4.5, pos_z = 436.19, pos_rot = 22 WHERE mobid = 16920610;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 4, pos_x = -487.695, pos_y = -0.774, pos_z = 427.142, pos_rot = 255 WHERE mobid = 16920611;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 6, pos_x = -339.114, pos_y = -4.185, pos_z = 428.657, pos_rot = 68 WHERE mobid = 16920612;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 12, pos_x = -332.157, pos_y = -4.856, pos_z = 413.972, pos_rot = 126 WHERE mobid = 16920613;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 10, pos_x = -345.665, pos_y = -4.593, pos_z = 307.345, pos_rot = 130 WHERE mobid = 16920614;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 2, pos_x = -343.505, pos_y = -3, pos_z = 294.098, pos_rot = 126 WHERE mobid = 16920615;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -460.326, pos_y = -0.599, pos_z = 307.02, pos_rot = 217 WHERE mobid = 16920616;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -498.997, pos_y = -0.539, pos_z = 343.077, pos_rot = 92 WHERE mobid = 16920617;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -506.674, pos_y = -0.599, pos_z = 380.256, pos_rot = 146 WHERE mobid = 16920618;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -462.836, pos_y = -0.599, pos_z = 417.66, pos_rot = 120 WHERE mobid = 16920619;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -538.53, pos_y = -0.599, pos_z = 425.427, pos_rot = 21 WHERE mobid = 16920620;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -382.342, pos_y = -0.598, pos_z = 422.991, pos_rot = 47 WHERE mobid = 16920621;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -296.895, pos_y = -0.598, pos_z = 417.378, pos_rot = 93 WHERE mobid = 16920622;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -378.367, pos_y = -0.599, pos_z = 342.681, pos_rot = 216 WHERE mobid = 16920623;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -338.803, pos_y = -0.599, pos_z = 382.425, pos_rot = 229 WHERE mobid = 16920624;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -343.272, pos_y = -0.6, pos_z = 339.849, pos_rot = 2 WHERE mobid = 16920625;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -500.491, pos_y = -6.599, pos_z = 366.498, pos_rot = 192 WHERE mobid = 16920626;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -340.497, pos_y = -6.504, pos_z = 361.467, pos_rot = 192 WHERE mobid = 16920627;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -547, pos_y = 1.5, pos_z = 293, pos_rot = 224 WHERE mobid = 16920628;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -548.432, pos_y = 1.399, pos_z = 300.749, pos_rot = 218 WHERE mobid = 16920629;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -537.38, pos_y = 0, pos_z = 301.729, pos_rot = 203 WHERE mobid = 16920630;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -547, pos_y = 1.5, pos_z = 467, pos_rot = 32 WHERE mobid = 16920631;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -547.04, pos_y = -0.291, pos_z = 450.548, pos_rot = 65 WHERE mobid = 16920632;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -537.328, pos_y = 0, pos_z = 458.119, pos_rot = 79 WHERE mobid = 16920633;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -467, pos_y = 1.5, pos_z = 507, pos_rot = 32 WHERE mobid = 16920634;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -463.362, pos_y = 1.394, pos_z = 507.511, pos_rot = 102 WHERE mobid = 16920635;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -466.225, pos_y = -0.314, pos_z = 490.8, pos_rot = 66 WHERE mobid = 16920636;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -373, pos_y = 1.5, pos_z = 507, pos_rot = 96 WHERE mobid = 16920637;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -384.016, pos_y = 0.648, pos_z = 506.646, pos_rot = 0 WHERE mobid = 16920638;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -302.967, pos_y = 0, pos_z = 458.358, pos_rot = 123 WHERE mobid = 16920639;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -293, pos_y = 1.5, pos_z = 467, pos_rot = 96 WHERE mobid = 16920640;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -285.436, pos_y = 1.399, pos_z = 455.968, pos_rot = 45 WHERE mobid = 16920641;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -302.967, pos_y = 0, pos_z = 458.358, pos_rot = 223 WHERE mobid = 16920642;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -293, pos_y = 1.5, pos_z = 293, pos_rot = 160 WHERE mobid = 16920643;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -286.113, pos_y = 1.399, pos_z = 301.312, pos_rot = 65 WHERE mobid = 16920644;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -301.91, pos_y = 0, pos_z = 302.814, pos_rot = 251 WHERE mobid = 16920645;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 4, pos_x = -565.475, pos_y = 3.429, pos_z = 225.344, pos_rot = 121 WHERE mobid = 16920646;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 12, pos_x = -572.602, pos_y = 3.4, pos_z = 250.923, pos_rot = 220 WHERE mobid = 16920647;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 17, pos_x = -560.618, pos_y = 4.4, pos_z = 238.588, pos_rot = 9 WHERE mobid = 16920648;
UPDATE mob_spawn_points SET mobname = "Aerns_Elemental", polutils_name = "Aern\'s Elemental", groupid = 18, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 0 WHERE mobid = 16920649;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -580.375, pos_y = -0.549, pos_z = 176.519, pos_rot = 191 WHERE mobid = 16920650;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 6, pos_x = -592.21, pos_y = 3.4, pos_z = 456.318, pos_rot = 145 WHERE mobid = 16920651;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 4, pos_x = -584.828, pos_y = 3.4, pos_z = 439.509, pos_rot = 193 WHERE mobid = 16920652;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 3, pos_x = -608.558, pos_y = 3.414, pos_z = 424.864, pos_rot = 23 WHERE mobid = 16920653;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -647.097, pos_y = -0.529, pos_z = 460.204, pos_rot = 128 WHERE mobid = 16920654;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 5, pos_x = -428.841, pos_y = 5.4, pos_z = 530.952, pos_rot = 43 WHERE mobid = 16920655;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 7, pos_x = -413.601, pos_y = 5.4, pos_z = 590.178, pos_rot = 190 WHERE mobid = 16920656;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 14, pos_x = -427.387, pos_y = 5.4, pos_z = 579.235, pos_rot = 46 WHERE mobid = 16920657;
UPDATE mob_spawn_points SET mobname = "Aerns_Euvhi", polutils_name = "Aern\'s Euvhi", groupid = 15, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 0 WHERE mobid = 16920658;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -420.617, pos_y = -0.549, pos_z = 623.35, pos_rot = 192 WHERE mobid = 16920659;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 5, pos_x = -232.885, pos_y = 4.399, pos_z = 439.697, pos_rot = 30 WHERE mobid = 16920660;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 11, pos_x = -238.99, pos_y = 4.409, pos_z = 447.597, pos_rot = 129 WHERE mobid = 16920661;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 8, pos_x = -235.86, pos_y = 4.402, pos_z = 437.321, pos_rot = 66 WHERE mobid = 16920662;
UPDATE mob_spawn_points SET mobname = "Aerns_Wynav", polutils_name = "Aern\'s Wynav", groupid = 9, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 0 WHERE mobid = 16920663;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -183.206, pos_y = -0.545, pos_z = 458.967, pos_rot = 127 WHERE mobid = 16920664;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 16, pos_x = -271.243, pos_y = 4.445, pos_z = 231.24, pos_rot = 58 WHERE mobid = 16920665;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 10, pos_x = -286.324, pos_y = 3.4, pos_z = 223.187, pos_rot = 128 WHERE mobid = 16920666;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 20, pos_x = -271.153, pos_y = 4.445, pos_z = 231.735, pos_rot = 49 WHERE mobid = 16920667;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -259.459, pos_y = -0.545, pos_z = 175.219, pos_rot = 192 WHERE mobid = 16920668;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -580, pos_y = -0.5, pos_z = 140, pos_rot = 33 WHERE mobid = 16920669;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -539.873, pos_y = -0.599, pos_z = 133.373, pos_rot = 67 WHERE mobid = 16920670;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -533, pos_y = 1.5, pos_z = 147, pos_rot = 96 WHERE mobid = 16920671;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -536.009, pos_y = 1.399, pos_z = 151.768, pos_rot = 159 WHERE mobid = 16920672;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -541.239, pos_y = -0.595, pos_z = 61.221, pos_rot = 0 WHERE mobid = 16920673;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -533, pos_y = 1.5, pos_z = 53, pos_rot = 160 WHERE mobid = 16920674;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -532.485, pos_y = 1.399, pos_z = 51.917, pos_rot = 234 WHERE mobid = 16920675;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -618.402, pos_y = -0.599, pos_z = 67.211, pos_rot = 189 WHERE mobid = 16920676;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -627, pos_y = 1.5, pos_z = 53, pos_rot = 224 WHERE mobid = 16920677;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -617.518, pos_y = 1.399, pos_z = 53.591, pos_rot = 224 WHERE mobid = 16920678;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -611.827, pos_y = -0.599, pos_z = 140.404, pos_rot = 109 WHERE mobid = 16920679;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -627, pos_y = 1.5, pos_z = 147, pos_rot = 32 WHERE mobid = 16920680;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -627.37, pos_y = 1.003, pos_z = 137.211, pos_rot = 192 WHERE mobid = 16920681;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -533.356, pos_y = -0.587, pos_z = 91.805, pos_rot = 69 WHERE mobid = 16920682;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -633.916, pos_y = -0.599, pos_z = 101.54, pos_rot = 116 WHERE mobid = 16920683;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -552, pos_y = -0.5, pos_z = 128, pos_rot = 208 WHERE mobid = 16920684;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -552, pos_y = -0.5, pos_z = 72, pos_rot = 0 WHERE mobid = 16920685;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -608, pos_y = -0.5, pos_z = 72, pos_rot = 48 WHERE mobid = 16920686;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -608, pos_y = -0.5, pos_z = 128, pos_rot = 128 WHERE mobid = 16920687;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -700, pos_y = -0.5, pos_z = 460, pos_rot = 100 WHERE mobid = 16920688;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -712.238, pos_y = -0.599, pos_z = 419.144, pos_rot = 113 WHERE mobid = 16920689;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -693, pos_y = 1.5, pos_z = 413, pos_rot = 160 WHERE mobid = 16920690;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -702.478, pos_y = 1.399, pos_z = 411.93, pos_rot = 147 WHERE mobid = 16920691;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -778.945, pos_y = -0.599, pos_z = 419.964, pos_rot = 128 WHERE mobid = 16920692;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -787, pos_y = 1.5, pos_z = 413, pos_rot = 224 WHERE mobid = 16920693;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -786.057, pos_y = 1.399, pos_z = 410.173, pos_rot = 121 WHERE mobid = 16920694;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -769.289, pos_y = -0.599, pos_z = 499.216, pos_rot = 243 WHERE mobid = 16920695;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -787, pos_y = 1.5, pos_z = 507, pos_rot = 32 WHERE mobid = 16920696;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -788.453, pos_y = 1.399, pos_z = 501.722, pos_rot = 52 WHERE mobid = 16920697;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -710.532, pos_y = -0.595, pos_z = 499.309, pos_rot = 142 WHERE mobid = 16920698;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -693, pos_y = 1.5, pos_z = 507, pos_rot = 96 WHERE mobid = 16920699;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -692.238, pos_y = 1.39, pos_z = 497.736, pos_rot = 58 WHERE mobid = 16920700;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -748.916, pos_y = -0.599, pos_z = 411.822, pos_rot = 134 WHERE mobid = 16920701;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -770.663, pos_y = -0.599, pos_z = 464.144, pos_rot = 190 WHERE mobid = 16920702;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -768, pos_y = -0.5, pos_z = 432, pos_rot = 240 WHERE mobid = 16920703;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -712, pos_y = -0.5, pos_z = 432, pos_rot = 144 WHERE mobid = 16920704;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -712, pos_y = -0.5, pos_z = 488, pos_rot = 48 WHERE mobid = 16920705;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -768, pos_y = -0.5, pos_z = 488, pos_rot = 0 WHERE mobid = 16920706;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -420, pos_y = -0.5, pos_z = 660, pos_rot = 115 WHERE mobid = 16920707;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -453.101, pos_y = -0.571, pos_z = 661.764, pos_rot = 2 WHERE mobid = 16920708;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -467, pos_y = 1.5, pos_z = 653, pos_rot = 224 WHERE mobid = 16920709;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -455.64, pos_y = 1.399, pos_z = 648.065, pos_rot = 134 WHERE mobid = 16920710;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -453.807, pos_y = -0.57, pos_z = 740.228, pos_rot = 2 WHERE mobid = 16920711;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -467, pos_y = 1.5, pos_z = 747, pos_rot = 32 WHERE mobid = 16920712;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -462.504, pos_y = 1.391, pos_z = 753.46, pos_rot = 222 WHERE mobid = 16920713;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -381.516, pos_y = -0.598, pos_z = 738.419, pos_rot = 191 WHERE mobid = 16920714;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -373, pos_y = 1.5, pos_z = 747, pos_rot = 96 WHERE mobid = 16920715;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -381.933, pos_y = 1.399, pos_z = 753.926, pos_rot = 2 WHERE mobid = 16920716;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -391.405, pos_y = -0.599, pos_z = 660.393, pos_rot = 116 WHERE mobid = 16920717;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -373, pos_y = 1.5, pos_z = 653, pos_rot = 160 WHERE mobid = 16920718;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -374.473, pos_y = 1.396, pos_z = 661.219, pos_rot = 203 WHERE mobid = 16920719;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -469.672, pos_y = -0.598, pos_z = 704.744, pos_rot = 194 WHERE mobid = 16920720;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -424.204, pos_y = -0.596, pos_z = 731.651, pos_rot = 1 WHERE mobid = 16920721;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -448, pos_y = -0.5, pos_z = 728, pos_rot = 112 WHERE mobid = 16920722;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -448, pos_y = -0.5, pos_z = 672, pos_rot = 112 WHERE mobid = 16920723;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -392, pos_y = -0.5, pos_z = 672, pos_rot = 112 WHERE mobid = 16920724;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -392, pos_y = -0.5, pos_z = 728, pos_rot = 112 WHERE mobid = 16920725;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -140, pos_y = -0.5, pos_z = 460, pos_rot = 148 WHERE mobid = 16920726;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -139.511, pos_y = -0.59, pos_z = 495.742, pos_rot = 194 WHERE mobid = 16920727;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -147, pos_y = 1.5, pos_z = 507, pos_rot = 32 WHERE mobid = 16920728;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -149.779, pos_y = 1.377, pos_z = 501.621, pos_rot = 101 WHERE mobid = 16920729;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -68.218, pos_y = -0.598, pos_z = 498.553, pos_rot = 12 WHERE mobid = 16920730;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -53, pos_y = 1.5, pos_z = 507, pos_rot = 96 WHERE mobid = 16920731;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -59.956, pos_y = 1.399, pos_z = 506.553, pos_rot = 121 WHERE mobid = 16920732;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -59.675, pos_y = -0.578, pos_z = 421.131, pos_rot = 60 WHERE mobid = 16920733;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -53, pos_y = 1.5, pos_z = 413, pos_rot = 160 WHERE mobid = 16920734;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -62.912, pos_y = 1.4, pos_z = 406.322, pos_rot = 117 WHERE mobid = 16920735;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -132.637, pos_y = -0.572, pos_z = 420.842, pos_rot = 255 WHERE mobid = 16920736;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -147, pos_y = 1.5, pos_z = 413, pos_rot = 224 WHERE mobid = 16920737;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -151.258, pos_y = 1.399, pos_z = 420.708, pos_rot = 58 WHERE mobid = 16920738;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -88.265, pos_y = -0.599, pos_z = 507.994, pos_rot = 7 WHERE mobid = 16920739;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -66.876, pos_y = -0.594, pos_z = 465.531, pos_rot = 194 WHERE mobid = 16920740;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -128, pos_y = -0.5, pos_z = 488, pos_rot = 32 WHERE mobid = 16920741;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -72, pos_y = -0.5, pos_z = 488, pos_rot = 224 WHERE mobid = 16920742;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -72, pos_y = -0.5, pos_z = 432, pos_rot = 208 WHERE mobid = 16920743;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -128, pos_y = -0.5, pos_z = 432, pos_rot = 48 WHERE mobid = 16920744;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -260, pos_y = -0.5, pos_z = 140, pos_rot = 180 WHERE mobid = 16920745;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -221.488, pos_y = -0.599, pos_z = 134.422, pos_rot = 51 WHERE mobid = 16920746;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -213, pos_y = 1.5, pos_z = 147, pos_rot = 96 WHERE mobid = 16920747;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -213.594, pos_y = 0.061, pos_z = 133.471, pos_rot = 191 WHERE mobid = 16920748;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -220.493, pos_y = -0.599, pos_z = 71.725, pos_rot = 203 WHERE mobid = 16920749;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -213, pos_y = 1.5, pos_z = 53, pos_rot = 160 WHERE mobid = 16920750;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -222.663, pos_y = 1.399, pos_z = 53.311, pos_rot = 120 WHERE mobid = 16920751;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -299.107, pos_y = -0.599, pos_z = 60.871, pos_rot = 129 WHERE mobid = 16920752;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -307, pos_y = 1.5, pos_z = 53, pos_rot = 224 WHERE mobid = 16920753;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -298.992, pos_y = 1.399, pos_z = 51.616, pos_rot = 208 WHERE mobid = 16920754;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -300.095, pos_y = -0.599, pos_z = 136.103, pos_rot = 185 WHERE mobid = 16920755;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -307, pos_y = 1.5, pos_z = 147, pos_rot = 32 WHERE mobid = 16920756;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -312.144, pos_y = 1.383, pos_z = 141.022, pos_rot = 99 WHERE mobid = 16920757;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -212.725, pos_y = -0.599, pos_z = 100.067, pos_rot = 60 WHERE mobid = 16920758;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -262.191, pos_y = -0.595, pos_z = 65.998, pos_rot = 255 WHERE mobid = 16920759;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -232, pos_y = -0.5, pos_z = 128, pos_rot = 144 WHERE mobid = 16920760;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -232, pos_y = -0.5, pos_z = 72, pos_rot = 96 WHERE mobid = 16920761;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -288, pos_y = -0.5, pos_z = 72, pos_rot = 128 WHERE mobid = 16920762;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -288, pos_y = -0.5, pos_z = 128, pos_rot = 32 WHERE mobid = 16920763;
UPDATE mob_spawn_points SET mobname = "Qnzdei", polutils_name = "Qn\'zdei", groupid = 1, pos_x = -429, pos_y = -1, pos_z = -373, pos_rot = 234 WHERE mobid = 16920764;
UPDATE mob_spawn_points SET mobname = "Qnzdei", polutils_name = "Qn\'zdei", groupid = 1, pos_x = -411, pos_y = -1, pos_z = -373, pos_rot = 149 WHERE mobid = 16920765;
UPDATE mob_spawn_points SET mobname = "Qnzdei", polutils_name = "Qn\'zdei", groupid = 1, pos_x = -429, pos_y = -1, pos_z = -387, pos_rot = 60 WHERE mobid = 16920766;
UPDATE mob_spawn_points SET mobname = "Qnzdei", polutils_name = "Qn\'zdei", groupid = 1, pos_x = -411, pos_y = -1, pos_z = -387, pos_rot = 221 WHERE mobid = 16920767;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 4, pos_x = -420.312, pos_y = -3.654, pos_z = -460.286, pos_rot = 248 WHERE mobid = 16920768;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 22, pos_x = -417.416, pos_y = -3.556, pos_z = -460.041, pos_rot = 135 WHERE mobid = 16920769;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 20, pos_x = -425.464, pos_y = -2.964, pos_z = -455.729, pos_rot = 178 WHERE mobid = 16920770;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 11, pos_x = -492.35, pos_y = -3.399, pos_z = -460.64, pos_rot = 196 WHERE mobid = 16920771;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 4, pos_x = -505.8, pos_y = -6.338, pos_z = -470.415, pos_rot = 64 WHERE mobid = 16920772;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 12, pos_x = -486.89, pos_y = -6.595, pos_z = -457.648, pos_rot = 61 WHERE mobid = 16920773;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 16, pos_x = -333.968, pos_y = -3.607, pos_z = -459.017, pos_rot = 254 WHERE mobid = 16920774;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 10, pos_x = -346.391, pos_y = -2.505, pos_z = -452.703, pos_rot = 64 WHERE mobid = 16920775;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 5, pos_x = -333.169, pos_y = -3.887, pos_z = -457.433, pos_rot = 63 WHERE mobid = 16920776;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 6, pos_x = -523.442, pos_y = 5, pos_z = -520.66, pos_rot = 192 WHERE mobid = 16920777;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 4, pos_x = -516.574, pos_y = 5, pos_z = -524.607, pos_rot = 195 WHERE mobid = 16920778;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 14, pos_x = -517.681, pos_y = 5, pos_z = -515.512, pos_rot = 242 WHERE mobid = 16920779;
UPDATE mob_spawn_points SET mobname = "Aerns_Euvhi", polutils_name = "Aern\'s Euvhi", groupid = 15, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 0 WHERE mobid = 16920780;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 7, pos_x = -514.866, pos_y = 4.5, pos_z = -362.77, pos_rot = 155 WHERE mobid = 16920781;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 4, pos_x = -520.48, pos_y = 5, pos_z = -354.912, pos_rot = 46 WHERE mobid = 16920782;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 8, pos_x = -524.369, pos_y = 4.399, pos_z = -362.952, pos_rot = 100 WHERE mobid = 16920783;
UPDATE mob_spawn_points SET mobname = "Aerns_Wynav", polutils_name = "Aern\'s Wynav", groupid = 9, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 0 WHERE mobid = 16920784;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 3, pos_x = -316.703, pos_y = 5, pos_z = -364.271, pos_rot = 127 WHERE mobid = 16920785;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 23, pos_x = -320.583, pos_y = 5, pos_z = -355.315, pos_rot = 18 WHERE mobid = 16920786;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 17, pos_x = -325.021, pos_y = 4.399, pos_z = -363.585, pos_rot = 80 WHERE mobid = 16920787;
UPDATE mob_spawn_points SET mobname = "Aerns_Elemental", polutils_name = "Aern\'s Elemental", groupid = 18, pos_x = 1, pos_y = 1, pos_z = 1, pos_rot = 0 WHERE mobid = 16920788;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 10, pos_x = -317.925, pos_y = 4.4, pos_z = -517.558, pos_rot = 236 WHERE mobid = 16920789;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 12, pos_x = -323.929, pos_y = 4.348, pos_z = -515.356, pos_rot = 90 WHERE mobid = 16920790;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 5, pos_x = -322.975, pos_y = 4.351, pos_z = -521.855, pos_rot = 2 WHERE mobid = 16920791;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -450.591, pos_y = -0.599, pos_z = -501.571, pos_rot = 12 WHERE mobid = 16920792;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -447.625, pos_y = -0.599, pos_z = -505.248, pos_rot = 76 WHERE mobid = 16920793;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -499.654, pos_y = -0.599, pos_z = -427.084, pos_rot = 238 WHERE mobid = 16920794;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 4, pos_x = -447.895, pos_y = -0.599, pos_z = -377.984, pos_rot = 22 WHERE mobid = 16920795;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 11, pos_x = -453.444, pos_y = -0.599, pos_z = -381.291, pos_rot = 86 WHERE mobid = 16920796;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 2, pos_x = -465.277, pos_y = -0.6, pos_z = -340.585, pos_rot = 208 WHERE mobid = 16920797;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 16, pos_x = -450.814, pos_y = -0.6, pos_z = -348.223, pos_rot = 25 WHERE mobid = 16920798;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 3, pos_x = -369.976, pos_y = -0.6, pos_z = -345.163, pos_rot = 157 WHERE mobid = 16920799;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 2, pos_x = -385.308, pos_y = -0.6, pos_z = -339.924, pos_rot = 218 WHERE mobid = 16920800;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 7, pos_x = -367.85, pos_y = -0.6, pos_z = -377.513, pos_rot = 52 WHERE mobid = 16920801;
UPDATE mob_spawn_points SET mobname = "Awaern", polutils_name = "Aw\'aern", groupid = 6, pos_x = -368.635, pos_y = -0.6, pos_z = -374.224, pos_rot = 2 WHERE mobid = 16920802;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -334.486, pos_y = -0.598, pos_z = -408.796, pos_rot = 221 WHERE mobid = 16920803;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -370.993, pos_y = -0.598, pos_z = -505.414, pos_rot = 212 WHERE mobid = 16920804;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -363.831, pos_y = -0.599, pos_z = -499.745, pos_rot = 233 WHERE mobid = 16920805;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -457.144, pos_y = -6.599, pos_z = -526.962, pos_rot = 138 WHERE mobid = 16920806;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -526.787, pos_y = -6.599, pos_z = -470.092, pos_rot = 11 WHERE mobid = 16920807;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -526.957, pos_y = -6.599, pos_z = -431.183, pos_rot = 230 WHERE mobid = 16920808;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -491.279, pos_y = -0.6, pos_z = -407.426, pos_rot = 254 WHERE mobid = 16920809;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -448, pos_y = -0.5, pos_z = -312, pos_rot = 144 WHERE mobid = 16920810;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -392, pos_y = -0.5, pos_z = -312, pos_rot = 192 WHERE mobid = 16920811;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -352.736, pos_y = -0.599, pos_z = -420.682, pos_rot = 187 WHERE mobid = 16920812;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -311.953, pos_y = -6.599, pos_z = -407.036, pos_rot = 30 WHERE mobid = 16920813;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -307.993, pos_y = -6.598, pos_z = -459.501, pos_rot = 15 WHERE mobid = 16920814;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -369.084, pos_y = -6.599, pos_z = -528.699, pos_rot = 75 WHERE mobid = 16920815;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -471.933, pos_y = -0.597, pos_z = -548.854, pos_rot = 154 WHERE mobid = 16920816;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -553.887, pos_y = -0.599, pos_z = -458.816, pos_rot = 69 WHERE mobid = 16920817;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -552.228, pos_y = -0.59, pos_z = -430.01, pos_rot = 155 WHERE mobid = 16920818;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -289.699, pos_y = -0.598, pos_z = -418.721, pos_rot = 55 WHERE mobid = 16920819;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -288.99, pos_y = -0.594, pos_z = -462.705, pos_rot = 191 WHERE mobid = 16920820;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -371.957, pos_y = -0.593, pos_z = -552.325, pos_rot = 93 WHERE mobid = 16920821;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -467, pos_y = 1.5, pos_z = -293, pos_rot = 32 WHERE mobid = 16920822;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -459.448, pos_y = -0.599, pos_z = -304.569, pos_rot = 193 WHERE mobid = 16920823;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -373, pos_y = 1.5, pos_z = -293, pos_rot = 96 WHERE mobid = 16920824;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -386.058, pos_y = -0.599, pos_z = -302.297, pos_rot = 141 WHERE mobid = 16920825;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -580, pos_y = -0.5, pos_z = -740, pos_rot = 212 WHERE mobid = 16920826;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -627.102, pos_y = 1.394, pos_z = -739.585, pos_rot = 210 WHERE mobid = 16920827;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -621.936, pos_y = 1.399, pos_z = -750.944, pos_rot = 136 WHERE mobid = 16920828;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -627, pos_y = 1.5, pos_z = -747, pos_rot = 224 WHERE mobid = 16920829;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -617.707, pos_y = 1.399, pos_z = -652.774, pos_rot = 17 WHERE mobid = 16920830;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -631.085, pos_y = 1.399, pos_z = -661.288, pos_rot = 200 WHERE mobid = 16920831;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -627, pos_y = 1.5, pos_z = -653, pos_rot = 32 WHERE mobid = 16920832;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -527.795, pos_y = 1.399, pos_z = -659.235, pos_rot = 239 WHERE mobid = 16920833;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -533.081, pos_y = 1.399, pos_z = -650.913, pos_rot = 254 WHERE mobid = 16920834;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -533, pos_y = 1.5, pos_z = -653, pos_rot = 96 WHERE mobid = 16920835;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -540.554, pos_y = 1.393, pos_z = -746.447, pos_rot = 133 WHERE mobid = 16920836;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -541.097, pos_y = 1.399, pos_z = -746.25, pos_rot = 132 WHERE mobid = 16920837;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -533, pos_y = 1.5, pos_z = -747, pos_rot = 160 WHERE mobid = 16920838;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -627.078, pos_y = -0.598, pos_z = -708.46, pos_rot = 59 WHERE mobid = 16920839;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -582.696, pos_y = -0.566, pos_z = -666.526, pos_rot = 3 WHERE mobid = 16920840;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -532.105, pos_y = -0.597, pos_z = -699.885, pos_rot = 195 WHERE mobid = 16920841;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -608, pos_y = -0.5, pos_z = -728, pos_rot = 160 WHERE mobid = 16920842;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -608, pos_y = -0.5, pos_z = -672, pos_rot = 112 WHERE mobid = 16920843;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -552, pos_y = -0.5, pos_z = -672, pos_rot = 184 WHERE mobid = 16920844;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -552, pos_y = -0.5, pos_z = -728, pos_rot = 68 WHERE mobid = 16920845;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -580, pos_y = -0.5, pos_z = -645, pos_rot = 64 WHERE mobid = 16920846;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -780, pos_y = -0.5, pos_z = -340, pos_rot = 76 WHERE mobid = 16920847;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -787.005, pos_y = -0.595, pos_z = -310.056, pos_rot = 143 WHERE mobid = 16920848;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -792.564, pos_y = 1.272, pos_z = -300.688, pos_rot = 155 WHERE mobid = 16920849;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -787, pos_y = 1.5, pos_z = -293, pos_rot = 32 WHERE mobid = 16920850;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -710.68, pos_y = -0.599, pos_z = -293.947, pos_rot = 208 WHERE mobid = 16920851;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -702.285, pos_y = 1.399, pos_z = -288.564, pos_rot = 4 WHERE mobid = 16920852;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -693, pos_y = 1.5, pos_z = -293, pos_rot = 96 WHERE mobid = 16920853;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -697.13, pos_y = 1.399, pos_z = -391.503, pos_rot = 248 WHERE mobid = 16920854;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -702.065, pos_y = 1.399, pos_z = -388.156, pos_rot = 126 WHERE mobid = 16920855;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -693, pos_y = 1.5, pos_z = -387, pos_rot = 160 WHERE mobid = 16920856;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -783.899, pos_y = 1.343, pos_z = -390.061, pos_rot = 117 WHERE mobid = 16920857;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -786.799, pos_y = 1.297, pos_z = -386.886, pos_rot = 64 WHERE mobid = 16920858;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -787, pos_y = 1.5, pos_z = -387, pos_rot = 224 WHERE mobid = 16920859;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -734.332, pos_y = -0.599, pos_z = -293.064, pos_rot = 1 WHERE mobid = 16920860;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -706.438, pos_y = -0.57, pos_z = -333.438, pos_rot = 73 WHERE mobid = 16920861;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -743.785, pos_y = -0.594, pos_z = -388.604, pos_rot = 129 WHERE mobid = 16920862;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -768, pos_y = -0.5, pos_z = -312, pos_rot = 140 WHERE mobid = 16920863;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -712, pos_y = -0.5, pos_z = -312, pos_rot = 132 WHERE mobid = 16920864;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -712, pos_y = -0.5, pos_z = -368, pos_rot = 232 WHERE mobid = 16920865;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -768, pos_y = -0.5, pos_z = -368, pos_rot = 64 WHERE mobid = 16920866;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -685, pos_y = -0.5, pos_z = -340, pos_rot = 128 WHERE mobid = 16920867;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -420, pos_y = -0.5, pos_z = -60, pos_rot = 55 WHERE mobid = 16920868;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -378.582, pos_y = 1.399, pos_z = -47.92, pos_rot = 209 WHERE mobid = 16920869;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -380.292, pos_y = -0.599, pos_z = -61.51, pos_rot = 192 WHERE mobid = 16920870;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -373, pos_y = 1.5, pos_z = -53, pos_rot = 96 WHERE mobid = 16920871;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -386.733, pos_y = 0.188, pos_z = -146.01, pos_rot = 255 WHERE mobid = 16920872;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -381.535, pos_y = 1.399, pos_z = -151.217, pos_rot = 248 WHERE mobid = 16920873;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -373, pos_y = 1.5, pos_z = -147, pos_rot = 160 WHERE mobid = 16920874;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -470.92, pos_y = 1.399, pos_z = -141.748, pos_rot = 51 WHERE mobid = 16920875;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -474.237, pos_y = 1.399, pos_z = -137.708, pos_rot = 62 WHERE mobid = 16920876;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -467, pos_y = 1.5, pos_z = -147, pos_rot = 224 WHERE mobid = 16920877;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -463.311, pos_y = -0.57, pos_z = -70.961, pos_rot = 17 WHERE mobid = 16920878;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -467.572, pos_y = 1.399, pos_z = -49.84, pos_rot = 110 WHERE mobid = 16920879;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -467, pos_y = 1.5, pos_z = -53, pos_rot = 32 WHERE mobid = 16920880;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -468.928, pos_y = -0.575, pos_z = -89.974, pos_rot = 198 WHERE mobid = 16920881;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -431.579, pos_y = -0.587, pos_z = -133.104, pos_rot = 122 WHERE mobid = 16920882;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -373.326, pos_y = -0.573, pos_z = -96.755, pos_rot = 192 WHERE mobid = 16920883;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -392, pos_y = -0.5, pos_z = -72, pos_rot = 40 WHERE mobid = 16920884;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -392, pos_y = -0.5, pos_z = -128, pos_rot = 248 WHERE mobid = 16920885;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -448, pos_y = -0.5, pos_z = -128, pos_rot = 96 WHERE mobid = 16920886;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -448, pos_y = -0.5, pos_z = -72, pos_rot = 96 WHERE mobid = 16920887;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -420, pos_y = -0.5, pos_z = -155, pos_rot = 192 WHERE mobid = 16920888;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -60, pos_y = -0.5, pos_z = -340, pos_rot = 189 WHERE mobid = 16920889;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -55.207, pos_y = 1.298, pos_z = -390.664, pos_rot = 88 WHERE mobid = 16920890;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -52.484, pos_y = 1.399, pos_z = -379.704, pos_rot = 199 WHERE mobid = 16920891;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -53, pos_y = 1.5, pos_z = -387, pos_rot = 160 WHERE mobid = 16920892;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -149.001, pos_y = 1.4, pos_z = -383.811, pos_rot = 152 WHERE mobid = 16920893;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -148.91, pos_y = 1.399, pos_z = -380.829, pos_rot = 105 WHERE mobid = 16920894;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -147, pos_y = 1.5, pos_z = -387, pos_rot = 224 WHERE mobid = 16920895;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -129.143, pos_y = -0.593, pos_z = -293.597, pos_rot = 175 WHERE mobid = 16920896;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -137.485, pos_y = 1.399, pos_z = -292.084, pos_rot = 19 WHERE mobid = 16920897;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -147, pos_y = 1.5, pos_z = -293, pos_rot = 32 WHERE mobid = 16920898;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -53.376, pos_y = 1.399, pos_z = -290.599, pos_rot = 251 WHERE mobid = 16920899;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -45.746, pos_y = 1.392, pos_z = -303.198, pos_rot = 53 WHERE mobid = 16920900;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -53, pos_y = 1.5, pos_z = -293, pos_rot = 96 WHERE mobid = 16920901;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -101.891, pos_y = -0.595, pos_z = -386.618, pos_rot = 2 WHERE mobid = 16920902;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -131.018, pos_y = -0.529, pos_z = -338.808, pos_rot = 68 WHERE mobid = 16920903;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -100.133, pos_y = -0.599, pos_z = -290.02, pos_rot = 4 WHERE mobid = 16920904;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -72, pos_y = -0.5, pos_z = -368, pos_rot = 120 WHERE mobid = 16920905;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -128, pos_y = -0.5, pos_z = -368, pos_rot = 240 WHERE mobid = 16920906;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -128, pos_y = -0.5, pos_z = -312, pos_rot = 32 WHERE mobid = 16920907;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -72, pos_y = -0.5, pos_z = -312, pos_rot = 148 WHERE mobid = 16920908;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -155, pos_y = -0.5, pos_z = -340, pos_rot = 0 WHERE mobid = 16920909;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -260, pos_y = -0.5, pos_z = -740, pos_rot = 23 WHERE mobid = 16920910;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -308.028, pos_y = 1.399, pos_z = -738.438, pos_rot = 187 WHERE mobid = 16920911;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -300.735, pos_y = 1.39, pos_z = -752.736, pos_rot = 130 WHERE mobid = 16920912;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -307, pos_y = 1.5, pos_z = -747, pos_rot = 224 WHERE mobid = 16920913;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -296.978, pos_y = 1.397, pos_z = -647.238, pos_rot = 242 WHERE mobid = 16920914;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -308.438, pos_y = 1.399, pos_z = -662.728, pos_rot = 32 WHERE mobid = 16920915;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -307, pos_y = 1.5, pos_z = -653, pos_rot = 32 WHERE mobid = 16920916;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -209.296, pos_y = 1.399, pos_z = -662.534, pos_rot = 118 WHERE mobid = 16920917;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -207.208, pos_y = 1.398, pos_z = -665.629, pos_rot = 47 WHERE mobid = 16920918;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -213, pos_y = 1.5, pos_z = -653, pos_rot = 96 WHERE mobid = 16920919;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -216.013, pos_y = 1.399, pos_z = -750.55, pos_rot = 5 WHERE mobid = 16920920;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -222.072, pos_y = 1.398, pos_z = -749.568, pos_rot = 183 WHERE mobid = 16920921;
UPDATE mob_spawn_points SET mobname = "Aweuvhi", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -213, pos_y = 1.5, pos_z = -747, pos_rot = 160 WHERE mobid = 16920922;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -307.318, pos_y = -0.599, pos_z = -699.831, pos_rot = 65 WHERE mobid = 16920923;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -268.62, pos_y = -0.59, pos_z = -668.498, pos_rot = 134 WHERE mobid = 16920924;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = -210.234, pos_y = -0.599, pos_z = -703.533, pos_rot = 199 WHERE mobid = 16920925;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -288, pos_y = -0.5, pos_z = -728, pos_rot = 136 WHERE mobid = 16920926;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -288, pos_y = -0.5, pos_z = -672, pos_rot = 200 WHERE mobid = 16920927;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -232, pos_y = -0.5, pos_z = -672, pos_rot = 180 WHERE mobid = 16920928;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = -232, pos_y = -0.5, pos_z = -728, pos_rot = 12 WHERE mobid = 16920929;
UPDATE mob_spawn_points SET mobname = "Aweuvhi_open", polutils_name = "Aw\'euvhi", groupid = 19, pos_x = -260, pos_y = -0.5, pos_z = -645, pos_rot = 64 WHERE mobid = 16920930;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 236, pos_y = -6.5, pos_z = 98, pos_rot = 136 WHERE mobid = 16920931;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 236, pos_y = -6.5, pos_z = 102, pos_rot = 120 WHERE mobid = 16920932;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 225.627, pos_y = -0.599, pos_z = 91.089, pos_rot = 117 WHERE mobid = 16920933;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 284, pos_y = -6.5, pos_z = 98, pos_rot = 217 WHERE mobid = 16920934;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 284, pos_y = -6.5, pos_z = 102, pos_rot = 8 WHERE mobid = 16920935;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 290.849, pos_y = -0.553, pos_z = 92.292, pos_rot = 191 WHERE mobid = 16920936;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 220, pos_y = -0.5, pos_z = 60, pos_rot = 88 WHERE mobid = 16920937;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 220, pos_y = -0.5, pos_z = 140, pos_rot = 98 WHERE mobid = 16920938;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 300, pos_y = -0.5, pos_z = 140, pos_rot = 101 WHERE mobid = 16920939;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 300, pos_y = -0.5, pos_z = 60, pos_rot = 31 WHERE mobid = 16920940;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 255.998, pos_y = -0.599, pos_z = 48.775, pos_rot = 0 WHERE mobid = 16920941;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 256.097, pos_y = -0.599, pos_z = 152.539, pos_rot = 247 WHERE mobid = 16920942;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 259.087, pos_y = -6.599, pos_z = 67.366, pos_rot = 43 WHERE mobid = 16920943;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 270.279, pos_y = -6.599, pos_z = 129.499, pos_rot = 227 WHERE mobid = 16920944;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 208, pos_y = -0.5, pos_z = 88, pos_rot = 160 WHERE mobid = 16920945;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 312, pos_y = -0.5, pos_z = 112, pos_rot = 144 WHERE mobid = 16920946;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 98, pos_y = -6.5, pos_z = 436, pos_rot = 62 WHERE mobid = 16920947;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 102, pos_y = -6.5, pos_z = 436, pos_rot = 72 WHERE mobid = 16920948;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 90.284, pos_y = -0.599, pos_z = 422.378, pos_rot = 237 WHERE mobid = 16920949;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 98, pos_y = -6.5, pos_z = 484, pos_rot = 200 WHERE mobid = 16920950;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 102, pos_y = -6.5, pos_z = 484, pos_rot = 184 WHERE mobid = 16920951;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 94.203, pos_y = -0.595, pos_z = 499.79, pos_rot = 218 WHERE mobid = 16920952;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 60, pos_y = -0.5, pos_z = 420, pos_rot = 127 WHERE mobid = 16920953;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 60, pos_y = -0.5, pos_z = 500, pos_rot = 37 WHERE mobid = 16920954;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 140, pos_y = -0.5, pos_z = 500, pos_rot = 36 WHERE mobid = 16920955;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 140, pos_y = -0.5, pos_z = 420, pos_rot = 143 WHERE mobid = 16920956;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 49.261, pos_y = -0.599, pos_z = 461.768, pos_rot = 63 WHERE mobid = 16920957;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 146.811, pos_y = -0.585, pos_z = 471.081, pos_rot = 252 WHERE mobid = 16920958;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 72.472, pos_y = -6.597, pos_z = 465.221, pos_rot = 193 WHERE mobid = 16920959;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 128.348, pos_y = -6.582, pos_z = 468.656, pos_rot = 50 WHERE mobid = 16920960;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 88, pos_y = -0.5, pos_z = 408, pos_rot = 96 WHERE mobid = 16920961;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 88, pos_y = -0.5, pos_z = 512, pos_rot = 176 WHERE mobid = 16920962;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 396, pos_y = -6.5, pos_z = 702, pos_rot = 120 WHERE mobid = 16920963;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 396, pos_y = -6.5, pos_z = 698, pos_rot = 136 WHERE mobid = 16920964;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 389.904, pos_y = -0.599, pos_z = 690.634, pos_rot = 41 WHERE mobid = 16920965;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 444, pos_y = -6.5, pos_z = 702, pos_rot = 8 WHERE mobid = 16920966;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 444, pos_y = -6.5, pos_z = 698, pos_rot = 217 WHERE mobid = 16920967;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 455.555, pos_y = -0.599, pos_z = 711.86, pos_rot = 168 WHERE mobid = 16920968;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 380, pos_y = -0.5, pos_z = 660, pos_rot = 121 WHERE mobid = 16920969;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 380, pos_y = -0.5, pos_z = 740, pos_rot = 219 WHERE mobid = 16920970;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 460, pos_y = -0.5, pos_z = 740, pos_rot = 81 WHERE mobid = 16920971;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 460, pos_y = -0.5, pos_z = 660, pos_rot = 242 WHERE mobid = 16920972;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 423.585, pos_y = -0.598, pos_z = 648.974, pos_rot = 3 WHERE mobid = 16920973;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 412.913, pos_y = -0.599, pos_z = 753.01, pos_rot = 128 WHERE mobid = 16920974;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 419.577, pos_y = -6.599, pos_z = 667.373, pos_rot = 72 WHERE mobid = 16920975;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 416.226, pos_y = -6.599, pos_z = 728.008, pos_rot = 0 WHERE mobid = 16920976;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 368, pos_y = -0.5, pos_z = 688, pos_rot = 64 WHERE mobid = 16920977;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 472, pos_y = -0.5, pos_z = 712, pos_rot = 128 WHERE mobid = 16920978;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 738, pos_y = -6.5, pos_z = 436, pos_rot = 62 WHERE mobid = 16920979;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 742, pos_y = -6.5, pos_z = 436, pos_rot = 72 WHERE mobid = 16920980;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 734.073, pos_y = -0.599, pos_z = 418.35, pos_rot = 131 WHERE mobid = 16920981;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 738, pos_y = -6.5, pos_z = 484, pos_rot = 200 WHERE mobid = 16920982;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 742, pos_y = -6.5, pos_z = 484, pos_rot = 184 WHERE mobid = 16920983;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 737.968, pos_y = -0.599, pos_z = 499.551, pos_rot = 251 WHERE mobid = 16920984;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 700, pos_y = -0.5, pos_z = 420, pos_rot = 197 WHERE mobid = 16920985;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 700, pos_y = -0.5, pos_z = 500, pos_rot = 90 WHERE mobid = 16920986;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 780, pos_y = -0.5, pos_z = 500, pos_rot = 243 WHERE mobid = 16920987;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 780, pos_y = -0.5, pos_z = 420, pos_rot = 60 WHERE mobid = 16920988;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 689.308, pos_y = -0.561, pos_z = 456.922, pos_rot = 191 WHERE mobid = 16920989;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 794.05, pos_y = -0.599, pos_z = 456.765, pos_rot = 191 WHERE mobid = 16920990;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 712.404, pos_y = -6.599, pos_z = 451.573, pos_rot = 203 WHERE mobid = 16920991;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 768.414, pos_y = -6.599, pos_z = 451.42, pos_rot = 206 WHERE mobid = 16920992;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 752, pos_y = -0.5, pos_z = 408, pos_rot = 176 WHERE mobid = 16920993;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 752, pos_y = -0.5, pos_z = 512, pos_rot = 64 WHERE mobid = 16920994;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 556, pos_y = -6.5, pos_z = 102, pos_rot = 120 WHERE mobid = 16920995;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 556, pos_y = -6.5, pos_z = 98, pos_rot = 136 WHERE mobid = 16920996;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 544.97, pos_y = -0.599, pos_z = 90.332, pos_rot = 123 WHERE mobid = 16920997;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 604, pos_y = -6.5, pos_z = 102, pos_rot = 8 WHERE mobid = 16920998;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 604, pos_y = -6.5, pos_z = 98, pos_rot = 217 WHERE mobid = 16920999;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 612.324, pos_y = -0.599, pos_z = 109.232, pos_rot = 117 WHERE mobid = 16921000;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 540, pos_y = -0.5, pos_z = 60, pos_rot = 197 WHERE mobid = 16921001;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 540, pos_y = -0.5, pos_z = 140, pos_rot = 115 WHERE mobid = 16921002;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 620, pos_y = -0.5, pos_z = 140, pos_rot = 54 WHERE mobid = 16921003;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 620, pos_y = -0.5, pos_z = 60, pos_rot = 24 WHERE mobid = 16921004;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 572.851, pos_y = -0.599, pos_z = 48.673, pos_rot = 127 WHERE mobid = 16921005;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 576.127, pos_y = -0.595, pos_z = 152.523, pos_rot = 247 WHERE mobid = 16921006;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 590.363, pos_y = -6.594, pos_z = 70.813, pos_rot = 22 WHERE mobid = 16921007;
UPDATE mob_spawn_points SET mobname = "Awghrah", polutils_name = "Aw\'ghrah", groupid = 13, pos_x = 569.121, pos_y = -6.546, pos_z = 126.954, pos_rot = 112 WHERE mobid = 16921008;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 632, pos_y = -0.5, pos_z = 112, pos_rot = 208 WHERE mobid = 16921009;
UPDATE mob_spawn_points SET mobname = "Awzdei", polutils_name = "Aw\'zdei", groupid = 21, pos_x = 528, pos_y = -0.5, pos_z = 88, pos_rot = 64 WHERE mobid = 16921010;
UPDATE mob_spawn_points SET mobname = "Ixzdei_RDM", polutils_name = "Ix\'zdei", groupid = 33, pos_x = 428.714, pos_y = -1.5, pos_z = 426.944, pos_rot = 128 WHERE mobid = 16921011;
UPDATE mob_spawn_points SET mobname = "Ixzdei_RDM", polutils_name = "Ix\'zdei", groupid = 33, pos_x = 411.359, pos_y = -1.5, pos_z = 426.962, pos_rot = 0 WHERE mobid = 16921012;
UPDATE mob_spawn_points SET mobname = "Ixzdei_BLM", polutils_name = "Ix\'zdei", groupid = 24, pos_x = 428.593, pos_y = -1.5, pos_z = 412.98, pos_rot = 128 WHERE mobid = 16921013;
UPDATE mob_spawn_points SET mobname = "Ixzdei_BLM", polutils_name = "Ix\'zdei", groupid = 24, pos_x = 411.333, pos_y = -1.5, pos_z = 412.997, pos_rot = 0 WHERE mobid = 16921014;
UPDATE mob_spawn_points SET mobname = "Jailer_of_Fortitude", polutils_name = "Jailer of Fortitude", groupid = 25, pos_x = -434.1, pos_y = -0.5, pos_z = 697.849, pos_rot = 190 WHERE mobid = 16921015;
UPDATE mob_spawn_points SET mobname = "Kfghrah_WHM", polutils_name = "Kf\'ghrah", groupid = 26, pos_x = -433.362, pos_y = -0.5, pos_z = 700.028, pos_rot = 186 WHERE mobid = 16921016;
UPDATE mob_spawn_points SET mobname = "Kfghrah_BLM", polutils_name = "Kf\'ghrah", groupid = 27, pos_x = -406.51, pos_y = -0.5, pos_z = 699.651, pos_rot = 237 WHERE mobid = 16921017;
UPDATE mob_spawn_points SET mobname = "Ixaern_DRK", polutils_name = "Ix\'aern", groupid = 28, pos_x = -590.082, pos_y = -0.592, pos_z = 110.876, pos_rot = 235 WHERE mobid = 16921018;
UPDATE mob_spawn_points SET mobname = "Qnaern", polutils_name = "Qn\'aern", groupid = 29, pos_x = -575.403, pos_y = 3.5, pos_z = 228.466, pos_rot = 108 WHERE mobid = 16921019;
UPDATE mob_spawn_points SET mobname = "Qnaern", polutils_name = "Qn\'aern", groupid = 29, pos_x = -548.878, pos_y = 3.449, pos_z = 253.894, pos_rot = 145 WHERE mobid = 16921020;
UPDATE mob_spawn_points SET mobname = "Jailer_of_Faith", polutils_name = "Jailer of Faith", groupid = 30, pos_x = -272.831, pos_y = -0.5, pos_z = -703.309, pos_rot = 66 WHERE mobid = 16921021;
UPDATE mob_spawn_points SET mobname = "Ixaern_DRG", polutils_name = "Ix\'aern", groupid = 31, pos_x = -347.931, pos_y = -0.5, pos_z = -377.922, pos_rot = 147 WHERE mobid = 16921022;
UPDATE mob_spawn_points SET mobname = "Ixaern_DRGs_Wynav", polutils_name = "Aern\'s Wynav", groupid = 32, pos_x = -350.158, pos_y = -0.607, pos_z = -379.099, pos_rot = 66 WHERE mobid = 16921023;
UPDATE mob_spawn_points SET mobname = "Ixaern_DRGs_Wynav", polutils_name = "Aern\'s Wynav", groupid = 32, pos_x = -347.785, pos_y = -0.379, pos_z = -380.105, pos_rot = 94 WHERE mobid = 16921024;
UPDATE mob_spawn_points SET mobname = "Ixaern_DRGs_Wynav", polutils_name = "Aern\'s Wynav", groupid = 32, pos_x = -349.207, pos_y = -0.499, pos_z = -380, pos_rot = 79 WHERE mobid = 16921025;

-- Toau mission fix
UPDATE mob_spawn_points SET pos_x = -180.186, pos_y = 39.575, pos_y = 178.076, pos_rot = 65 WHERE mobid = 17010725; -- Gessho pos fix

-- --------------------
-- Removing OOE NMs --
-- --------------------

-- Alzadaal Undersea Ruins
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Wulgaru"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Oupire"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Ob"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Cookieduster_Lipiroon"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17072150"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Cheese_Hoarder_Gigiroon"; -- 2008

-- Arrapago Reef
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Zareehkl_the_Jubilant"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Velionis"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Nuhn"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Lil_Apkallu"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Euryale"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Boompadu"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Armed_Gears"; -- 2008

-- Attohwa Chasm
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Sekhmet"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Sargas"; -- 2009

-- Aydeewa_Subterrane
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Pandemonium_Warden"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Nosferatu"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Lizardtrap"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Chigre"; -- 2008

-- Batallia Downs
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Skirling_Liger"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17207410"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Prankster_Maverix"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17207640"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Eyegouger"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17207608"; -- 2009

-- Beaucedine Glacier
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Calcabrina"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17232117"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Humbaba"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17232094"; -- 2009

-- Bhaflau Thickets
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Nis_Puk"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "16990403"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Mahishasura"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "16990306"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Lividroot_Amooshah"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Harvestman"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "16990252"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Dea"; -- 2008

-- Bibiki Bay
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Shankha"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "16793698"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Splacknuck";
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "16793776"; -- 2009

-- Buburimu Peninsula
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Wake_Warder_Wanda"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17260732"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Backoo"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0, pos_rot = 0 WHERE mobname = "Ketos"; -- 2009

-- Caedarva_Mire
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Vidhuwa_the_Wrathborn"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Verdelet"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Tyger"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Mahjlaef_the_Paintorn"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Experimental_Lamia"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Aynu-kaysey"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17101099"; -- 2009

-- Cape Teriggan
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Zmey_Gorynych"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Tegmine"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Killer_Jonny"; -- 2009

-- Carpenters Landing
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Tempest_Tigon"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "16785593"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Splacknuck"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "16793776"; -- 2009

-- Castle Oztroja
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Lii_Jixa_the_Somnolist"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Saa_Doyi_the_Fervid"; -- 2009

-- Castle Zvahl Baileys
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Marquis_Sabnock"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17436881"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Marquis_Naberius"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Likho"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17436714"; -- 2009

-- Crawlers Nest
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Dynast_Beetle"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17584312"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Aqrabuamelu"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17584416"; -- 2009

-- Dangruf Wadi
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Teporingo"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17559584"; -- 2009

-- East Ronfaure
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Rambukk"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17191044"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Quagmire Pugil"; -- 2010
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Sunderclaw"; -- 2010

-- East Sarutabaruta
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Duke_Decapod"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17252725"; -- 2009

-- Eastern Altepa Desert
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Sabotender_Corrido"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Nandi"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17244471"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Donnergugi"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17244268"; -- 2009

-- Fei'Yin
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Sluagh"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Mind_Hoarder"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17612859"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Jenglot"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17612840"; -- 2009

-- Fort Ghelsba
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Kegpaunch_Doshgnosh"; -- 2009

-- Garlaige Citadel
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Hovering_Hotpot"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17596628"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Hazmat"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17596520"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Frogamander"; -- 2009

-- Giddeus
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Quu_Xijo_the_Illusory"; -- 2009

-- Halvung
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Reacton"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Flammeri"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Dextrose"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Copper_Borer"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Achamoth"; -- 2008

-- Inner Horutoto Ruins
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Slendlix_Spindlethumb"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17563785"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Nocuous_Weapon"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17563801"; -- 2009

-- Jugner Forest
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Supplespine_Mujwuj"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17203475"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Sappy_Sycamore"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17203547"; -- 2009

-- King Ranperres Tomb
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Gwyllgi"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17555664"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Barbastelle"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17555721"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Ankou"; -- 2009

-- Konschtat Highlands
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Highlander_Lizard"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17219787"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Ghillie_Dhu"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17219619"; -- 2009

-- Korroloka Tunnel
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Thoon"; -- 2009

-- La Theine Plateau
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Void_Hare"; -- 2010
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Prickly_Sheep"; -- 2010
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Slumbering_Samwell"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17195221"; -- 2009

-- Lower Delkfutts Tower
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Tyrant"; -- 2009

-- Lufaise Meadows
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Flockbock"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Sengann"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Yal-un_Eke"; -- 2009

-- Mamook
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Venomfang"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Iriri_Samariri"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Firedance_Magmaal_Ja"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17043779"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Chamrosh"; -- 2008

-- Maze of Shakhrami
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Trembler_Tabitha"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17588278"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Gloombound_Lurker"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Lesath"; -- 2009

-- Meriphataud Mountains
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Chonchon"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Naa_Zeku_the_Unwaiting"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17264768"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Patripatan"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17264972"; -- 2009

-- Misareaux Coast
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Goaftrap"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Okyupete"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "16879847"; -- 2009

-- Mount Zhayolm
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Sarameya"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Khromasoul_Bhurborlor"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Fahrafahr_the_Bloodied"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Ignamoth"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17027423"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Claret"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Anantaboga"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Brass_Borer"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Chary_Apkallu"; -- 2009

-- Newton Movalpolos
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Sword_Sorcerer_Solisoq"; -- 2009

-- North Gustaberg
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Bedrock_Barry"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17211666"; -- 2009

-- Oldton Movalpolos
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Bugbear_Muscleman"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "16797770"; -- 2009

-- Ordelles Caves
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Agar_Agar"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17567901"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Donggu"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17567801"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Bombast"; -- 2009

-- Outer Horutoto Ruins
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Desmodont"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17571870"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Ah_Puch"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17571903"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Legalox_Heftyhind"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17571873"; -- 2009

-- Palborough Mines
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "BeHya_Hundredwall"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17363258"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "QuVho_Deathhurler"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17363080"; -- 2009

-- Pashhow Marshlands
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "NiZho_Bladebender"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17223797"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Toxic_Tamlyn"; -- 2009

-- Qufim Island
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Qoofim"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Atkorkamuy"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Slippery_Sucker"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17293389"; -- 2009

-- Ranguemont Pass
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Hyakume"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Gloom_Eye"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17457204"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Mucoid_Mass"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17457245"; -- 2009

-- Rolanberry Fields
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Eldritch_Edge"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17228150"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Ravenous_Crawler"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17228086"; -- 2009

-- RoMaeve
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Rogue_Receptacle"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17277079"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Nargun"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Martinet"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17277011"; -- 2009

-- Sauromugue Champaign
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Blighting_Brand"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17269016"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Thunderclaw_Thuban"; -- 2009

-- South Gustaberg
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Tococo"; -- 2009

-- Tahrongi Canyon
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Herbage_Hunter"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17256836"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Habrok"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17256493"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Bashe"; -- 2009

-- The Sanctuary of ZiTah
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Huwasi"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17272958"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Elusive_Edwin"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17272915"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Bastet"; -- 2009

-- Toraimarai Canal
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Konjac"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17469632"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Brazen_Bones"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Canal_Moocher"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17469578"; -- 2009

-- Uleguerand Range
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Frost_Flambeau"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Magnotaur"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "16797968"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Skvader"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "16797770"; -- 2009

-- Upper Delkfutts Tower
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Autarch"; -- 2009

-- Valkurm_Dunes
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Hippomaritimus"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17199351"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Metal_Shears"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17199161"; -- 2009

-- Wajaom Woodlans
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Vulpangue"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Tinnin"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Iriz_Ima"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Gotoh_Zha_the_Redolent"; -- 2008
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Gharial"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "16986320"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Chelicerata"; -- 2009

-- West Ronfaure
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Amanita"; -- 2009

-- Yhoator Jungle
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Powderer_Penny"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17285248"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Hoar-knuckled_Rimberry"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17285394"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Acolnahuacatl"; -- 2009

-- Yuhtunga Jungle
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Pyuu_the_Spatemaker"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Koropokkur"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17281061"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Bayawak"; -- 2009

-- Wajaom Woodlands
UPDATE mob_spawn_points SET pos_x = -282.000, pos_y = -24.000, pos_z = -1.000 WHERE mobid = "16986355"; -- Hydra is back!

-- West Ronfaure
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Pyracmon";

-- West Sarutabaruta
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Numbing_Norman"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17248588"; -- 2009

-- Western Altepa Desert
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Picolaton"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17289638"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Dahu"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Calchas"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17289547"; -- 2009

-- Xarcabard
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Timeworn_Warrior"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17236045"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Duke_Focalor"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17236146"; -- 2009
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobname = "Barbaric_Weapon"; -- 2009
UPDATE nm_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = "17236027"; -- 2009

-- --------------------------------
-- Cleaning over populated areas --
-- --------------------------------

-- NOTE: this is a known DSP/Topaz issue as retail mob pool and spawn system is not fully accurate


-- Maze of Shakhrami

/*  COMMENTING THIS OUT FOR WINGS AS SEEMS ALREADY FIX BUT IT AFFECTS TOPAZ-NEXT DB
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588449; -- Poison Leech
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588690; -- Poison Leech
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588693; -- Poison Leech
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588686; -- Poison Leech
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588679; -- Poison Leech
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588680; -- Poison Leech
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588678; -- Poison Leech
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588689; -- Poison Leech
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588692; -- Poison Leech
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588327; -- Ghoul
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588326; -- Ghoul
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588612; -- Wendigo
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588660; -- Goblin Shaman
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588659; -- Globin Smithy
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588663; -- Globin Furrier
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588632; -- Globin Shaman
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588658; -- Goblin Furrier
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588655; -- Goblin Shaman
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588694; -- Protozoan
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588665; -- Goblin Shaman
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588672; -- Labyrinth Scorpion
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588638; -- Ancient Bat
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588639; -- Ancient Bat
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588586; -- Labyrinth Scorpion
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588625; -- Ancient Bat
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588596; -- Goblin Mugger
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588592; -- Goblin Gambler
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588597; -- Goblin Leecher
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588507; -- Stink Bats
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588545; -- Wight
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588577; -- Wight
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588531; -- Wight
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588537; -- Ancient Bat
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588527; -- Wight
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588504; -- Goblin Ambusher
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588508; -- Stink Bat
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588498; -- Wight
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588521; -- Abyss Worm
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588581; -- Wight
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588518; -- Abyss Worm
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588576; -- Wight
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588505; -- Goblin Tinkerer
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588511; -- Goblin Leecher
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588494; -- Stink Bats
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588535; -- Abyss Worm
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588533; -- Abyss Worm
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588522; -- Abyss Worm
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588580; -- Wight
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588503; -- Goblin Butcher
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588517; -- Abyss Worm
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588470; -- Ghoul
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588302; -- Goblin Butcher
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588300; -- Goblin Ambusher
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588308; -- Goblin Leecher
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588312; -- Goblin Gambler
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588307; -- Goblin Mugger
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588587; -- Protozoan
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17588544; -- Wight 
*/

-- Monastic Cavern
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391621; -- Orcish Warchief
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391639; -- Orcish Zerker
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391640; -- Orcish Warchief
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391623; -- Orcish Gladiator
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391620; -- Orcish Trooper
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391618; -- Orcish Footsoldier
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391631; -- Orcish Zerker
UPDATE mob_spawn_points SET pos_x = -20.4147, pos_y = -0.1545, pos_z = -357.7487 WHERE mobid = 17391624; -- Orcish Trooper
UPDATE mob_spawn_points SET pos_x = -22.9977, pos_y = -1.6404, pos_z = -368.0098 WHERE mobid = 17391632; -- Orcish Warchief
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391685; -- Orcish Zerker
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391686; -- Orcish Warchief
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391687; -- Orcish Veteran
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391693; -- Orcish Veteran
UPDATE mob_spawn_points SET pos_x = -22.2975, pos_y = -0.6960, pos_z = -212.0731 WHERE mobid = 17391691; -- Orcish Warchief
UPDATE mob_spawn_points SET pos_x = 25.4334, pos_y = -0.5305, pos_z = -219.0916 WHERE mobid = 17391692; -- Orcish Dreadnought
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391697; -- Orcish Veteran
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391694; -- Orcish Predator
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391699; -- Orcish Zerker
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391702; -- Orcish Predator
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391738; -- Orcish Champion
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391737; -- Orcish Dreadnought
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391719; -- Orcish Dreadnought
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391721; -- Orcish Dragoon
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391724; -- Orcish Champion
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391730; -- Orcish Champion
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391733; -- Orcish Dreadnought
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391728; -- Orcish Farkiller
UPDATE mob_spawn_points SET pos_x = -41.9276, pos_y = -0.1078, pos_z = -139.0336 WHERE mobid = 17391705; -- Orcish Champion
UPDATE mob_spawn_points SET pos_x = -63.2345, pos_y = -0.4325, pos_z = -232.5098 WHERE mobid = 17391670; -- Orcish Dreadnought
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391673; -- Orcish Zerker
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391671; -- Orcish Veteran
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391676; -- Orcish Predator
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391664; -- Orcish Warchief
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391657; -- Orcish Veteran
UPDATE mob_spawn_points SET pos_x = -57.2369, pos_y = -0.3426, pos_z = -262.1214 WHERE mobid = 17391658; -- Orcish Dreadnought
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391648; -- Orcish Warchief
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391647; -- Orcish Zerker
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391641; -- Orcish Veteran
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391655; -- Orcish
UPDATE mob_spawn_points SET pos_x = 55.5556, pos_y = -0.4180, pos_z = -259.9413 WHERE mobid = 17391682; -- Orcish Warchief
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391747; -- Orcish Dragoon
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391759; -- Orcish Dragoon
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391746; -- Orcish Champion
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391740; -- Orcish Farkiller
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391752; -- Orcish Farkiller
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391753; -- Orcish Dreadnought
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391748; -- Orcish Farkiller
UPDATE mob_spawn_points SET pos_x = 218.0535, pos_y = -0.5788, pos_z = -148.6448 WHERE mobid = 17391757; -- Orcish Warchief
UPDATE mob_spawn_points SET pos_x = 216.7683, pos_y = -9.1406, pos_z = -238.7831 WHERE mobid = 17391750; -- Orcish Warchief
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391799; -- Orcish Footsoldier
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391798; -- Orcish Bowshooter
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391790; -- Orcish Gladiator
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391792; -- Orcish Predator
UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = 17391785; -- Orcish Footsoldier
-- UPDATE mob_spawn_points SET pos_x = 0, pos_y = 0, pos_z = 0 WHERE mobid = xxxxxx; -- xxxx