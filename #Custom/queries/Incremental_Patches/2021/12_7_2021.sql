-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------

-- -----------
-- GENERAL  --
-- -----------

-- Dynamis Jeuno
UPDATE mob_groups SET HP = 4000 WHERE zoneid = 188 AND name = "Vanguard_Dragontamer"; -- Set HP to 4k

-- Hydra is back!
UPDATE mob_spawn_points SET pos_x = -282.000, pos_y = -24.000, pos_z = -1.000 WHERE mobid = "16986355";
UPDATE nm_spawn_points SET pos_x = -282.000, pos_y = -24.000, pos_z = -1.000 WHERE mobid = "16986355";

-- Cerberus
UPDATE mob_skills SET mob_skill_aoe = 4 WHERE mob_skill_name = "sulfurous_breath"; -- Frontal cone
INSERT IGNORE INTO `mob_skills` VALUES(1892, 1229, 'cerberus_howl', 0, 7.0, 2000, 0, 1, 0, 0, 0, 0, 0, 0);

-- Deleting OOE Drops
DELETE FROM mob_droplist WHERE itemID = "2718"; -- Delete etoile_tiara_-1
DELETE FROM mob_droplist WHERE itemID = "2719"; -- Delete etoile_casaque_-1
DELETE FROM mob_droplist WHERE itemID = "2720"; -- Delete etoile_bangles_-1
DELETE FROM mob_droplist WHERE itemID = "2721"; -- Delete etoile_tights_-1
DELETE FROM mob_droplist WHERE itemID = "2722"; -- Delete etoile_toe_shoes_-1
DELETE FROM mob_droplist WHERE itemID = "3619"; -- Delete couronne_des_etoiles
DELETE FROM mob_droplist WHERE itemID = "3635"; -- Delete buche_des_etoiles
DELETE FROM mob_droplist WHERE itemID = "10668"; -- Delete etoile_tiara_+2
DELETE FROM mob_droplist WHERE itemID = "10688"; -- Delete etoile_casaque_+2
DELETE FROM mob_droplist WHERE itemID = "10708"; -- Delete etoile_bangles_+2
DELETE FROM mob_droplist WHERE itemID = "10728"; -- Delete etoile_tights_+2
DELETE FROM mob_droplist WHERE itemID = "10748"; -- Delete etoile_toe_shoes_+2
DELETE FROM mob_droplist WHERE itemID = "11305"; -- Delete etoile_casaque
DELETE FROM mob_droplist WHERE itemID = "11306"; -- Delete etoile_casaque_+1
DELETE FROM mob_droplist WHERE itemID = "11396"; -- Delete etoile_toe_shoes
DELETE FROM mob_droplist WHERE itemID = "11397"; -- Delete etoile_toe_shoes_+1
DELETE FROM mob_droplist WHERE itemID = "11478"; -- Delete etoile_tiara
DELETE FROM mob_droplist WHERE itemID = "11479"; -- Delete etoile_tiara_+1
DELETE FROM mob_droplist WHERE itemID = "15038"; -- Delete etoile_bangles
DELETE FROM mob_droplist WHERE itemID = "15039"; -- Delete etoile_bangles_+1
DELETE FROM mob_droplist WHERE itemID = "16248"; -- Delete etoile_cape
DELETE FROM mob_droplist WHERE itemID = "16360"; -- Delete etoile_tights
DELETE FROM mob_droplist WHERE itemID = "16361"; -- Delete etoile_tights_+1
DELETE FROM mob_droplist WHERE itemID = "21582"; -- Delete etoile_knife
DELETE FROM mob_droplist WHERE itemID = "25525"; -- Delete etoile_gorget
DELETE FROM mob_droplist WHERE itemID = "25526"; -- Delete etoile_gorget_+1
DELETE FROM mob_droplist WHERE itemID = "25527"; -- Delete etoile_gorget_+2
DELETE FROM mob_droplist WHERE itemID = "2723"; -- Delete argute_mortarboard_-1
DELETE FROM mob_droplist WHERE itemID = "2724"; -- Delete argute_gown_-1
DELETE FROM mob_droplist WHERE itemID = "2725"; -- Delete argute_bracers_-1
DELETE FROM mob_droplist WHERE itemID = "2726"; -- Delete argute_pants_-1
DELETE FROM mob_droplist WHERE itemID = "2727"; -- Delete argute_loafers_-1
DELETE FROM mob_droplist WHERE itemID = "10669"; -- Delete argute_mortarboard_+2
DELETE FROM mob_droplist WHERE itemID = "10689"; -- Delete argute_gown_+2
DELETE FROM mob_droplist WHERE itemID = "10709"; -- Delete argute_bracers_+2
DELETE FROM mob_droplist WHERE itemID = "10729"; -- Delete argute_pants_+2
DELETE FROM mob_droplist WHERE itemID = "10749"; -- Delete argute_loafers_+2
DELETE FROM mob_droplist WHERE itemID = "11307"; -- Delete argute_gown
DELETE FROM mob_droplist WHERE itemID = "11308"; -- Delete argute_gown_+1
DELETE FROM mob_droplist WHERE itemID = "11398"; -- Delete argute_loafers
DELETE FROM mob_droplist WHERE itemID = "11399"; -- Delete argute_loafers_+1
DELETE FROM mob_droplist WHERE itemID = "11480"; -- Delete argute_mortarboard
DELETE FROM mob_droplist WHERE itemID = "11481"; -- Delete argute_mortarboard_+1
DELETE FROM mob_droplist WHERE itemID = "15040"; -- Delete argute_bracers
DELETE FROM mob_droplist WHERE itemID = "15041"; -- Delete argute_bracers_+1
DELETE FROM mob_droplist WHERE itemID = "15925"; -- Delete argute_belt
DELETE FROM mob_droplist WHERE itemID = "16362"; -- Delete argute_pants
DELETE FROM mob_droplist WHERE itemID = "16363"; -- Delete argute_pants_+1
DELETE FROM mob_droplist WHERE itemID = "22097"; -- Delete argute_staff
DELETE FROM mob_droplist WHERE itemID = "25531"; -- Delete argute_stole
DELETE FROM mob_droplist WHERE itemID = "25532"; -- Delete argute_stole_+1
DELETE FROM mob_droplist WHERE itemID = "25533"; -- Delete argute_stole_+2