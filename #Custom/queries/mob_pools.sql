-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------

-- IMPORTANT: Run mob_family_system queries before these!!!


-- -----------
-- GENERAL  --
-- -----------



-- Beadeaux
UPDATE mob_pools SET modelid = X'00008B0200000000000000000000000000000000' WHERE poolid = 538 AND name = "Bronze_Quadav"; -- His model was not era accurate.

-- Bibiki Bay
UPDATE mob_pools SET immunity = "1040", entityflags = "7" WHERE name = "Shen"; -- Immune to Silence and Requiem and also increased its size
UPDATE mob_pools SET immunity = "12" WHERE name = "Shens_Filtrate"; -- Immune to Bind, Stun

-- Bostaunieux Oubliette
UPDATE mob_pools SET familyid = "172" WHERE poolid = 5138 AND name = "Bloodsucker"; -- Bloodsucker NM should link with regular Bloodsucker as confirmed here: https://ffxi.allakhazam.com/db/bestiary.html?fmob=726

-- Castle Oztroja
UPDATE mob_pools SET entityFlags = "4" WHERE poolid = 2016 AND name = "Huu_Xalmo_the_Savage"; -- Model size should be bigger.

-- Davoi
UPDATE mob_pools SET immunity = "1" WHERE poolid = 419 AND name = "Bilopdop"; -- Bilopdop should be immune to Sleep
UPDATE mob_pools SET immunity = "17" WHERE poolid = 3233 AND name = "Purpleflash_Brukdok"; -- Immune to sleep and silence
UPDATE mob_pools SET immunity = "1" WHERE poolid = 2980 AND name = "One-eyed_Gwajboj"; -- Immunte to sleep
UPDATE mob_pools SET immunity = "1" WHERE poolid = 3902 AND name = "Three-eyed_Prozpuz"; -- Immunte to sleep
UPDATE mob_groups SET HP = 7500 WHERE name = "One-eyed_Gwajboj" and zoneid = 149; --  Adjusted HP values for PLD AF3
UPDATE mob_groups SET HP = 6000 WHERE name = "Three-eyed_Prozpuz" and zoneid = 149; --  Adjusted HP values for PLD AF3

-- Fei'yin
UPDATE mob_pools SET cmbDelay = 200 WHERE poolid = 2678 AND name = "Miser_Murphy"; -- Faster delay

-- Garliage Citidel
UPDATE mob_pools SET immunity = "233" WHERE poolid = 1831 AND name = "Guardian_Statue"; -- Immune to Sleep, Stun, Bind, Slow, Paralyze

-- Ghelsba Outpost
UPDATE mob_pools SET cmbDelay = 160 WHERE NAME = 'Kilioa'; -- Kilioa has CLEARLY a faster attack speed as seen here: https://www.youtube.com/watch?v=DR8MG-hftXY
UPDATE mob_pools SET cmbDelay = 160 WHERE NAME = 'Kalamainu'; -- Kalamainu has CLEARLY a faster attack speed as seen here: https://www.youtube.com/watch?v=DR8MG-hftXY

-- Gustav Tunnel
UPDATE mob_pools SET spellList = "0", immunity = "1" WHERE poolid = 584 AND name = "Bune"; -- Bune should be immune to all forms of sleep and should not cast spells

-- Horlais Peak
UPDATE mob_pools SET spellList = 477 WHERE NAME = 'Pilwiz'; -- He should only cast Stonega II

-- Jade Sepulcher
UPDATE mob_pools SET aggro = "0", true_detection = "0", links = "0" WHERE poolid = 3327; -- Raubahn - BLU LB5 -- Adding his spell list

-- Jugner Forest
UPDATE mob_pools SET entityFlags = "159", familyid = 189 WHERE poolid = 2643 AND name = "Meteormauler_Zhagtegg"; -- His size should be bigger and should link with other Orcs

-- Labyrinth of Onzozo
UPDATE mob_pools SET immunity = "17" WHERE poolid = 2439 AND name = "Lord_of_Onzozo"; -- LoO should be only immune to Silence and Sleep

-- Meriphataud Mountains
UPDATE mob_pools SET entityFlags = "4", familyid = 270 WHERE poolid = 788 AND name = "Coo_Keja_the_Unseen"; -- His size should be bigger and should link with other Yagudos

-- Monastic Cavern
UPDATE mob_pools SET entityFlags = "1183", skill_list_id = 189 WHERE poolid = 3075 AND name = "Overlord_Bakgodek"; -- He was previously using a skill list for Dynamis Orc. He was also missing Howl. His model was also too small

-- Palborough Mines
UPDATE mob_pools SET hasSpellScript = "1", entityflags = "4", cmbDmgMult = "200"  WHERE name = "Ni_Ghu_Nestfender"; -- His casts are scripted via LUA. His dmg needs to be higher. He should have a bigger size
UPDATE mob_pools SET modelid = X'0000990200000000000000000000000000000000', entityflags = "4"  WHERE name = "No_Mho_Crimsonarmor"; -- His model and size was not era accurate. Fixed.

-- Passhow Marshlands
UPDATE mob_pools SET entityFlags = "4", familyid = 202 WHERE poolid = 519 AND name = "Bo_Who_Warmonger"; -- His size should be bigger and should link with other Quadavs
UPDATE mob_pools SET links = "1" WHERE name = "Water_Wasp"; -- Add sight linking to Water Wasps.
UPDATE mob_pools SET aggro = "1" WHERE name = "Land_Pugil"; -- Add sound aggro to Land Pugil.

-- Quicksand Caves
UPDATE mob_pools SET hasSpellScript = "1" WHERE name = "Ancient_Vessel" AND poolid = 128; -- Scripting the spell casting of Ancient Vessel so it use more Firaga 3 & Aeroga 3
INSERT IGNORE INTO `mob_pools` VALUES(6186, 'Sabotender_Bailaor', 'Sabotender_Bailaor', 212, 0x0000740100000000000000000000000000000000, 2, 2, 5, 240, 100, 0, 1, 0, 0, 0, 0, 0, 204, 133, 0, 0, 0, 1, 0, 212);
INSERT IGNORE INTO `mob_pools` VALUES(6187, 'Sabotender_Bailarin', 'Sabotender_Bailarin', 212, 0x0000740100000000000000000000000000000000, 2, 2, 7, 240, 100, 0, 1, 0, 0, 2, 0, 0, 204, 133, 0, 0, 0, 1, 0, 212);

-- RoMaeve
UPDATE mob_pools SET familyid = 110, links = 1 WHERE poolid = 3603 AND name = "Shikigami_Weapon"; -- Changed family so it links with other weapons

-- Sacrarium
UPDATE mob_pools SET immunity = 16 WHERE name = "Caithleann";
	-- CoP 4-3
UPDATE mob_pools SET immunity = "17", spellList = 478, behavior = 0 WHERE name = "Old_Professor_Mariselle" AND poolid = 2964; -- Immune to sleep and silence & custom spell list. Fixes no_standack issue.
UPDATE mob_pools SET immunity = "17", spellList = 479 WHERE name = "Mariselles_Pupil" AND poolid = 2566; -- Immune to sleep and silence & custom spell list

-- Sea Serpent Grotto
UPDATE mob_pools SET entityFlags = "4" WHERE poolid = 2914 AND name = "Novv_the_Whitehearted"; -- His size should be bigger

-- Spire of Mea
UPDATE mob_pools SET cmbDelay = 200 WHERE NAME = 'Delver'; -- Delver has CLEARLY a faster attack speed as seen here: https://www.youtube.com/watch?v=rd4QhL_pqUQ

-- Spire of Dem
UPDATE mob_pools SET cmbDelay = 180, entityFlags = 4 WHERE NAME = 'Progenerator'; -- Faster attack speed to mimic era

-- Spire of Holla
UPDATE mob_pools SET cmbDelay = 180 WHERE NAME = 'Wreaker'; -- Faster attack speed to mimic era

-- Promy & Spire Vahzl
UPDATE mob_pools SET cmbDelay = 180 WHERE NAME = 'Propagator'; -- Faster attack speed to mimic era
UPDATE mob_pools SET cmbDelay = 180 WHERE NAME = 'Procreator'; -- Faster attack speed to mimic era
UPDATE mob_pools SET cmbDelay = 200 WHERE NAME = 'Solicitor'; -- Faster attack speed to mimic era
UPDATE mob_pools SET cmbDelay = 200 WHERE NAME = 'Cumulator'; -- Faster attack speed to mimic era
UPDATE mob_pools SET cmbDelay = 180 WHERE NAME = 'Ponderer'; -- Faster attack speed to mimic era
UPDATE mob_pools SET cmbDelay = 180 WHERE NAME = 'Agonizer'; -- Faster attack speed to mimic era

-- The Shrine of Ru'Avitau
UPDATE mob_pools SET roamflag = "256" WHERE name = "Faust"; -- Change fausts roam flag to follow only lua.

-- Throne Room
UPDATE mob_pools SET cmbDelay = 185 WHERE NAME = 'Shadow_Lord'; -- Faster attack speed to mimic era

-- Uleguerand Range
UPDATE mob_pools SET spellList = "470", cmbDelay = "260", immunity = "8" WHERE name = "Geush_Urvan"; -- Immune to Stun, custom auto-attack ability to ignore shadows, adjusting his spell list
UPDATE mob_pools SET skill_list_id = "1199" WHERE name = "Black_Coney"; -- Adding his ability list

-- Upper Delkfuts Tower
UPDATE mob_pools SET cmbSkill = 6, cmbDelay = 210 WHERE name = "Alkyoneus"; -- Alky wrongfully had H2H skill, which somehow added an innate Hundred Fists bug under some conditions. Era reference video https://www.youtube.com/watch?v=3BpBRFamFxs

-- Yuhtunga Jungle
UPDATE mob_pools SET entityFlags = "4" where poolid = 2647 AND name = "Meww_the_Turtlerider"; -- His size should be bigger
UPDATE mob_pools SET entityFlags = "7" where poolid = 4252 AND name = "Voluptuous_Vilma"; -- Her size should be bigger

-- Zeruhn Mines
INSERT IGNORE INTO `mob_pools` VALUES (6031,'Giant_Amoeba','Giant_Amoeba',229,X'0000240100000000000000000000000000000000',1,1,7,240,100,0,1,0,0,16,0,0,245,131,0,0,0,0,0,229); -- Adding Giant Amoeba to the server

-- Divine Might (Quest)
UPDATE mob_pools SET immunity = "1" WHERE name = "Ark_Angel_EV"; -- Immune to Sleep - retail accurate
UPDATE mob_pools SET immunity = "1" WHERE name = "Ark_Angel_GK"; -- Immune to Sleep - retail accurate
UPDATE mob_pools SET immunity = "1" WHERE name = "Ark_Angel_HM"; -- Immune to Sleep - retail accurate
UPDATE mob_pools SET immunity = "1" WHERE name = "Ark_Angel_MR"; -- Immune to Sleep - retail accurate
UPDATE mob_pools SET immunity = "1" WHERE name = "Ark_Angel_TT"; -- Immune to Sleep - retail accurate

-- Mousse family
UPDATE mob_pools SET links = 0 WHERE name = "Mousse"; -- Mousse mobs should not link https://ffxiclopedia.fandom.com/wiki/Mousse?oldid=224795

-- CoP 2-5 Ancient Vows: Mammets
UPDATE mob_pools SET immunity = "1" WHERE name = "Mammet-19_Epsilon"; -- Immune to Sleep & Lullaby: https://www.youtube.com/watch?v=r1zd7ZTkDkE&t=151s&ab_channel=Mcatchan

-- Carpenters Landing
UPDATE mob_pools SET cmbDelay = 100 WHERE NAME = 'Overgrown_Ivy'; -- Overgrown_Ivy should have attacks speed close to that of Hundred Fists https://youtu.be/7Jrnhdg7PFs?t=830

-- CoP 5-3U Where Messengers Gather (Boneyard Gully Fight)
-- Shikaree_X
UPDATE mob_pools SET skill_list_id = "1155" WHERE name = "Shikaree_X" and poolid = 3598; -- Had wrong skill id list
UPDATE mob_pools SET cmbSkill = "2" WHERE name = "Shikaree_X" and poolid = 3598; -- Was H2H now Dagger
UPDATE mob_pools SET spellList = "471" WHERE name = "Shikaree_X" and poolid = 3598; -- Created spell list for Shikaree_X

-- Shikaree_Y
UPDATE mob_pools SET skill_list_id = "1156" WHERE name = "Shikaree_Y" and poolid = 3600; -- Had wrong skill id list
UPDATE mob_pools SET cmbSkill = "7" WHERE name = "Shikaree_Y" and poolid = 3600; -- Was Dagger now Scythe
UPDATE mob_pools SET sJob = "2" WHERE name = "Shikaree_Y" and poolid = 3600; -- Was DRK/DRK now DRK/MNK
UPDATE mob_pools SET spellList = "472" WHERE name = "Shikaree_Y" and poolid = 3600; -- Created spell list for Shikaree_Y

-- Shikaree_Z
UPDATE mob_pools SET skill_list_id = "1157" WHERE name = "Shikaree_Z" and poolid = 3601; -- Had wrong skill id list
UPDATE mob_pools SET spellList = "473" WHERE name = "Shikaree_Z" and poolid = 3601; -- Creasted spell list for Shikaree_Z

-- CoP 5-3L Past Sins (Mineshaft 2716 Fight)
-- Chekochuk
UPDATE mob_pools SET spellList = "474" WHERE name = "Chekochuk" and poolid = 707; -- Created custom spell list ID - used three different FFXI Captures
-- Movamuq
UPDATE mob_pools SET spellList = "475" WHERE name = "Movamuq" and poolid = 2766; -- Created custom spell list ID - used three different FFXI Captures
-- Trikotrak
UPDATE mob_pools SET spellList = "476" WHERE name = "Trikotrak" and poolid = 4005; -- Created custom spell list ID - used three different FFXI Captures
-- Bugbby
UPDATE mob_pools SET cmbDelay = 400 WHERE NAME = 'Bugbby'; -- Highered delay to mimic attack speed with the addition of double attack/triple attack % https://youtu.be/KM6KAX2iAQ0?t=575

-- Multiple areas
UPDATE mob_pools SET modelid = X'0000890200000000000000000000000000000000', entityFlags = "133" WHERE name = "Ancient_Quadav" AND poolid  = 125; -- Ancient Quadavs were using wrong armored models
UPDATE mob_pools SET entityFlags = "4" WHERE name = "Ruby_Quadav" AND poolid = 3410; -- Ruby Quadav model was too small
UPDATE mob_pools SET modelid = X'0000910200000000000000000000000000000000', entityFlags = "4" WHERE name = "Adaman_Quadav" AND poolid = 46; -- Adaman Quadavs were using wrong armored models and wrong size
UPDATE mob_pools SET modelid = X'00008B0200000000000000000000000000000000' WHERE name = "Bronze_Quadav" AND poolid = 538; -- Bronze Quadavs were using wrong armored models

-- CoP 3-5 Diabolos
UPDATE mob_pools SET immunity = "17", cmbDelay = 180 WHERE name = "Diabolos" and poolid = 1027; -- Immune to Sleeps and Silence

-- CoP 5-3 & 6-2
UPDATE mob_pools SET immunity = 241 WHERE name = "Disaster_Idol" and poolid = 1049;
UPDATE mob_pools SET skill_list_id = 5003 WHERE name = "Gargoyle" and poolid = 1463;
UPDATE mob_pools SET skill_list_id = 5003 WHERE name = "Gargoyle" and poolid = 5158;
UPDATE mob_pools SET skill_list_id = 706 WHERE name = "Ponderer" and poolid = 3172;
UPDATE mob_pools SET skill_list_id = 5002 WHERE name = "Shikaree_Zs_Wyvern" and poolid = 3602;
UPDATE mob_pools SET skill_list_id = 1158 WHERE name = "Snoll_Tzar" and poolid = 3684;

-- CoP 6-2 Spheroids
UPDATE mob_pools SET skill_list_id = 1165 WHERE name = "Warder_Aglaia" and poolid = 4291;
UPDATE mob_pools SET skill_list_id = 1165 WHERE name = "Warder_Euphrosyne" and poolid = 4292;
UPDATE mob_pools SET skill_list_id = 1165 WHERE name = "Warder_Thalia" and poolid = 4293;

-- CoP 7-4
UPDATE mob_pools SET spellList = 0, skill_list_id = 1167 WHERE name = "Boggelmann";
UPDATE mob_pools SET immunity = 17 WHERE name = "Cryptonberry_Assassin" and poolid = 847;
UPDATE mob_pools SET immunity = 17 WHERE name = "Cryptonberry_Executor" and poolid = 851;
UPDATE mob_pools SET immunity = 17, skill_list_id = 1168 WHERE name = "Dalham" and poolid = 895;
UPDATE mob_pools SET immunity = 17 WHERE name = "Cryptonberry_Assassin" and poolid = 4833;
UPDATE mob_pools SET immunity = 17 WHERE name = "Cryptonberry_Assassin" and poolid = 4834;

-- CoP Prog until 7-5
UPDATE mob_pools SET immunity = 16386 WHERE name = "Mammet-22_Zeta";
UPDATE mob_pools SET immunity = 16385 WHERE name = "Omega" AND poolid = 2973;
UPDATE mob_pools SET immunity = 16385 WHERE name = "Ultima" AND poolid = 4083;

-- CoP 7-5
UPDATE mob_pools SET immunity = 25, spellList = 458, skill_list_id = 0 WHERE name = "Cherukiki" AND poolid = 710;
UPDATE mob_pools SET immunity = 25, spellList = 459, skill_list_id = 0 WHERE name = "Kukki-Chebukki" AND poolid = 2293;
UPDATE mob_pools SET immunity = 25, animationsub = 6, skill_list_id = 0 WHERE name = "Makki-Chebukki" AND poolid = 2492;
UPDATE mob_pools SET immunity = 16417, animationsub = 7 WHERE name = "Tenzen" AND poolid = 3875;

-- BCNM 20 Wings of Fury - Furies
UPDATE mob_pools SET skill_list_id = 504 WHERE name = "Furies" and poolid = 1441; -- Change skill list ID for new WS

-- Fixing all Bat Trio ("bats" family - NOT "bat") WS Skill Lists (Zilart Areas = 47, Promathia areas + NM = 504)
UPDATE mob_pools SET skill_list_id = 504 WHERE name = "Black_Triple_Stars" and poolid = 440; -- Adds Turbulence and Slipstream
UPDATE mob_pools SET skill_list_id = 504 WHERE name = "Bulldog_Bats" and poolid = 575; -- Adds Turbulence and Slipstream
UPDATE mob_pools SET skill_list_id = 504 WHERE name = "Canal_Bats" and poolid = 5129; -- Adds Turbulence and Slipstream
UPDATE mob_pools SET skill_list_id = 504 WHERE name = "Dark_Bats" and poolid = 909; -- Adds Turbulence and Slipstream
UPDATE mob_pools SET skill_list_id = 504 WHERE name = "Fomors_Bats" and poolid = 1394; -- Adds Turbulence and Slipstream
UPDATE mob_pools SET skill_list_id = 504 WHERE name = "Hazhalm_Bats" and poolid = 1906; -- Adds Turbulence and Slipstream
UPDATE mob_pools SET skill_list_id = 504 WHERE name = "Incubus_Bats" and poolid = 2073; -- Adds Turbulence and Slipstream
UPDATE mob_pools SET skill_list_id = 504 WHERE name = "Nightmare_Gaylas" and poolid = 2853; -- Adds Turbulence and Slipstream
UPDATE mob_pools SET skill_list_id = 504 WHERE name = "Nosferatu_Bats" and poolid = 2911; -- Adds Turbulence and Slipstream
UPDATE mob_pools SET skill_list_id = 504 WHERE name = "Succubus_Bats" and poolid = 6177; -- Adds Turbulence and Slipstream
UPDATE mob_pools SET skill_list_id = 504 WHERE name = "Tres_Duendes" and poolid = 3997; -- Adds Turbulence and Slipstream
UPDATE mob_pools SET skill_list_id = 504 WHERE name = "Utgarth_Bats" and poolid = 4110; -- Adds Turbulence and Slipstream
UPDATE mob_pools SET skill_list_id = 504 WHERE name = "Vampyr_Bats" and poolid = 4128; -- Adds Turbulence and Slipstream
UPDATE mob_pools SET skill_list_id = 504 WHERE name = "Volcanic_Bats" and poolid = 4244; -- Adds Turbulence and Slipstream
UPDATE mob_pools SET skill_list_id = 504 WHERE name = "Wingrats" and poolid = 4353; -- Adds Turbulence and Slipstream

-- Minotaur
UPDATE mob_pools SET cmbDelay = 340 WHERE name = "Minotaur" and poolid = 2675; -- Lowered delay to match era

-- King Vinegarroon
UPDATE mob_pools SET immunity = 1 WHERE name = "King_Vinegarroon" and poolid = 2262; -- Immunte to sleep

-- SAM AF2
UPDATE mob_pools SET immunity = 1, spellList = 2 WHERE name = "Onryo" and poolid = 2984; -- Immunte to sleep and changed spell list to better reflect correct spells
UPDATE mob_pools SET immunity = 1, cmbDelay = 300 WHERE name = "Doman" and poolid = 1077; -- Immunte to sleep

-- BLM AF 1 & 3
UPDATE mob_pools SET immunity = "17" WHERE name = "chaos_elemental"; -- Adds silence and sleep immune for BLM AF weapon mob.
UPDATE mob_pools SET immunity = "25" WHERE name = "magic_sludge"; -- Add silence, sleep and stun immune.

-- WHM AF 3
UPDATE mob_pools SET immunity = "25" WHERE name = "altedour_i_tavnazia"; -- Add silence, sleep and stun immune. WHM AF3 mob

-- PLD AF 3
UPDATE mob_pools SET immunity = "17" WHERE name = "three-eyed_prozpuz"; -- Add Sleep and silence immune to PFD AF3 NM 1
UPDATE mob_pools SET immunity = "17" WHERE name = "one-eyed_gwajboj"; -- Add Sleep and silence immune to PFD AF3 NM 2

-- DRK AF 1,2,3
UPDATE mob_pools SET immunity = "8" WHERE name = "vaa_huja_the_erudite"; -- Add stun immune to DRK AF1 NM
UPDATE mob_pools SET immunity = "48" WHERE name = "gerwitzs_soul"; -- Add silence and paralyze immune to DRK AF2 NM3
UPDATE mob_pools SET immunity = "17" WHERE name = "gerwitzs_scythe"; -- Add silence and sleep immune to DRK AF3 NM1
UPDATE mob_pools SET immunity = "1" WHERE name = "scythe_victim"; -- Add sleep immune to DRK AF3 NM2

-- BST AF 2
UPDATE mob_pools SET immunity = "16" WHERE name = "dabotzs_ghost"; -- Add silence immune to BST AF2 NM

-- BRD AF 1,2,3
UPDATE mob_pools SET immunity = "16" WHERE name = "tros"; -- Add silence immune to BRD AF1 NM
UPDATE mob_pools SET immunity = "1" WHERE name = "yum_kimil"; -- Add sleep immune to BRD AF2 NM1
UPDATE mob_pools SET immunity = "8" WHERE name = "bugaboo"; -- Add stun immune to BRD AF3 NM

-- NIN AF 1,3
UPDATE mob_pools SET immunity = "1" WHERE name = "tsuchigumo"; -- Add sleep immune to NIN AF1 NM1 & 2
UPDATE mob_pools SET immunity = "1" WHERE name = "kappa_akuso"; -- Add sleep immune to NIN AF3 NM1
UPDATE mob_pools SET immunity = "1" WHERE name = "kappa_bonze"; -- Add sleep immune to NIN AF3 NM2
UPDATE mob_pools SET immunity = "1" WHERE name = "kappa_biwa"; -- Add sleep immune to NIN AF3 NM3

-- DRG AF 1,3
UPDATE mob_pools SET immunity = "16" WHERE name = "decurio_i-iii"; -- Add silence immune to DRG AF1 NM
UPDATE mob_pools SET immunity = "9" WHERE name = "cleuvarion_m_resoaix"; -- Add sleep and stun immune to DRG AF3 NM1
UPDATE mob_pools SET immunity = "9" WHERE name = "rompaulion_s_citalle"; -- Add sleep and stun immune to DRG AF3 NM2

-- BLU AF
UPDATE mob_pools SET immunity = "24", skill_list_id ="238" WHERE name = "nepionic_soulflayer"; -- Add silence and stun immune to BLU AF3 NM

-- LB2 Mobs
UPDATE mob_pools SET immunity = "17", cmbDelay = "200" WHERE name = "Boreal_Tiger"; -- Immune to sleep & silence
UPDATE mob_pools SET immunity = "1", cmbDelay = "200" WHERE name = "Boreal_Hound"; -- Immune to sleep
UPDATE mob_pools SET immunity = "1" WHERE name = "Boreal_Coeurl"; -- Immune to sleep

-- KA Attack Speed Fix
UPDATE mob_pools SET cmbDelay ="140" WHERE name = "King_Arthro"; -- Increase KA Attack speed to match reference video https://www.youtube.com/watch?v=mhDcFbpuS0Y&t=287s

-- Sacrarium
UPDATE mob_pools SET links = "0" WHERE name = "Azren_Kuba"; -- Should not link with each other
UPDATE mob_pools SET links = "0" WHERE name = "Azren_Kuguza"; -- Should not link with each other

-- Sky
UPDATE mob_pools SET cmbDelay = 150, immunity = 1 WHERE name = "Zipacna"; -- Speeds up delay
UPDATE mob_pools SET immunity = 17 WHERE name = "Mother_Globe"; -- Immune to sleep and silence
UPDATE mob_pools SET immunity = 641 WHERE name = "Despot"; -- Immune to Slow, Elegy and Sleep
UPDATE mob_pools SET immunity = 17 WHERE name = "Ullikummi"; -- Immune to sleep and silence
UPDATE mob_pools SET immunity = 641 WHERE name = "Faust"; -- Immune to Slow, Elegy and Sleep
UPDATE mob_pools SET immunity = 17 WHERE name = "Olla_Grande"; -- Immune to sleep and silence
UPDATE mob_pools SET immunity = 17 WHERE name = "Olla_Media"; -- Immune to sleep and silence
UPDATE mob_pools SET immunity = 17 WHERE name = "Olla_Pequena"; -- Immune to sleep and silence
UPDATE mob_pools SET immunity = 17 WHERE name = "Steam_Cleaner"; -- Immune to sleep and silence
UPDATE mob_pools SET immunity = 1 WHERE name = "Brigandish_Blade"; -- Immune to sleep

-- King Behemoth/Behemoth
UPDATE mob_pools SET cmbDelay = 180, spellList = 0, mJob = 6, immunity = 29, entityFlags = 133 WHERE name = "King_Behemoth"; -- Faster attack speed, force usaage of meteor instead of spelllist, job thf/war
UPDATE mob_pools SET cmbDelay = 200, mJob = 6 WHERE name = "Behemoth"; -- Faster attack speed, force usaage of meteor instead of spelllist, job thf/war

-- Set all Dynamis Mobs to check as NMs
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Alchemist" and poolid = 4133;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Ambusher" and poolid = 4134;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Amputator" and poolid = 4135;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Armorer" and poolid = 4136;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Assassin" and poolid = 4137;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Backstabber" and poolid = 4138;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Beasttender" and poolid = 4139;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Bugler" and poolid = 4140;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Chanter" and poolid = 4141;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Constable" and poolid = 4142;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Defender" and poolid = 4143;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Dollmaster" and poolid = 4144;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Dragon" and poolid = 4145;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Drakekeeper" and poolid = 4146;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Enchanter" and poolid = 4147;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Exemplar" and poolid = 4148;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Eye" and poolid = 4149;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Footsoldier" and poolid = 4150;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Grappler" and poolid = 4151;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Gutslasher" and poolid = 4152;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Hatamoto" and poolid = 4153;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Hawker" and poolid = 4154;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Hitman" and poolid = 4155;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Impaler" and poolid = 4156;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Inciter" and poolid = 4157;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Kusa" and poolid = 4158;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Liberator" and poolid = 4159;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Maestro" and poolid = 4160;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Mason" and poolid = 4161;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Mesmerizer" and poolid = 4162;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Militant" and poolid = 4163;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Minstrel" and poolid = 4164;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Neckchopper" and poolid = 4165;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Necromancer" and poolid = 4166;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Ogresoother" and poolid = 4167;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Oracle" and poolid = 4168;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Partisan" and poolid = 4169;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Pathfinder" and poolid = 4170;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Persecutor" and poolid = 4171;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Pillager" and poolid = 4172;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Pitfighter" and poolid = 4173;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Predator" and poolid = 4174;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Prelate" and poolid = 4175;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Priest" and poolid = 4176;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Protector" and poolid = 4177;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Purloiner" and poolid = 4178;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Ronin" and poolid = 4179;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Salvager" and poolid = 4180;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Sentinel" and poolid = 4181;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Shaman" and poolid = 4182;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Skirmisher" and poolid = 4183;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Smithy" and poolid = 4184;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Thaumaturge" and poolid = 4191;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Tinkerer" and poolid = 4192;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Trooper" and poolid = 4193;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Undertaker" and poolid = 4194;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Vexer" and poolid = 4195;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Vigilante" and poolid = 4196;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Vindicator" and poolid = 4197;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Visionary" and poolid = 4198;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Welldigger" and poolid = 4199;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vanguard_Dragontamer" and poolid = 4649;
UPDATE mob_pools SET mobType = 2 WHERE name = "Adamantking_Effigy" and poolid = 43;
UPDATE mob_pools SET mobType = 2 WHERE name = "Adamantking_Image" and poolid = 5111;
UPDATE mob_pools SET mobType = 2 WHERE name = "Arch_GuDha_Effigy" and poolid = 2284;
UPDATE mob_pools SET mobType = 2 WHERE name = "BuBho_Truesteel" and poolid = 6063;
UPDATE mob_pools SET mobType = 2 WHERE name = "GiPha_Manameister" and poolid = 1614;
UPDATE mob_pools SET mobType = 2 WHERE name = "GuDha_Effigy" and poolid = 1855;
UPDATE mob_pools SET mobType = 2 WHERE name = "GuNhi_Noondozer" and poolid = 1859;
UPDATE mob_pools SET mobType = 2 WHERE name = "KoDho_Cannonball" and poolid = 2285;
UPDATE mob_pools SET mobType = 2 WHERE name = "Vazhe_Pummelsong" and poolid = 6062;
UPDATE mob_pools SET mobType = 2 WHERE name = "ZeVho_Fallsplitter" and poolid = 4499;
UPDATE mob_pools SET mobType = 2 WHERE name = "Goblin_Replica" and poolid = 1707;
UPDATE mob_pools SET mobType = 2 WHERE name = "Avatar_Icon" and poolid = 294;
UPDATE mob_pools SET mobType = 2 WHERE name = "Adamantking_Effigy" and poolid = 43;
UPDATE mob_pools SET mobType = 2 WHERE name = "Serjeant_Tombstone" and poolid = 3548;

-- Voluptuous Vivian
UPDATE mob_pools SET cmbDelay = 180, immunity = 32 WHERE name = "Voluptuous_Vivian"; -- Slight increase attack speed

-- UPDATE mob_pools SET familyid = "111" WHERE name = "Goblin_Fisher";  --NOT NEEDED FOR WINGS AND NEEDS UPDATE FOR TOPAZ AS SHOULD AFFECT ALL ARMORED GOBLINS
UPDATE mob_pools SET familyid = "550" WHERE name LIKE "%_of_batons"; -- Unique Cardian family type so they can drop Earth crystals
UPDATE mob_pools SET familyid = "551" WHERE name LIKE "%_of_coins"; -- Unique Cardian family type so they can drop Light crystals
UPDATE mob_pools SET familyid = "552" WHERE name LIKE "%_of_swords"; -- Unique Cardian family type so they can drop Fire crystals

-- misc toau missions fixes
UPDATE mob_pools SET mobType = 0 WHERE name = "K23H1-LAMIA"; -- wrong mobType
UPDATE mob_pools SET mJob = 6, cmbDelay = 150 WHERE name = "Khimaira_13" and poolid = 2221;
UPDATE mob_pools SET flag = 1 WHERE name = "Lancelord_Gaheel_Ja";

-- Puk Family Fix
INSERT IGNORE INTO `mob_pools` VALUES(6204, 'Puk', 'Puk', 198, 0x0000d00600000000000000000000000000000000, 1, 1, 7, 200, 100, 0, 1, 0, 0, 0, 0, 0, 239, 131, 0, 0, 0, 0, 0, 1183);
INSERT IGNORE INTO `mob_pools` VALUES(6205, 'Sea_Puk', 'Sea_Puk', 198, 0x0000d00600000000000000000000000000000000, 1, 1, 7, 200, 100, 0, 1, 0, 0, 0, 0, 0, 2335, 133, 0, 0, 0, 0, 0, 1183);

-- Nyzul Chariot Fix
UPDATE mob_pools SET behavior = 1024 WHERE name = "Long-Gunned_Chariot";
UPDATE mob_pools SET behavior = 1024 WHERE name = "Long-Horned_Chariot";
UPDATE mob_pools SET behavior = 1024 WHERE name = "Shielded_Chariot";
