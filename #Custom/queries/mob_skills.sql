-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- ----------
-- General --
-- ----------


-- Balga's Dais
INSERT IGNORE INTO mob_skills VALUES (1333,152,'contagion_transfer',0,7.0,2000,1500,4,0,0,0,0,0,0);
INSERT IGNORE INTO mob_skills VALUES (1334,154,'contamination',0,7.0,2000,1500,4,0,0,0,0,0,0);
INSERT IGNORE INTO mob_skills VALUES (1335,151,'toxic_pick',0,7.0,2000,1500,4,0,0,0,0,0,0);

-- Riverne Site #A01 & #B01
INSERT IGNORE INTO mob_skills VALUES (577,911,'jettatura',4,10.0,2000,1500,4,0,0,0,0,0,0); -- There's another Jettatura with AOE properties, but that should be ONLY for Nightmare_Hippogryph
INSERT IGNORE INTO mob_skills VALUES (580,914,'fantod',0,7.0,2000,1500,1,0,0,0,0,0,0); -- Hippogryph ability
INSERT IGNORE INTO mob_skills VALUES (579,913,'choke_breath',4,7.0,2000,1500,4,0,0,0,0,0,0); -- Hippogryph ability
UPDATE mob_skills SET mob_anim_id = 910 WHERE mob_skill_name = "hoof_volley"; -- Hippogryph ability - Wrong animation was set

-- Uleguerand Range
INSERT IGNORE INTO mob_skills VALUES (1475,847,'bull_rush_alt',0,7.0,500,500,4,16,0,3,0,0,0); -- Geush Urvan
INSERT IGNORE INTO mob_skills VALUES (1331,1075,'counterstance',0,7.0,2000,1500,4,0,0,0,0,0,0); -- Geush Urvan

-- ??
INSERT IGNORE INTO mob_skills VALUES (39,8,'spirits_within',0,7.0,2000,0,4,0,0,0,0,0,0); -- Spirits Within

-- Bat Trios Family Skill
INSERT IGNORE INTO mob_skills VALUES (1157,339,'slipstream',1,16,2000,1500,4,0,0,0,0,0,0); -- Slipstream for bat trio family (bats_other)
INSERT IGNORE INTO mob_skills VALUES (1158,340,'turbulence',1,16,2000,1500,4,0,0,0,0,0,0); -- Turbulence for bat trio family (bats_other)



-- Corsair rolls
INSERT IGNORE INTO mob_skills VALUES (108,1,'hunters_roll',1,8.0,2000,0,1,0,0,0,0,0,0); -- Add hunters_roll, Dummy animation, acc
INSERT IGNORE INTO mob_skills VALUES (105,1,'chaos_roll',1,8.0,2000,0,1,0,0,0,0,0,0); -- Add chaos_roll, Dummy animation, attp
INSERT IGNORE INTO mob_skills VALUES (110,1,'ninja_roll',1,8.0,2000,0,1,0,0,0,0,0,0); -- Add ninja_roll, Dummy animation, eva

-- Cerberus
UPDATE mob_skills SET mob_skill_aoe = 4 WHERE mob_skill_name = "sulfurous_breath"; -- Frontal cone
INSERT IGNORE INTO `mob_skills` VALUES(1892, 1229, 'cerberus_howl', 0, 7.0, 2000, 0, 1, 0, 0, 0, 0, 0, 0);

-- CoP 6-2
INSERT IGNORE INTO `mob_skills` VALUES(562, 972, 'reactive_armor', 0, 7.0, 2000, 1500, 1, 0, 0, 0, 0, 0, 0);

-- CoP 7-4
INSERT IGNORE INTO `mob_skills` VALUES(626, 437, 'vulture_3', 0, 7.0, 2000, 0, 1, 0, 0, 0, 0, 0, 0);
INSERT IGNORE INTO `mob_skills` VALUES(1363, 822, 'hungry_crunch', 0, 7.0, 2000, 1500, 4, 0, 0, 0, 0, 0, 0);

-- CoP Prog until 7-5 (6-4)
INSERT IGNORE INTO `mob_skills` VALUES(1238, 928, 'target_analysis', 0, 7.0, 2000, 1500, 4, 0, 0, 0, 0, 0, 0);

-- CoP 7-5
DELETE FROM mob_skills WHERE mob_skill_name = "amatsu_torimai";
DELETE FROM mob_skills WHERE mob_skill_name = "amatsu_kazakiri";
DELETE FROM mob_skills WHERE mob_skill_name = "amatsu_yukiarashi";
DELETE FROM mob_skills WHERE mob_skill_name = "amatsu_tsukioboro";
DELETE FROM mob_skills WHERE mob_skill_name = "amatsu_hanaikusa";
DELETE FROM mob_skills WHERE mob_skill_name = "amatsu_tsukikage";
DELETE FROM mob_skills WHERE mob_skill_name = "tenzen_ranged_alt";
DELETE FROM mob_skills WHERE mob_skill_name = "oisoya";
DELETE FROM mob_skills WHERE mob_skill_name = "riceball";
DELETE FROM mob_skills WHERE mob_skill_name = "cosmic_elucidation";
INSERT IGNORE INTO `mob_skills` VALUES(1390, 1037, 'amatsu_torimai', 0, 15.0, 2000, 0, 4, 0, 0, 0, 1, 4, 0);
INSERT IGNORE INTO `mob_skills` VALUES(1391, 1038, 'amatsu_kazakiri', 0, 15.0, 2000, 0, 4, 0, 0, 0, 4, 6, 0);
INSERT IGNORE INTO `mob_skills` VALUES(1392, 1039, 'amatsu_yukiarashi', 0, 15.0, 2000, 0, 4, 0, 0, 0, 7, 6, 0);
INSERT IGNORE INTO `mob_skills` VALUES(1393, 1040, 'amatsu_tsukioboro', 0, 15.0, 2000, 0, 4, 0, 0, 0, 10, 5, 0);
INSERT IGNORE INTO `mob_skills` VALUES(1394, 1041, 'amatsu_hanaikusa', 0, 15.0, 2000, 0, 4, 0, 0, 0, 11, 2, 0);
INSERT IGNORE INTO `mob_skills` VALUES(1395, 1036, 'amatsu_tsukikage', 0, 15.0, 2000, 0, 4, 0, 0, 0, 12, 6, 0);
INSERT IGNORE INTO `mob_skills` VALUES(1396, 1034, 'tenzen_ranged_alt', 0, 25, 2000, 0, 4, 4, 0, 0, 0, 0, 0); -- tenzen ranged attack
INSERT IGNORE INTO `mob_skills` VALUES(1397, 1042, 'oisoya', 0, 15.0, 2000, 0, 4, 0, 0, 0, 0, 0, 0);
INSERT IGNORE INTO `mob_skills` VALUES(1398, 1032, 'riceball', 0, 7.0, 2000, 0, 1, 0, 0, 0, 0, 0, 0);
INSERT IGNORE INTO `mob_skills` VALUES(1399, 1035, 'cosmic_elucidation', 1, 15.0, 2000, 0, 4, 0, 0, 0, 0, 0, 0);

-- ToAU skills
UPDATE mob_skills SET mob_skill_aoe = 1, mob_skill_distance = 15 WHERE mob_skill_name = "fire_angon"; -- Adjust skill aoe and distance
UPDATE mob_skills SET mob_skill_aoe = 1, mob_skill_distance = 15 WHERE mob_skill_name = "blazing_angon"; -- Adjust skill aoe and distance