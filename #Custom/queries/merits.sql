-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- These queries will be used to make any updates needed to the database.                          --
-- Please ANNOTATE WELL for a clear understanding of what your query is changing/adding/removing.  --
-- Overly detaling your query function is NOT needed.                                              --
-- DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT.             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------

-- -----------------------------------------------------------------------------------------------
-- DO NOT USE THIS FILE TO INSERT DROPS AS WE RUN THE RISK OF DUPLICATING DATA (no primary key) --
-- --------------- INSERT NEW ENTRIES ON MOB_DROPLIST.SQL FILE IF NEEDED -------------------------
-- -----------------------------------------------------------------------------------------------

UPDATE merits SET value = 2 WHERE name = "maintenance_recast";